/* YARR (Yet Another RSS Reader)
 * Copyright (C) 2017  Logan Garcia
 *
 * This file is part of YARR.
 *
 * YARR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * YARR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with YARR.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.cgarcia.yarr.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.SystemClock;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import me.cgarcia.yarr.object.Entry;
import me.cgarcia.yarr.object.Subscription;
import me.cgarcia.yarr.object.Tag;

/**
 * Manages the SQLite database that holds entries, subscriptions,
 * and tags.
 *
 * @author  Logan Garcia
 * @version %I%, %G%
 */
public class FeedDatabase extends SQLiteOpenHelper {
    // database version
    private static final int DATABASE_VERSION = 1;

    // database name
    private static final String DATABASE_NAME = "Feeds";

    // table names
    private static final String TABLE_ENTRY = "entries";
    private static final String TABLE_SUB = "subscriptions";
    private static final String TABLE_CATEGORIZED_AS = "categorized_as";
    private static final String TABLE_TAG = "tags";

    // common column names
    private static final String KEY_SUB_ID = "sub_id";
    private static final String KEY_TAG_ID = "tag_id";

    // tags table - column names
    private static final String KEY_TAG_NAME = "tag_name";
    private static final String KEY_TAG_COUNT = "tag_count";

    // subscriptions table - column names
    private static final String KEY_SUB_NAME = "sub_name";
    private static final String KEY_SUB_URL = "sub_url";
    private static final String KEY_SUB_COUNT = "sub_count";
    private static final String KEY_SUB_ICON = "sub_icon";

    // entries table - column names
    private static final String KEY_ENTRY_ID = "entry_id";
    private static final String KEY_ENTRY_NAME = "entry_name";
    private static final String KEY_ENTRY_DATE = "entry_date";
    private static final String KEY_ENTRY_TITLE = "entry_title";
    private static final String KEY_ENTRY_CONTENT = "entry_content";
    private static final String KEY_ENTRY_AUTHOR = "entry_author";
    private static final String KEY_ENTRY_URL = "entry_url";
    private static final String KEY_ENTRY_ENCLOSURE = "entry_enclosure";
    private static final String KEY_ENTRY_READ = "entry_read";
    private static final String KEY_ENTRY_STAR = "entry_star";

    // tags table create statement
    private static final String CREATE_TABLE_TAG = "CREATE TABLE " + TABLE_TAG + "("
            + KEY_TAG_ID + " TEXT PRIMARY KEY, "
            + KEY_TAG_NAME + " TEXT NOT NULL COLLATE NOCASE, "
            + KEY_TAG_COUNT + " INTEGER)";

    // categorized as create statement
    private static final String CREATE_TABLE_CATEGORIZED_AS = "CREATE TABLE "
            + TABLE_CATEGORIZED_AS + "("
            + KEY_TAG_ID + " TEXT, "
            + KEY_SUB_ID + " TEXT, PRIMARY KEY ("
            + KEY_TAG_ID + ", "
            + KEY_SUB_ID + "), "
            + "FOREIGN KEY(" + KEY_SUB_ID + ") REFERENCES " + TABLE_SUB + "(" + KEY_SUB_ID + "), "
            + "FOREIGN KEY(" + KEY_TAG_ID + ") REFERENCES " + TABLE_TAG + "(" + KEY_TAG_ID + "))";

    // subscription table create statement
    private static final String CREATE_TABLE_SUB = "CREATE TABLE " + TABLE_SUB + "("
            + KEY_SUB_ID + " TEXT PRIMARY KEY, "
            + KEY_SUB_NAME + " TEXT NOT NULL COLLATE NOCASE, "
            + KEY_SUB_URL + " TEXT NOT NULL, "
            + KEY_SUB_COUNT + " INTEGER, "
            + KEY_SUB_ICON + " BLOB)";

    // entry table create statement
    private static final String CREATE_TABLE_ENTRY = "CREATE TABLE " + TABLE_ENTRY + "("
            + KEY_ENTRY_ID + " TEXT PRIMARY KEY, "
            + KEY_ENTRY_NAME + " TEXT COLLATE NOCASE, "
            + KEY_ENTRY_DATE + " TEXT NOT NULL, "
            + KEY_ENTRY_TITLE + " TEXT COLLATE NOCASE, "
            + KEY_ENTRY_CONTENT + " TEXT COLLATE NOCASE, "
            + KEY_ENTRY_AUTHOR + " TEXT COLLATE NOCASE, "
            + KEY_ENTRY_URL + " TEXT NOT NULL, "
            + KEY_ENTRY_ENCLOSURE + " TEXT, "
            + KEY_ENTRY_READ + " INTEGER, "
            + KEY_ENTRY_STAR + " INTEGER, "
            + KEY_SUB_ID + " TEXT, "
            + "FOREIGN KEY(" + KEY_SUB_ID + ") REFERENCES " + TABLE_SUB + "(" + KEY_SUB_ID + "))";

    /**
     * The default constructor.
     *
     * @param context the context of the activity
     */
    public FeedDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // create the tables
        db.execSQL(CREATE_TABLE_ENTRY);
        db.execSQL(CREATE_TABLE_SUB);
        db.execSQL(CREATE_TABLE_CATEGORIZED_AS);
        db.execSQL(CREATE_TABLE_TAG);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // drop the tables
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ENTRY);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SUB);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CATEGORIZED_AS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TAG);

        // create the tables again
        onCreate(db);
    }

    /**
     * Closes the database.
     */
    public void closeDB() {
        SQLiteDatabase db = this.getReadableDatabase();

        // check if database can be closed
        if (db != null && db.isOpen()) {
            db.close();
        }
    }

    /**
     * Deletes the database.
     */
    public void deleteDB() {
        SQLiteDatabase db = this.getWritableDatabase();

        // drop the tables
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ENTRY);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SUB);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CATEGORIZED_AS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TAG);

        // create the tables again
        onCreate(db);
    }

    /**
     * Creates an entry and stores it in the database.
     *
     * @param entry the entry object to store
     * @return      the status of the insertion
     */
    public long createEntry(Entry entry) {
        // set the database
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        // put the values into the table
        values.put(KEY_ENTRY_ID, entry.getEntryId());
        values.put(KEY_ENTRY_NAME, entry.getEntryName());
        values.put(KEY_ENTRY_DATE, entry.getEntryDate());
        values.put(KEY_ENTRY_TITLE, entry.getEntryTitle());
        values.put(KEY_ENTRY_CONTENT, entry.getEntryContent());
        values.put(KEY_ENTRY_AUTHOR, entry.getEntryAuthor());
        values.put(KEY_ENTRY_URL, entry.getEntryUrl());
        values.put(KEY_ENTRY_ENCLOSURE, entry.getEntryEnclosure());
        values.put(KEY_ENTRY_READ, entry.isRead());
        values.put(KEY_ENTRY_STAR, entry.isStarred());
        values.put(KEY_SUB_ID, entry.getSubId());

        // insert row
        return db.insert(TABLE_ENTRY, null, values);
    }

    /**
     * Creates a subscription and stores it in the database.
     *
     * @param subscription the subscription object to store
     * @return             the status of the insertion
     */
    public long createSub(Subscription subscription) {
        // set the database
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        // put the values into the table
        values.put(KEY_SUB_ID, subscription.getSubId());
        values.put(KEY_SUB_NAME, subscription.getSubName());
        values.put(KEY_SUB_URL, subscription.getSubUrl());
        values.put(KEY_SUB_COUNT, subscription.getSubCount());
        Bitmap bitmap = subscription.getSubIcon();

        if (bitmap != null) {
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
            values.put(KEY_SUB_ICON, stream.toByteArray());
        } else {
            values.put(KEY_SUB_ICON, new byte[0]);
        }

        // insert row
        return db.insert(TABLE_SUB, null, values);
    }

    /**
     * Creates a relationship between a tag and a
     * subscription and stores it in the database.
     *
     * @param tagId the ID of the tag
     * @param subId the ID of the subscription
     * @return      the status of the insertion
     */
    public long createCategorizedAs(String tagId, String subId) {
        // set the database
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        // put the values into the table
        values.put(KEY_TAG_ID, tagId);
        values.put(KEY_SUB_ID, subId);

        // insert row
        return db.insert(TABLE_CATEGORIZED_AS, null, values);
    }

    /**
     * Creates a tag and stores it in the database.
     *
     * @param tag the tag object to store
     * @return    the status of the insertion
     */
    public long createTag(Tag tag) {
        // set the database
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        // put the values into the table
        values.put(KEY_TAG_ID, tag.getTagId());
        values.put(KEY_TAG_NAME, tag.getTitle());
        values.put(KEY_TAG_COUNT, tag.getTagCount());

        // insert row
        return db.insert(TABLE_TAG, null, values);
    }

    /**
     * Returns an entry from the database.
     *
     * @param entryId the ID of the entry to retrieve
     * @return        the entry object
     */
    public Entry getEntry(String entryId) {
        // set the database
        SQLiteDatabase db = this.getReadableDatabase();

        // create the query
        String selectQuery = "SELECT * FROM " + TABLE_ENTRY + " WHERE "
                + KEY_ENTRY_ID + " = '" + entryId + "'";

        // set the cursor based on the query
        Cursor cursor = db.rawQuery(selectQuery, null);
        Entry entry = null;

        if (cursor.moveToFirst()) {
            // retrieve elements from table
            String entrySubId = cursor.getString(cursor.getColumnIndex(KEY_SUB_ID));
            String entryName = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_NAME));
            String entryDate = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_DATE));
            String entryTitle = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_TITLE));
            String entryContent = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_CONTENT));
            String entryAuthor = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_AUTHOR));
            String entryUrl = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_URL));
            String entryEnclosure = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_ENCLOSURE));
            boolean isRead = cursor.getInt(cursor.getColumnIndex(KEY_ENTRY_READ)) != 0;
            boolean isStarred = cursor.getInt(cursor.getColumnIndex(KEY_ENTRY_STAR)) != 0;
            entry = new Entry(entryId, entrySubId, entryName, entryDate, entryTitle, entryContent,
                    entryAuthor, entryUrl, entryEnclosure, isRead, isStarred);
        }

        // close the cursor and return the object
        cursor.close();
        return entry;
    }

    /**
     * Returns a subscription from the database.
     *
     * @param subId the ID of the subscription to retrieve
     * @return      the subscription object
     */
    public Subscription getSubscription(String subId) {
        // set the database
        SQLiteDatabase db = this.getReadableDatabase();

        // create the query
        String selectQuery = "SELECT * FROM " + TABLE_SUB + " WHERE "
                + KEY_SUB_ID + " = '" + subId + "'";

        // get the cursor from the query
        Cursor cursor = db.rawQuery(selectQuery, null);
        Subscription subscription = null;

        if (cursor.moveToFirst()) {
            // get the elements
            String subName = cursor.getString(cursor.getColumnIndex(KEY_SUB_NAME));
            String subUrl = cursor.getString(cursor.getColumnIndex(KEY_SUB_URL));
            int subCount = getSubscriptionCount(subId);
            byte[] image = cursor.getBlob(cursor.getColumnIndex(KEY_SUB_ICON));
            Bitmap subIcon = null;

            if (image.length > 0) {
                subIcon = BitmapFactory.decodeByteArray(image, 0, image.length);
            }

            subscription = new Subscription(subId, subName, subUrl, subCount, subIcon);
        }

        // close the cursor and return the object
        cursor.close();
        return subscription;
    }

    /**
     * Returns a tag from the database.
     *
     * @param tagId the ID of the tag to retrieve
     * @return      the tag object
     */
    public Tag getTag(String tagId) {
        // get the database
        SQLiteDatabase db = this.getReadableDatabase();

        // create the query
        String selectTagQuery = "SELECT * FROM " + TABLE_TAG + " WHERE "
                + KEY_TAG_ID + " = '" + tagId + "'";

        // get the cursor from the query
        Cursor cursor = db.rawQuery(selectTagQuery, null);
        Tag tag = null;

        if (cursor.moveToFirst()) {
            // get the values from the table
            String tagName = cursor.getString(cursor.getColumnIndex(KEY_TAG_NAME));
            List<Subscription> subscriptions = new ArrayList<>(getSubscriptionsByTag(tagId));
            int tagCount = getTagCount(tagId);
            tag = new Tag(tagName, subscriptions, tagId, tagCount);
        }

        // close the cursor and return the object
        cursor.close();
        return tag;
    }

    /**
     * Returns a tag from the database.
     *
     * @param tagName the name of the tag to retrieve
     * @return        the tag object
     */
    public Tag getTagByName(String tagName) {
        // get the database
        SQLiteDatabase db = this.getReadableDatabase();

        // create the query
        String selectTagQuery = "SELECT * FROM " + TABLE_TAG + " WHERE "
                + KEY_TAG_NAME + " = '" + tagName + "'";

        // get the cursor from the query
        Cursor cursor = db.rawQuery(selectTagQuery, null);
        Tag tag = null;

        if (cursor.moveToFirst()) {
            // get the values from the table
            String tagId = cursor.getString(cursor.getColumnIndex(KEY_TAG_ID));
            List<Subscription> subscriptions = new ArrayList<>(getSubscriptionsByTag(tagId));
            int tagCount = getTagCount(tagId);
            tag = new Tag(tagName, subscriptions, tagId, tagCount);
        }

        // close the cursor and return the object
        cursor.close();
        return tag;
    }

    /**
     * Returns a list composed of all entries from
     * the database.
     *
     * @return the list of entries
     */
    public List<Entry> getAllEntries() {
        // declare a list and create the query
        List<Entry> entries = new ArrayList<>();
        String selectQuery = "SELECT * FROM " + TABLE_ENTRY
                + " ORDER BY datetime(" + KEY_ENTRY_DATE + ") DESC";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                String entryId = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_ID));
                String entrySubId = cursor.getString(cursor.getColumnIndex(KEY_SUB_ID));
                String entryName = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_NAME));
                String entryDate = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_DATE));
                String entryTitle = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_TITLE));
                String entryContent = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_CONTENT));
                String entryAuthor = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_AUTHOR));
                String entryUrl = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_URL));
                String entryEnclosure = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_ENCLOSURE));
                boolean isRead = cursor.getInt(cursor.getColumnIndex(KEY_ENTRY_READ)) != 0;
                boolean isStarred = cursor.getInt(cursor.getColumnIndex(KEY_ENTRY_STAR)) != 0;

                Entry entry = new Entry(entryId, entrySubId, entryName, entryDate, entryTitle, entryContent, entryAuthor, entryUrl,
                        entryEnclosure, isRead, isStarred);

                entries.add(entry);
            } while (cursor.moveToNext());
        }

        // close the cursor and return the list
        cursor.close();
        return entries;
    }

    /**
     * Returns a list composed of all entries from
     * the database.
     *
     * @return the list of entries
     */
    public List<Entry> getAllEntries(String date, int limit) {
        // declare a list and create the query
        List<Entry> entries = new ArrayList<>();
        String selectQuery;

        if (date != null) {
            selectQuery = "SELECT * FROM " + TABLE_ENTRY
                    + " WHERE " + KEY_ENTRY_DATE + " < '" + date
                    + "' ORDER BY datetime(" + KEY_ENTRY_DATE + ") DESC "
                    + "LIMIT " + limit;
        } else {
            selectQuery = "SELECT * FROM " + TABLE_ENTRY
                    + " ORDER BY datetime(" + KEY_ENTRY_DATE + ") DESC "
                    + "LIMIT " + limit;
        }

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        long startTime = SystemClock.elapsedRealtime();
        if (cursor.moveToFirst()) {
            do {
                String entryId = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_ID));
                String entrySubId = cursor.getString(cursor.getColumnIndex(KEY_SUB_ID));
                String entryName = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_NAME));
                String entryDate = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_DATE));
                String entryTitle = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_TITLE));
                String entryContent = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_CONTENT));
                String entryAuthor = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_AUTHOR));
                String entryUrl = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_URL));
                String entryEnclosure = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_ENCLOSURE));
                boolean isRead = cursor.getInt(cursor.getColumnIndex(KEY_ENTRY_READ)) != 0;
                boolean isStarred = cursor.getInt(cursor.getColumnIndex(KEY_ENTRY_STAR)) != 0;

                Log.e("Title", entryTitle);

                Entry entry = new Entry(entryId, entrySubId, entryName, entryDate, entryTitle, entryContent, entryAuthor, entryUrl,
                        entryEnclosure, isRead, isStarred);

                entries.add(entry);
            } while (cursor.moveToNext());
        }

        long elapsedTime = (SystemClock.elapsedRealtime() - startTime) / 1000;
        Log.e("Database Time", String.valueOf(elapsedTime));

        // close the cursor and return the list
        cursor.close();
        return entries;
    }

    /**
     * Returns a list composed of all subscriptions from
     * the database.
     *
     * @return the list of subscriptions
     */
    public List<Subscription> getAllSubscriptions() {
        // declare the list and create the query
        List<Subscription> subscriptions = new ArrayList<>();
        String selectQuery = "SELECT * FROM " + TABLE_SUB
                + " ORDER BY " + KEY_SUB_NAME + " ASC";

        // set the database
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // loop through all rows and get the values
        if (cursor.moveToFirst()) {
            do {
                String subId = cursor.getString(cursor.getColumnIndex(KEY_SUB_ID));
                String subName = cursor.getString(cursor.getColumnIndex(KEY_SUB_NAME));
                String subUrl = cursor.getString(cursor.getColumnIndex(KEY_SUB_URL));
                int subCount = getSubscriptionCount(subId);
                byte[] image = cursor.getBlob(cursor.getColumnIndex(KEY_SUB_ICON));
                Bitmap subIcon = null;

                if (image.length > 0) {
                    subIcon = BitmapFactory.decodeByteArray(image, 0, image.length);
                }

                Subscription subscription = new Subscription(subId, subName, subUrl, subCount, subIcon);
                subscriptions.add(subscription);
            } while (cursor.moveToNext());
        }

        // close the cursor and return the list
        cursor.close();
        return subscriptions;
    }

    /**
     * Returns a list composed of all tags from
     * the database.
     *
     * @return the list of tags
     */
    public List<Tag> getAllTags() {
        // declare the list and create the query
        List<Tag> tags = new ArrayList<>();
        String selectQuery = "SELECT * FROM " + TABLE_TAG
                + " ORDER BY " + KEY_TAG_NAME + " ASC";

        // get the database
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // loop through and get the values
        if (cursor.moveToFirst()) {
            do {
                String tagName = cursor.getString(cursor.getColumnIndex(KEY_TAG_NAME));
                String tagId = cursor.getString(cursor.getColumnIndex(KEY_TAG_ID));
                List<Subscription> subscriptions = new ArrayList<>(getSubscriptionsByTag(tagId));
                int tagCount = getTagCount(tagId);

                Tag tag = new Tag(tagName, subscriptions, tagId, tagCount);
                tags.add(tag);
            } while (cursor.moveToNext());
        }

        // close the cursor and return the list
        cursor.close();
        return tags;
    }

    /**
     * Returns a list composed of all unread entries
     * from the database.
     *
     * @return the list of unread entries
     */
    public List<Entry> getUnreadEntries() {
        // declare the list and create the query
        List<Entry> entries = new ArrayList<>();
        String selectQuery = "SELECT * FROM " + TABLE_ENTRY
                + " WHERE " + KEY_ENTRY_READ + " = " + 0
                + " ORDER BY datetime(" + KEY_ENTRY_DATE + ") DESC";

        // get the database
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                String entryId = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_ID));
                String entrySubId = cursor.getString(cursor.getColumnIndex(KEY_SUB_ID));
                String entryName = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_NAME));
                String entryDate = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_DATE));
                String entryTitle = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_TITLE));
                String entryContent = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_CONTENT));
                String entryAuthor = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_AUTHOR));
                String entryUrl = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_URL));
                String entryEnclosure = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_ENCLOSURE));
                boolean isRead = cursor.getInt(cursor.getColumnIndex(KEY_ENTRY_READ)) != 0;
                boolean isStarred = cursor.getInt(cursor.getColumnIndex(KEY_ENTRY_STAR)) != 0;

                Entry entry = new Entry(entryId, entrySubId, entryName, entryDate, entryTitle, entryContent, entryAuthor, entryUrl,
                        entryEnclosure, isRead, isStarred);

                entries.add(entry);
            } while (cursor.moveToNext());
        }

        // close the cursor and return the list
        cursor.close();
        return entries;
    }

    /**
     * Returns a list composed of all unread entries
     * from the database.
     *
     * @return the list of unread entries
     */
    public List<Entry> getUnreadEntries(String date, int limit) {
        // declare the list and create the query
        List<Entry> entries = new ArrayList<>();
        String selectQuery;

        if (date != null) {
            selectQuery = "SELECT * FROM " + TABLE_ENTRY
                    + " WHERE " + KEY_ENTRY_READ + " = " + 0
                    + " AND " + KEY_ENTRY_DATE + " < '" + date
                    + "' ORDER BY datetime(" + KEY_ENTRY_DATE + ") DESC"
                    + " LIMIT " + limit;
        } else {
            selectQuery = "SELECT * FROM " + TABLE_ENTRY
                    + " WHERE " + KEY_ENTRY_READ + " = " + 0
                    + " ORDER BY datetime(" + KEY_ENTRY_DATE + ") DESC"
                    + " LIMIT " + limit;
        }

        // get the database
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                String entryId = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_ID));
                String entrySubId = cursor.getString(cursor.getColumnIndex(KEY_SUB_ID));
                String entryName = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_NAME));
                String entryDate = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_DATE));
                String entryTitle = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_TITLE));
                String entryContent = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_CONTENT));
                String entryAuthor = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_AUTHOR));
                String entryUrl = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_URL));
                String entryEnclosure = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_ENCLOSURE));
                boolean isRead = cursor.getInt(cursor.getColumnIndex(KEY_ENTRY_READ)) != 0;
                boolean isStarred = cursor.getInt(cursor.getColumnIndex(KEY_ENTRY_STAR)) != 0;

                Entry entry = new Entry(entryId, entrySubId, entryName, entryDate, entryTitle, entryContent, entryAuthor, entryUrl,
                        entryEnclosure, isRead, isStarred);

                entries.add(entry);
            } while (cursor.moveToNext());
        }

        // close the cursor and return the list
        cursor.close();
        return entries;
    }

    /**
     * Returns a list composed of all starred entries
     * from the database.
     *
     * @return the list of starred entries
     */
    public List<Entry> getStarredEntries() {
        // declare the list and create the query
        List<Entry> entries = new ArrayList<>();
        String selectQuery = "SELECT * FROM " + TABLE_ENTRY
                + " WHERE " + KEY_ENTRY_STAR + " != " + 0
                + " ORDER BY datetime(" + KEY_ENTRY_DATE + ") DESC";

        // get the database
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                String entryId = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_ID));
                String entrySubId = cursor.getString(cursor.getColumnIndex(KEY_SUB_ID));
                String entryName = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_NAME));
                String entryDate = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_DATE));
                String entryTitle = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_TITLE));
                String entryContent = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_CONTENT));
                String entryAuthor = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_AUTHOR));
                String entryUrl = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_URL));
                String entryEnclosure = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_ENCLOSURE));
                boolean isRead = cursor.getInt(cursor.getColumnIndex(KEY_ENTRY_READ)) != 0;
                boolean isStarred = cursor.getInt(cursor.getColumnIndex(KEY_ENTRY_STAR)) != 0;

                Entry entry = new Entry(entryId, entrySubId, entryName, entryDate, entryTitle, entryContent, entryAuthor, entryUrl,
                        entryEnclosure, isRead, isStarred);

                entries.add(entry);
            } while (cursor.moveToNext());
        }

        // close the cursor and return the list
        cursor.close();
        return entries;
    }

    /**
     * Returns a list composed of all starred entries
     * from the database.
     *
     * @return the list of starred entries
     */
    public List<Entry> getStarredEntries(String date, int limit) {
        // declare the list and create the query
        List<Entry> entries = new ArrayList<>();
        String selectQuery;

        if (date != null) {
            selectQuery = "SELECT * FROM " + TABLE_ENTRY
                    + " WHERE " + KEY_ENTRY_STAR + " != " + 0
                    + " AND " + KEY_ENTRY_DATE + " < '" + date
                    + "' ORDER BY datetime(" + KEY_ENTRY_DATE + ") DESC"
                    + " LIMIT " + limit;
        } else {
            selectQuery = "SELECT * FROM " + TABLE_ENTRY
                    + " WHERE " + KEY_ENTRY_STAR + " != " + 0
                    + " ORDER BY datetime(" + KEY_ENTRY_DATE + ") DESC"
                    + " LIMIT " + limit;
        }

        // get the database
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                String entryId = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_ID));
                String entrySubId = cursor.getString(cursor.getColumnIndex(KEY_SUB_ID));
                String entryName = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_NAME));
                String entryDate = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_DATE));
                String entryTitle = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_TITLE));
                String entryContent = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_CONTENT));
                String entryAuthor = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_AUTHOR));
                String entryUrl = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_URL));
                String entryEnclosure = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_ENCLOSURE));
                boolean isRead = cursor.getInt(cursor.getColumnIndex(KEY_ENTRY_READ)) != 0;
                boolean isStarred = cursor.getInt(cursor.getColumnIndex(KEY_ENTRY_STAR)) != 0;

                Entry entry = new Entry(entryId, entrySubId, entryName, entryDate, entryTitle, entryContent, entryAuthor, entryUrl,
                        entryEnclosure, isRead, isStarred);

                entries.add(entry);
            } while (cursor.moveToNext());
        }

        // close the cursor and return the list
        cursor.close();
        return entries;
    }

    /**
     * Returns a list composed of entries based on a
     * subscription from the database.
     *
     * @param subId the ID of the subscription
     * @return      the list of entries based on the subscription
     */
    public List<Entry> getEntriesBySub(String subId) {
        // declare the list and create the query
        List<Entry> entries = new ArrayList<>();
        String selectQuery = "SELECT * FROM " + TABLE_ENTRY
                + " WHERE " + KEY_SUB_ID + " = '" + subId
                + "' ORDER BY datetime(" + KEY_ENTRY_DATE + ") DESC";

        // get the database
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                String entryId = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_ID));
                String entryName = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_NAME));
                String entryDate = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_DATE));
                String entryTitle = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_TITLE));
                String entryContent = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_CONTENT));
                String entryAuthor = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_AUTHOR));
                String entryUrl = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_URL));
                String entryEnclosure = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_ENCLOSURE));
                boolean isRead = cursor.getInt(cursor.getColumnIndex(KEY_ENTRY_READ)) != 0;
                boolean isStarred = cursor.getInt(cursor.getColumnIndex(KEY_ENTRY_STAR)) != 0;

                Entry entry = new Entry(entryId, subId, entryName, entryDate, entryTitle, entryContent, entryAuthor, entryUrl,
                        entryEnclosure, isRead, isStarred);

                entries.add(entry);
            } while (cursor.moveToNext());
        }

        // close the cursor and return the list
        cursor.close();
        return entries;
    }

    /**
     * Returns a list composed of entries based on a
     * subscription from the database.
     *
     * @param subId the ID of the subscription
     * @return      the list of entries based on the subscription
     */
    public List<Entry> getEntriesBySub(String subId, String date, int limit) {
        // declare the list and create the query
        List<Entry> entries = new ArrayList<>();
        String selectQuery;

        if (date != null) {
            selectQuery = "SELECT * FROM " + TABLE_ENTRY
                    + " WHERE " + KEY_SUB_ID + " = '" + subId
                    + "' AND " + KEY_ENTRY_DATE + " < '" + date
                    + "' ORDER BY datetime(" + KEY_ENTRY_DATE + ") DESC "
                    + "LIMIT " + limit;
        } else {
            selectQuery = "SELECT * FROM " + TABLE_ENTRY
                    + " WHERE " + KEY_SUB_ID + " = '" + subId
                    + "' ORDER BY datetime(" + KEY_ENTRY_DATE + ") DESC "
                    + "LIMIT " + limit;
        }

        // get the database
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                String entryId = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_ID));
                String entryName = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_NAME));
                String entryDate = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_DATE));
                String entryTitle = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_TITLE));
                String entryContent = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_CONTENT));
                String entryAuthor = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_AUTHOR));
                String entryUrl = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_URL));
                String entryEnclosure = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_ENCLOSURE));
                boolean isRead = cursor.getInt(cursor.getColumnIndex(KEY_ENTRY_READ)) != 0;
                boolean isStarred = cursor.getInt(cursor.getColumnIndex(KEY_ENTRY_STAR)) != 0;

                Entry entry = new Entry(entryId, subId, entryName, entryDate, entryTitle, entryContent, entryAuthor, entryUrl,
                        entryEnclosure, isRead, isStarred);

                entries.add(entry);
            } while (cursor.moveToNext());
        }

        // close the cursor and return the list
        cursor.close();
        return entries;
    }

    /**
     * Returns a list composed of entries based on a
     * tag from the database.
     *
     * @param tagId the ID of the tag
     * @return      the list of entries based on the tag
     */
    public List<Entry> getEntriesByTag(String tagId) {
        // declare the list and create the query
        List<Entry> entries = new ArrayList<>();
        String selectQuery = "SELECT * FROM " + TABLE_ENTRY + " WHERE "
                + KEY_SUB_ID + " IN (SELECT "
                + KEY_SUB_ID + " FROM " + TABLE_CATEGORIZED_AS + " WHERE "
                + KEY_TAG_ID + " = '" + tagId + "') ORDER BY datetime("
                + KEY_ENTRY_DATE + ") DESC";

        // get the database
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                String entryId = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_ID));
                String subId = cursor.getString(cursor.getColumnIndex(KEY_SUB_ID));
                String entryName = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_NAME));
                String entryDate = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_DATE));
                String entryTitle = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_TITLE));
                String entryContent = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_CONTENT));
                String entryAuthor = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_AUTHOR));
                String entryUrl = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_URL));
                String entryEnclosure = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_ENCLOSURE));
                boolean isRead = cursor.getInt(cursor.getColumnIndex(KEY_ENTRY_READ)) != 0;
                boolean isStarred = cursor.getInt(cursor.getColumnIndex(KEY_ENTRY_STAR)) != 0;

                Entry entry = new Entry(entryId, subId, entryName, entryDate, entryTitle, entryContent, entryAuthor, entryUrl,
                        entryEnclosure, isRead, isStarred);

                entries.add(entry);
            } while (cursor.moveToNext());
        }

        // close the cursor and return the list
        cursor.close();
        return entries;
    }

    /**
     * Returns a list composed of entries based on a
     * tag from the database.
     *
     * @param tagId the ID of the tag
     * @return      the list of entries based on the tag
     */
    public List<Entry> getEntriesByTag(String tagId, String date, int limit) {
        // declare the list and create the query
        List<Entry> entries = new ArrayList<>();
        String selectQuery;

        if (date != null) {
            selectQuery = "SELECT * FROM " + TABLE_ENTRY + " WHERE "
                    + KEY_SUB_ID + " IN (SELECT "
                    + KEY_SUB_ID + " FROM " + TABLE_CATEGORIZED_AS + " WHERE "
                    + KEY_TAG_ID + " = '" + tagId + "')"
                    + " AND " + KEY_ENTRY_DATE + " < '" + date
                    + "' ORDER BY datetime("
                    + KEY_ENTRY_DATE + ") DESC LIMIT " + limit;
        } else {
            selectQuery = "SELECT * FROM " + TABLE_ENTRY + " WHERE "
                    + KEY_SUB_ID + " IN (SELECT "
                    + KEY_SUB_ID + " FROM " + TABLE_CATEGORIZED_AS + " WHERE "
                    + KEY_TAG_ID + " = '" + tagId + "')"
                    + " ORDER BY datetime("
                    + KEY_ENTRY_DATE + ") DESC LIMIT " + limit;
        }

        // get the database
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                String entryId = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_ID));
                String subId = cursor.getString(cursor.getColumnIndex(KEY_SUB_ID));
                String entryName = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_NAME));
                String entryDate = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_DATE));
                String entryTitle = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_TITLE));
                String entryContent = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_CONTENT));
                String entryAuthor = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_AUTHOR));
                String entryUrl = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_URL));
                String entryEnclosure = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_ENCLOSURE));
                boolean isRead = cursor.getInt(cursor.getColumnIndex(KEY_ENTRY_READ)) != 0;
                boolean isStarred = cursor.getInt(cursor.getColumnIndex(KEY_ENTRY_STAR)) != 0;

                Entry entry = new Entry(entryId, subId, entryName, entryDate, entryTitle, entryContent, entryAuthor, entryUrl,
                        entryEnclosure, isRead, isStarred);

                entries.add(entry);
            } while (cursor.moveToNext());
        }

        // close the cursor and return the list
        cursor.close();
        return entries;
    }

    /**
     * Returns a list composed of entries that match a given
     * query from the database.
     *
     * @param query the query to match against
     * @return      the list of entries matching the query
     */
    public List<Entry> getEntriesByQuery(String query) {
        // declare the list and create the query
        List<Entry> entries = new ArrayList<>();
        query = query.replace("'", "''");

        String selectQuery = "SELECT * FROM " + TABLE_ENTRY + " WHERE "
                + KEY_ENTRY_NAME + " LIKE '%" + query + "%' OR "
                + KEY_ENTRY_TITLE + " LIKE '%" + query + "%' OR "
                + KEY_ENTRY_CONTENT + " LIKE '%" + query + "%' OR "
                + KEY_ENTRY_AUTHOR + " LIKE '%" + query + "%'"
                + " ORDER BY datetime(" + KEY_ENTRY_DATE + ") DESC";

        // get the database
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                String entryId = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_ID));
                String entrySubId = cursor.getString(cursor.getColumnIndex(KEY_SUB_ID));
                String entryName = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_NAME));
                String entryDate = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_DATE));
                String entryTitle = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_TITLE));
                String entryContent = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_CONTENT));
                String entryAuthor = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_AUTHOR));
                String entryUrl = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_URL));
                String entryEnclosure = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_ENCLOSURE));
                boolean isRead = cursor.getInt(cursor.getColumnIndex(KEY_ENTRY_READ)) != 0;
                boolean isStarred = cursor.getInt(cursor.getColumnIndex(KEY_ENTRY_STAR)) != 0;

                Entry entry = new Entry(entryId, entrySubId, entryName, entryDate, entryTitle, entryContent, entryAuthor, entryUrl,
                        entryEnclosure, isRead, isStarred);

                entries.add(entry);
            } while (cursor.moveToNext());
        }

        // close the cursor and return the list
        cursor.close();
        return entries;
    }

    /**
     * Returns a list composed of entries that match a given
     * query from the database.
     *
     * @param query the query to match against
     * @return      the list of entries matching the query
     */
    public List<Entry> getEntriesByQuery(String query, String date, int limit) {
        // declare the list and create the query
        List<Entry> entries = new ArrayList<>();
        query = query.replace("'", "''");
        String selectQuery;

        if (date != null) {
            selectQuery = "SELECT * FROM " + TABLE_ENTRY + " WHERE ("
                    + KEY_ENTRY_TITLE + " LIKE '% " + query + " %' OR "
                    + KEY_ENTRY_TITLE + " LIKE '" + query + " %' OR "
                    + KEY_ENTRY_TITLE + " LIKE '% " + query + "' OR "
                    + KEY_ENTRY_CONTENT + " LIKE '% " + query + " %' OR "
                    + KEY_ENTRY_CONTENT + " LIKE '" + query + " %' OR "
                    + KEY_ENTRY_CONTENT + " LIKE '% " + query + "' OR "
                    + KEY_ENTRY_AUTHOR + " LIKE '% " + query + " %' OR "
                    + KEY_ENTRY_AUTHOR + " LIKE '" + query + " %' OR "
                    + KEY_ENTRY_AUTHOR + " LIKE '% " + query + "')"
                    + " AND " + KEY_ENTRY_DATE + " < '" + date
                    + "' ORDER BY datetime(" + KEY_ENTRY_DATE + ") DESC "
                    + "LIMIT " + limit;
        } else {
            selectQuery = "SELECT * FROM " + TABLE_ENTRY + " WHERE "
                    + KEY_ENTRY_TITLE + " LIKE '% " + query + " %' OR "
                    + KEY_ENTRY_TITLE + " LIKE '" + query + " %' OR "
                    + KEY_ENTRY_TITLE + " LIKE '% " + query + "' OR "
                    + KEY_ENTRY_CONTENT + " LIKE '% " + query + " %' OR "
                    + KEY_ENTRY_CONTENT + " LIKE '" + query + " %' OR "
                    + KEY_ENTRY_CONTENT + " LIKE '% " + query + "' OR "
                    + KEY_ENTRY_AUTHOR + " LIKE '% " + query + " %' OR "
                    + KEY_ENTRY_AUTHOR + " LIKE '" + query + " %' OR "
                    + KEY_ENTRY_AUTHOR + " LIKE '% " + query + "'"
                    + " ORDER BY datetime(" + KEY_ENTRY_DATE + ") DESC "
                    + "LIMIT " + limit;
        }

        // get the database
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                String entryId = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_ID));
                String entrySubId = cursor.getString(cursor.getColumnIndex(KEY_SUB_ID));
                String entryName = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_NAME));
                String entryDate = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_DATE));
                String entryTitle = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_TITLE));
                String entryContent = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_CONTENT));
                String entryAuthor = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_AUTHOR));
                String entryUrl = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_URL));
                String entryEnclosure = cursor.getString(cursor.getColumnIndex(KEY_ENTRY_ENCLOSURE));
                boolean isRead = cursor.getInt(cursor.getColumnIndex(KEY_ENTRY_READ)) != 0;
                boolean isStarred = cursor.getInt(cursor.getColumnIndex(KEY_ENTRY_STAR)) != 0;

                Entry entry = new Entry(entryId, entrySubId, entryName, entryDate, entryTitle, entryContent, entryAuthor, entryUrl,
                        entryEnclosure, isRead, isStarred);

                entries.add(entry);
            } while (cursor.moveToNext());
        }

        // close the cursor and return the list
        cursor.close();
        return entries;
    }

    /**
     * Returns a list composed of subscriptions based on a
     * tag from the database.
     *
     * @param tagId the ID of the tag
     * @return      the list of subscriptions based on the tag
     */
    public List<Subscription> getSubscriptionsByTag(String tagId) {
        // create the selection query
        String selectQuery = "SELECT * FROM " + TABLE_SUB + " WHERE "
                + KEY_SUB_ID + " IN (SELECT "
                + KEY_SUB_ID + " FROM " + TABLE_CATEGORIZED_AS + " WHERE "
                + KEY_TAG_ID + " = '" + tagId + "') ORDER BY " + KEY_SUB_NAME + " ASC";

        // get the database and cursor
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        List<Subscription> subscriptions = new ArrayList<>();

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                String subId = cursor.getString(cursor.getColumnIndex(KEY_SUB_ID));
                String subName = cursor.getString(cursor.getColumnIndex(KEY_SUB_NAME));
                String subUrl = cursor.getString(cursor.getColumnIndex(KEY_SUB_URL));
                int subCount = getSubscriptionCount(subId);
                byte[] image = cursor.getBlob(cursor.getColumnIndex(KEY_SUB_ICON));
                Bitmap subIcon = null;

                if (image.length > 0) {
                    subIcon = BitmapFactory.decodeByteArray(image, 0, image.length);
                }

                Subscription subscription = new Subscription(subId, subName, subUrl, subCount, subIcon);
                subscriptions.add(subscription);
            } while (cursor.moveToNext());
        }

        // close the cursor and return the list
        cursor.close();
        return subscriptions;
    }

    /**
     * Returns a list composed of untagged subscriptions
     * from the database.
     *
     * @return the list of untagged subscriptions
     */
    public List<Subscription> getUntaggedSubscriptions() {
        // create the selection query
        String selectQuery = "SELECT * FROM " + TABLE_SUB + " WHERE "
                + KEY_SUB_ID + " NOT IN (SELECT "
                + KEY_SUB_ID + " FROM " + TABLE_CATEGORIZED_AS + ") ORDER BY "
                + KEY_SUB_NAME + " ASC";

        // get the database and cursor
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        List<Subscription> subscriptions = new ArrayList<>();

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                String subId = cursor.getString(cursor.getColumnIndex(KEY_SUB_ID));
                String subName = cursor.getString(cursor.getColumnIndex(KEY_SUB_NAME));
                String subUrl = cursor.getString(cursor.getColumnIndex(KEY_SUB_URL));
                int subCount = getSubscriptionCount(subId);
                byte[] image = cursor.getBlob(cursor.getColumnIndex(KEY_SUB_ICON));
                Bitmap subIcon = null;

                if (image.length > 0) {
                    subIcon = BitmapFactory.decodeByteArray(image, 0, image.length);
                }

                Subscription subscription = new Subscription(subId, subName, subUrl, subCount, subIcon);
                subscriptions.add(subscription);
            } while (cursor.moveToNext());
        }

        // close the cursor and return the list
        cursor.close();
        return subscriptions;
    }

    /**
     * Returns a list composed of tags based on a
     * subscription from the database.
     *
     * @param subId the ID of the subscription
     * @return      the list of tags based on the subscription
     */
    public List<Tag> getTagsBySubscription(String subId) {
        // create the selection query
        String selectQuery = "SELECT * FROM " + TABLE_TAG + " WHERE "
                + KEY_TAG_ID + " IN (SELECT "
                + KEY_TAG_ID + " FROM " + TABLE_CATEGORIZED_AS + " WHERE "
                + KEY_SUB_ID + " = '" + subId + "') ORDER BY " + KEY_TAG_NAME + " ASC";

        // get the database and cursor
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        List<Tag> tags = new ArrayList<>();

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                String tagId = cursor.getString(cursor.getColumnIndex(KEY_TAG_ID));
                String tagName = cursor.getString(cursor.getColumnIndex(KEY_TAG_NAME));
                List<Subscription> subscriptions = new ArrayList<>(getSubscriptionsByTag(tagId));
                int tagCount = getTagCount(tagId);

                Tag tag = new Tag(tagName, subscriptions, tagId, tagCount);
                tags.add(tag);
            } while (cursor.moveToNext());
        }

        // close the cursor and return the list
        cursor.close();
        return tags;
    }

    /**
     * Returns the number of unread entries within the
     * database.
     *
     * @return the number of unread entries
     */
    public int getUnreadCount() {
        // create the selection query
        String selectQuery = "SELECT * FROM " + TABLE_ENTRY
                + " WHERE " + KEY_ENTRY_READ + " = " + 0
                + " ORDER BY datetime(" + KEY_ENTRY_DATE + ") DESC";

        // get the database and cursor
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // close the cursor and return the count
        int count = cursor.getCount();
        cursor.close();
        return count;
    }

    /**
     * Returns the number of starred entries within the
     * database.
     *
     * @return the number of starred entries
     */
    public int getStarredCount() {
        // create the selection query
        String selectQuery = "SELECT * FROM " + TABLE_ENTRY
                + " WHERE " + KEY_ENTRY_STAR + " != " + 0
                + " ORDER BY datetime(" + KEY_ENTRY_DATE + ") DESC";

        // get the database and cursor
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // close the cursor and return the count
        int count = cursor.getCount();
        cursor.close();
        return count;
    }

    /**
     * Returns the number of unread entries belonging to
     * a subscription within the database.
     *
     * @param subId the ID of the subscription
     * @return      the number of unread entries
     */
    public int getSubscriptionCount(String subId) {
        // create the selection query
        String selectQuery = "SELECT * FROM " + TABLE_ENTRY
                + " WHERE " + KEY_SUB_ID + " = '" + subId + "' AND "
                + KEY_ENTRY_READ + " = " + 0;

        // get the database and cursor
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // close the cursor and return the count
        int count = cursor.getCount();
        cursor.close();
        return count;
    }

    /**
     * Returns the number of unread entries belonging to
     * a tag within the database.
     *
     * @param tagId the ID of the tag
     * @return      the number of unread entries
     */
    public int getTagCount(String tagId) {
        // create the selection query
        String selectQuery = "SELECT * FROM " + TABLE_ENTRY + " WHERE "
                + KEY_SUB_ID + " IN (SELECT " + KEY_SUB_ID + " FROM "
                + TABLE_CATEGORIZED_AS + " WHERE " + KEY_TAG_ID + " = '" + tagId + "') AND "
                + KEY_ENTRY_READ + " = " + 0;

        // get the database and cursor
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // close the cursor and return the count
        int count = cursor.getCount();
        cursor.close();
        return count;
    }

    /**
     * Updates a preexisting entry within the database.
     *
     * @param entry the updated entry
     * @return      the status of the update
     */
    public int updateEntry(Entry entry) {
        // get the database
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        // set the values in the table
        values.put(KEY_ENTRY_ID, entry.getEntryId());
        values.put(KEY_ENTRY_NAME, entry.getEntryName());
        values.put(KEY_ENTRY_DATE, entry.getEntryDate());
        values.put(KEY_ENTRY_TITLE, entry.getEntryTitle());
        values.put(KEY_ENTRY_CONTENT, entry.getEntryContent());
        values.put(KEY_ENTRY_AUTHOR, entry.getEntryAuthor());
        values.put(KEY_ENTRY_URL, entry.getEntryUrl());
        values.put(KEY_ENTRY_ENCLOSURE, entry.getEntryEnclosure());
        values.put(KEY_ENTRY_READ, entry.isRead());
        values.put(KEY_ENTRY_STAR, entry.isStarred());
        values.put(KEY_SUB_ID, entry.getSubId());

        // update the row
        return db.update(TABLE_ENTRY, values, KEY_ENTRY_ID + " = ?",
                new String[] { entry.getEntryId() });
    }

    /**
     * Updates a preexisting subscription within the database.
     *
     * @param subscription the updated subscription
     * @return             the status of the update
     */
    public int updateSubscription(Subscription subscription) {
        // get the database
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        // set the values in the table
        values.put(KEY_SUB_ID, subscription.getSubId());
        values.put(KEY_SUB_NAME, subscription.getSubName());
        values.put(KEY_SUB_URL, subscription.getSubUrl());
        values.put(KEY_SUB_COUNT, subscription.getSubCount());

        // get the subscription's entries
        List<Entry> entries = getEntriesBySub(subscription.getSubId());

        // update the names of the subscription's entries
        for (int i = 0; i < entries.size(); i++) {
            Entry entry = entries.get(i);
            entry.setEntryName(subscription.getSubName());
            updateEntry(entry);
        }

        // update the row
        return db.update(TABLE_SUB, values, KEY_SUB_ID + " = ?",
                new String[] { subscription.getSubId() });
    }

    /**
     * Updates a preexisting tag within the database.
     *
     * @param tag the updated entry
     * @return    the status of the update
     */
    public int updateTag(Tag tag) {
        // get the database
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        // set the values in the table
        values.put(KEY_TAG_ID, tag.getTagId());
        values.put(KEY_TAG_NAME, tag.getTitle());
        values.put(KEY_TAG_COUNT, tag.getTagCount());

        // update the row
        return db.update(TABLE_TAG, values, KEY_TAG_ID + " = ?",
                new String[] { tag.getTagId() });
    }

    /**
     * Deletes an entry from the database.
     *
     * @param entryId the ID of the entry to delete
     * @return        the status of the deletion
     */
    public int deleteEntry(String entryId) {
        // get the database
        SQLiteDatabase db = this.getWritableDatabase();

        // delete the row
        return db.delete(TABLE_ENTRY, KEY_ENTRY_ID + " = ?",
                new String[] { String.valueOf(entryId) });
    }

    /**
     * Deletes a subscription from the database.
     *
     * @param subId the ID of the subscription to delete
     * @return      the status of the deletion
     */
    public int deleteSubscription(String subId) {
        // get the database
        SQLiteDatabase db = this.getWritableDatabase();
        deleteEntriesBySubscription(subId);

        // create the query
        String query = "DELETE FROM " + TABLE_CATEGORIZED_AS
                + " WHERE " + KEY_SUB_ID + " = '" + subId + "'";

        // delete relationship between subscription and tag
        db.execSQL(query);

        // delete the row
        return db.delete(TABLE_SUB, KEY_SUB_ID + " = ?",
                new String[] { String.valueOf(subId) });
    }

    /**
     * Deletes a relationship between a tag and a
     * subscription from the database.
     *
     * @param tagId the ID of the tag
     * @param subId the ID of the subscription
     */
    public void deleteCategorizedAs(String tagId, String subId) {
        // get the database
        SQLiteDatabase db = this.getWritableDatabase();

        // create the query
        String query = "DELETE FROM " + TABLE_CATEGORIZED_AS
                + " WHERE " + KEY_TAG_ID + " = '" + tagId + "' AND "
                + KEY_SUB_ID + " = '" + subId + "'";

        // delete the relationships
        db.execSQL(query);
    }

    /**
     * Deletes a tag from the database.
     *
     * @param tagId the ID of the tag to delete
     * @return      the status of the deletion
     */
    public int deleteTag(String tagId) {
        // get the database
        SQLiteDatabase db = this.getWritableDatabase();

        // create the query
        String query = "DELETE FROM " + TABLE_CATEGORIZED_AS
                + " WHERE " + KEY_TAG_ID + " = '" + tagId + "'";

        // delete the relationship between the tag and subscriptions
        db.execSQL(query);

        // delete the row
        return db.delete(TABLE_TAG, KEY_TAG_ID + " = ?",
                new String[] { String.valueOf(tagId) });
    }

    /**
     * Deletes entries of a subscription from the database.
     *
     * @param subId the ID of the subscription
     */
    public void deleteEntriesBySubscription(String subId) {
        // get the database
        SQLiteDatabase db = this.getWritableDatabase();

        // create the query
        String query = "DELETE FROM " + TABLE_ENTRY
                + " WHERE " + KEY_SUB_ID + " = '" + subId + "'";

        // delete the rows
        db.execSQL(query);
    }

    /**
     * Determines whether an entry exists in the database.
     *
     * @param entryId the ID of the entry
     * @return        the boolean existence of the entry
     */
    public boolean containsEntry(String entryId) {
        // get the database
        SQLiteDatabase db = this.getWritableDatabase();

        // create the query
        String query = "SELECT * FROM " + TABLE_ENTRY
                + " WHERE " + KEY_ENTRY_ID + " = '" + entryId + "'";
        Cursor cursor = db.rawQuery(query, null);

        // close the cursor and return the value
        boolean doesContain = cursor.getCount() > 0;
        cursor.close();
        return doesContain;
    }

    /**
     * Determines whether a subscription exists
     * in the database.
     *
     * @param subId the ID of the subscription
     * @return      the boolean existence of the subscription
     */
    public boolean containsSubscription(String subId) {
        // get the database
        SQLiteDatabase db = this.getWritableDatabase();

        // create the query
        String query = "SELECT * FROM " + TABLE_SUB
                + " WHERE " + KEY_SUB_ID + " = '" + subId + "'";
        Cursor cursor = db.rawQuery(query, null);

        // close the cursor and return the value
        boolean doesContain = cursor.getCount() > 0;
        cursor.close();
        return doesContain;
    }

    /**
     * Determines whether a relationship between a
     * subscription and a tag exists in the database.
     *
     * @param tagId the ID of the tag
     * @param subId the ID of the subscription
     * @return      the boolean existence of the relationship
     */
    public boolean containsCategorizedAs(String tagId, String subId) {
        // get the database
        SQLiteDatabase db = this.getWritableDatabase();

        // create the query
        String query = "SELECT * FROM " + TABLE_CATEGORIZED_AS
                + " WHERE " + KEY_TAG_ID + " = '" + tagId + "' AND "
                + KEY_SUB_ID + " = '" + subId + "'";
        Cursor cursor = db.rawQuery(query, null);

        // close the cursor and return the value
        boolean doesContain = cursor.getCount() > 0;
        cursor.close();
        return doesContain;
    }

    /**
     * Determines whether a tag is related to any
     * subscription within the database.
     *
     * @param tagId the ID of the tag
     * @return      the boolean existence of the subscriptions
     */
    public boolean containsSubscriptionByTag(String tagId) {
        // get the database
        SQLiteDatabase db = this.getWritableDatabase();

        // create the query
        String query = "SELECT * FROM " + TABLE_SUB
                + " WHERE " + KEY_TAG_ID + " = '" + tagId + "'";
        Cursor cursor = db.rawQuery(query, null);

        // close the cursor and return the value
        boolean doesContain = cursor.getCount() > 0;
        cursor.close();
        return doesContain;
    }

    /**
     * Determines whether an tag exists in the database.
     *
     * @param tagId the ID of the tag
     * @return      the boolean existence of the tag
     */
    public boolean containsTag(String tagId) {
        // get the database
        SQLiteDatabase db = this.getWritableDatabase();

        // create the query
        String query = "SELECT * FROM " + TABLE_TAG
                + " WHERE " + KEY_TAG_ID + " = '" + tagId + "'";
        Cursor cursor = db.rawQuery(query, null);

        // close the cursor and return the value
        boolean doesContain = cursor.getCount() > 0;
        cursor.close();
        return doesContain;
    }

    /**
     * Determines whether a tag exists in the database.
     *
     * @param tagName the name of the tag
     * @return        the boolean existence of the tag
     */
    public boolean containsTagByName(String tagName) {
        // get the database
        SQLiteDatabase db = this.getWritableDatabase();

        // create the query
        String query = "SELECT * FROM " + TABLE_TAG
                + " WHERE " + KEY_TAG_NAME + " = '" + tagName + "'";
        Cursor cursor = db.rawQuery(query, null);

        // close the cursor and return the value
        boolean doesContain = cursor.getCount() > 0;
        cursor.close();
        return doesContain;
    }
}
