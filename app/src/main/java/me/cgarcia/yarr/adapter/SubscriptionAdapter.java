/* YARR (Yet Another RSS Reader)
 * Copyright (C) 2017  Logan Garcia
 *
 * This file is part of YARR.
 *
 * YARR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * YARR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with YARR.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.cgarcia.yarr.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import me.cgarcia.yarr.DefaultApplication;
import me.cgarcia.yarr.R;
import me.cgarcia.yarr.SharedPreferencesManager;
import me.cgarcia.yarr.activity.MainActivity;
import me.cgarcia.yarr.object.Subscription;

/**
 * RecyclerViewAdapter that manages the views and behaviors of
 * untagged subscriptions in a list.
 *
 * @author  Logan Garcia
 * @version %I%, %G%
 */
public class SubscriptionAdapter extends RecyclerView.Adapter<SubscriptionAdapter.SubscriptionHolder> {
    // declare the subscriptions, inflater, and callback
    private List<Subscription> mSubs;
    private LayoutInflater mInflater;
    private ItemClickCallback mItemClickCallback;
    private Context mContext;

    /**
     * The default constructor.
     *
     * @param subs    the list of subscriptions to manage
     * @param context the context of the activity
     */
    public SubscriptionAdapter(List<Subscription> subs, Context context) {
        mContext = context;
        mInflater = LayoutInflater.from(mContext);
        mSubs = subs;
    }

    @Override
    public SubscriptionHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.list_subscription_untagged, parent, false);
        return new SubscriptionHolder(view);
    }

    @Override
    public void onBindViewHolder(SubscriptionHolder holder, int position) {
        // set the icon
        Bitmap bitmap = mSubs.get(position).getSubIcon();
        holder.mIcon.setImageBitmap(DefaultApplication.formatBitmap(bitmap, mContext));

        // set the name
        holder.mName.setText(mSubs.get(position).getSubName());

        // set the unread count
        if (mSubs.get(position).getSubCount() == 0) {
            holder.mCount.setText("");
        } else {
            holder.mCount.setText(String.valueOf(mSubs.get(position).getSubCount()));
        }

        // set the highlighted/clicked subscription
        String subId = SharedPreferencesManager.getValue(
                SharedPreferencesManager.KEY_STRING_SUB_ID,
                SharedPreferencesManager.VALUE_STRING_SUB_ID_DEFAULT);
        holder.mContainer.setSelected(subId.equals(mSubs.get(position).getSubId()));
    }

    @Override
    public int getItemCount() {
        return mSubs.size();
    }

    /**
     * Interface that receives the position of a subscription.
     */
    public interface ItemClickCallback {
        void onUntaggedSubscriptionItemClick(int p);
    }

    /**
     * Sets the item click callback.
     *
     * @param itemClickCallback the item click callback
     */
    public void setItemClickCallback(final ItemClickCallback itemClickCallback) {
        mItemClickCallback = itemClickCallback;
    }

    /**
     * RecyclerViewHolder that receives and holds the view
     * of the untagged subscriptions.
     *
     * @author  Logan Garcia
     * @version %I%, %G%
     */
    class SubscriptionHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        // declare the view elements
        private ImageView mIcon;
        private TextView mName;
        private TextView mCount;
        private View mContainer;

        /**
         * The default constructor.
         *
         * @param itemView the current view
         */
        private SubscriptionHolder(View itemView) {
            super(itemView);

            // get the view elements
            mIcon = (ImageView)itemView.findViewById(R.id.list_subscription_untagged_icon);
            mName = (TextView)itemView.findViewById(R.id.list_subscription_untagged_name);
            mCount = (TextView)itemView.findViewById(R.id.list_subscription_untagged_unread);
            mContainer = itemView.findViewById(R.id.list_subscription_untagged_root);

            // set the listener
            mContainer.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.list_subscription_untagged_root) {
                // set the position based on the position
                // of the subscription
                mItemClickCallback.onUntaggedSubscriptionItemClick(getAdapterPosition());
            }
        }
    }
}
