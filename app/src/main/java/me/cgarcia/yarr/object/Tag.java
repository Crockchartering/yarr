/* YARR (Yet Another RSS Reader)
 * Copyright (C) 2017  Logan Garcia
 *
 * This file is part of YARR.
 *
 * YARR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * YARR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with YARR.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.cgarcia.yarr.object;

import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.List;

/**
 * Holds information related to a tag.
 *
 * @author  Logan Garcia
 * @version %I%, %G%
 */
public class Tag extends ExpandableGroup<Subscription> {

    private String mTagId;
    private int mTagCount;

    /**
     * The default constructor.
     *
     * @param tagName  the name of the tag
     * @param tagSubs  the subscriptions of the tag
     * @param tagId    the ID of the tag
     * @param tagCount the number of unread entries
     *                 of the tag
     */
    public Tag(String tagName, List<Subscription> tagSubs, String tagId, int tagCount) {
        super(tagName, tagSubs);
        mTagId = tagId;
        mTagCount = tagCount;
    }

    /**
     * Returns the ID of the tag.
     *
     * @return the ID of the tag
     */
    public String getTagId() {
        return mTagId;
    }

    /**
     * Returns the number of unread entries
     * of the tag.
     *
     * @return the number of unread entries
     *         of the tag
     */
    public int getTagCount() {
        return mTagCount;
    }

    /**
     * Sets the ID of the tag.
     *
     * @param tagId the ID of the tag
     */
    public void setTagId(String tagId) {
        mTagId = tagId;
    }

    /**
     * Sets the number of unread entries
     * of the tag.
     *
     * @param tagCount the number of unread entries
     *                 of the tag
     */
    public void setTagCount(int tagCount) {
        mTagCount = tagCount;
    }
}
