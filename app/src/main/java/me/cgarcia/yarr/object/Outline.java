/* YARR (Yet Another RSS Reader)
 * Copyright (C) 2017  Logan Garcia
 *
 * This file is part of YARR.
 *
 * YARR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * YARR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with YARR.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.cgarcia.yarr.object;

/**
 * Holds information related to an outline element
 * of an OPML file.
 *
 * @author  Logan Garcia
 * @version %I%, %G%
 */
public class Outline {
    private String mTitle;
    private String mLink;

    /**
     * The default constructor.
     *
     * @param title the title of the outline
     * @param link  the atom link of the outline
     */
    public Outline(String title, String link) {
        mTitle = title;
        mLink = link;
    }

    /**
     * Returns the title of the outline.
     *
     * @return the title of the outline
     */
    public String getTitle() {
        return mTitle;
    }

    /**
     * Returns the atom link of the outline.
     *
     * @return the atom link of the outline
     */
    public String getLink() {
        return mLink;
    }
}
