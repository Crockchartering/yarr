/* YARR (Yet Another RSS Reader)
 * Copyright (C) 2017  Logan Garcia
 *
 * This file is part of YARR.
 *
 * YARR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * YARR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with YARR.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.cgarcia.yarr.object;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Holds information related to a subscription.
 *
 * @author  Logan Garcia
 * @version %I%, %G%
 */
public class Subscription implements Parcelable {

    private String mSubId;
    private String mSubName;
    private String mSubUrl;
    private int mSubCount;
    private Bitmap mSubIcon;

    /**
     * The default constructor.
     *
     * @param subId    the ID of the subscription
     * @param subName  the name of the subscription
     * @param subUrl   the URL of the subscription
     * @param subCount the number of the unread entries
     *                 of the subscription
     */
    public Subscription(String subId, String subName, String subUrl, int subCount, Bitmap subIcon) {
        mSubId = subId;
        mSubName = subName;
        mSubUrl = subUrl;
        mSubCount = subCount;
        mSubIcon = subIcon;
    }

    /**
     * Assigns values from a parcel.
     *
     * @param in the parcel to read from
     */
    private Subscription(Parcel in) {
        mSubId = in.readString();
        mSubName = in.readString();
        mSubUrl = in.readString();
        mSubCount = in.readInt();
    }

    /**
     * Sets up the subscription array from the
     * parcel.
     */
    public static final Creator<Subscription> CREATOR = new Creator<Subscription>() {
        @Override
        public Subscription createFromParcel(Parcel in) {
            return new Subscription(in);
        }

        @Override
        public Subscription[] newArray(int size) {
            return new Subscription[size];
        }
    };

    /**
     * Returns the ID of the subscription.
     *
     * @return the ID of the subscription
     */
    public String getSubId() { return mSubId; }

    /**
     * Returns the name of the subscription.
     *
     * @return the name of the subscription
     */
    public String getSubName() {
        return mSubName;
    }

    /**
     * Returns the URL of the subscription.
     *
     * @return the URL of the subscription
     */
    public String getSubUrl() {
        return mSubUrl;
    }

    /**
     * Returns the number of unread entries
     * of the subscription.
     *
     * @return the number of unread entries
     *         of the subscription
     */
    public int getSubCount() {
        return mSubCount;
    }

    /**
     * Returns the image associated with
     * the subscription
     *
     * @return the image associated with
     *         the subscription
     */
    public Bitmap getSubIcon() {
        return mSubIcon;
    }

    /**
     * Sets the ID of the subscription.
     *
     * @param subId the ID of the subscription
     */
    public void setSubId(String subId) {
        mSubId = subId;
    }

    /**
     * Sets the name of the subscription.
     *
     * @param subName the name of the subscription
     */
    public void setSubName(String subName) {
        mSubName = subName;
    }

    /**
     * Sets the URL of the subscription.
     *
     * @param subUrl the URL of the subscription
     */
    public void setSubUrl(String subUrl) {
        mSubUrl = subUrl;
    }

    /**
     * Sets the number of unread entries
     * of the subscription.
     *
     * @param subCount the number of unread entries
     *                 of the subscription
     */
    public void setSubCount(int subCount) {
        mSubCount = subCount;
    }

    /**
     * Sets the image associated with the
     * subscription
     *
     * @param subIcon the image associated with
     *                the subscription
     */
    public void setSubIcon(Bitmap subIcon) {
        mSubIcon = subIcon;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mSubId);
        dest.writeString(mSubName);
        dest.writeString(mSubUrl);
        dest.writeInt(mSubCount);
    }
}
