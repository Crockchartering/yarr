/* YARR (Yet Another RSS Reader)
 * Copyright (C) 2017  Logan Garcia
 *
 * This file is part of YARR.
 *
 * YARR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * YARR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with YARR.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.cgarcia.yarr.object;

/**
 * Holds information related to an entry.
 *
 * @author  Logan Garcia
 * @version %I%, %G%
 */
public class Entry {
    private String mEntryId;
    private String mSubId;
    private String mEntryName;
    private String mEntryDate;
    private String mEntryTitle;
    private String mEntryContent;
    private String mEntryAuthor;
    private String mEntryUrl;
    private String mEntryEnclosure;
    private boolean mIsRead;
    private boolean mIsStarred;

    /**
     * The default constructor.
     *
     * @param entryId        the ID of the entry
     * @param subId          the ID of the subscription
     * @param entryName      the name of the entry/subscription
     * @param entryDate      the date the entry was published
     * @param entryTitle     the title of the entry
     * @param entryContent   the main content of the entry
     * @param entryAuthor    the author of the entry
     * @param entryUrl       the URL to the original content
     * @param entryEnclosure the feed data enclosure of the entry
     * @param isRead         the read status of the entry
     * @param isStarred      the starred status of the entry
     */
    public Entry(String entryId, String subId, String entryName, String entryDate, String entryTitle, String entryContent, String entryAuthor,
                 String entryUrl, String entryEnclosure, boolean isRead, boolean isStarred) {
        mEntryId = entryId;
        mSubId = subId;
        mEntryName = entryName;
        mEntryDate = entryDate;
        mEntryTitle = entryTitle;
        mEntryContent = entryContent;
        mEntryAuthor = entryAuthor;
        mEntryUrl = entryUrl;
        mEntryEnclosure = entryEnclosure;
        mIsRead = isRead;
        mIsStarred = isStarred;
    }

    /**
     * Returns the ID of the entry.
     *
     * @return the ID of the entry
     */
    public String getEntryId() {
        return mEntryId;
    }

    /**
     * Returns the ID of the subscription.
     *
     * @return the ID of the subscription
     */
    public String getSubId() {
        return mSubId;
    }

    /**
     * Returns the name of the entry.
     *
     * @return the name of the entry
     */
    public String getEntryName() {
        return mEntryName;
    }

    /**
     * Returns the date of the entry.
     *
     * @return the date of the entry
     */
    public String getEntryDate() {
        return mEntryDate;
    }

    /**
     * Returns the title of the entry.
     *
     * @return the title of the entry
     */
    public String getEntryTitle() {
        return mEntryTitle;
    }

    /**
     * Returns the content of the entry.
     *
     * @return the content of the entry
     */
    public String getEntryContent() {
        return mEntryContent;
    }

    /**
     * Returns the author of the entry.
     *
     * @return the author of the entry
     */
    public String getEntryAuthor() {
        return mEntryAuthor;
    }

    /**
     * Returns the URL of the entry.
     *
     * @return the URL of the entry
     */
    public String getEntryUrl() {
        return mEntryUrl;
    }

    /**
     * Returns the enclosure of the entry.
     *
     * @return the enclosure of the entry
     */
    public String getEntryEnclosure() {
        return mEntryEnclosure;
    }

    /**
     * Returns the read status of the entry.
     *
     * @return the read status of the entry
     */
    public boolean isRead() { return mIsRead; }

    /**
     * Returns the starred status of the entry.
     *
     * @return the starred status of the entry
     */
    public boolean isStarred() {
        return mIsStarred;
    }

    /**
     * Sets the ID of the entry.
     *
     * @param entryId the ID of the entry
     */
    public void setEntryId(String entryId) {
        mEntryId = entryId;
    }

    /**
     * Sets the ID of the subscription.
     *
     * @param subId the ID of the entry
     */
    public void setSubId(String subId) {
        mSubId = subId;
    }

    /**
     * Sets the name of the entry.
     *
     * @param entryName the ID of the entry
     */
    public void setEntryName(String entryName) {
        mEntryName = entryName;
    }

    /**
     * Sets the date of the entry.
     *
     * @param entryDate the ID of the entry
     */
    public void setEntryDate(String entryDate) {
        mEntryDate = entryDate;
    }

    /**
     * Sets the title of the entry.
     *
     * @param entryTitle the ID of the entry
     */
    public void setEntryTitle(String entryTitle) {
        mEntryTitle = entryTitle;
    }

    /**
     * Sets the content of the entry.
     *
     * @param entryContent the ID of the entry
     */
    public void setEntryContent(String entryContent) {
        mEntryContent = entryContent;
    }

    /**
     * Sets the author of the entry.
     *
     * @param entryAuthor the ID of the entry
     */
    public void setEntryAuthor(String entryAuthor) {
        mEntryAuthor = entryAuthor;
    }

    /**
     * Sets the URL of the entry.
     *
     * @param entryUrl the ID of the entry
     */
    public void setEntryUrl(String entryUrl) {
        mEntryUrl = entryUrl;
    }

    /**
     * Sets the enclosure of the entry.
     *
     * @param entryEnclosure the ID of the entry
     */
    public void setEntryEnclosure(String entryEnclosure) {
        mEntryEnclosure = entryEnclosure;
    }

    /**
     * Sets the read status of the entry.
     *
     * @param isRead the read status of the entry
     */
    public void setRead(boolean isRead) { mIsRead = isRead; }

    /**
     * Sets the starred status of the entry.
     *
     * @param isStarred the starred status of the entry
     */
    public void setStarred(boolean isStarred) {
        mIsStarred = isStarred;
    }
}
