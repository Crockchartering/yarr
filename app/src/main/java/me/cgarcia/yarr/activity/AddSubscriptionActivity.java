/* YARR (Yet Another RSS Reader)
 * Copyright (C) 2017  Logan Garcia
 *
 * This file is part of YARR.
 *
 * YARR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * YARR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with YARR.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.cgarcia.yarr.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.MultiAutoCompleteTextView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import me.cgarcia.yarr.DefaultApplication;
import me.cgarcia.yarr.R;
import me.cgarcia.yarr.SharedPreferencesManager;
import me.cgarcia.yarr.api.Feedbin;
import me.cgarcia.yarr.database.FeedDatabase;
import me.cgarcia.yarr.object.Tag;

/**
 * Activity that manages the addition of new subscriptions.
 *
 * @author  Logan Garcia
 * @version %I%, %G%
 */
public class AddSubscriptionActivity extends AppCompatActivity {
    // determine whether there is loading
    private boolean mProgress;

    // hold the user input from the fields
    private String mUrl;
    private String mName;
    private String mTags;

    // the view fields of the layout
    private EditText mUrlView;
    private EditText mNameView;
    private MultiAutoCompleteTextView mTagsView;
    private TextView mTagsHelpView;
    private View mProgressView;
    private View mAddSubscriptionView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // set the layout
        setContentView(R.layout.activity_add_subscription);
        DefaultApplication.setupApi();

        // set the toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle(R.string.dialogue_add_subscription);

        // set back button on toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        // set the database
        FeedDatabase feedDatabase = new FeedDatabase(this);
        mProgress = false;

        // set the view fields of the layout
        mUrlView = (EditText) findViewById(R.id.activity_add_subscription_url);
        mNameView = (EditText) findViewById(R.id.activity_add_subscription_name);
        mProgressView = findViewById(R.id.activity_add_subscription_progress);
        mAddSubscriptionView = findViewById(R.id.activity_add_subscription_form);
        mTagsHelpView = (TextView) findViewById(R.id.activity_add_subscription_tags_hint);

        // get the names of the tags for auto-completion
        List<Tag> tags = feedDatabase.getAllTags();
        List<String> tagNames = new ArrayList<>();

        for (int i = 0; i < tags.size(); i++) {
            tagNames.add(i, tags.get(i).getTitle());
        }

        // set the tags field as comma-separated list
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_dropdown_item_1line, tagNames);
        mTagsView = (MultiAutoCompleteTextView) findViewById(R.id.activity_add_subscription_tags);
        mTagsView.setAdapter(adapter);
        mTagsView.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
        mTagsView.setOnFocusChangeListener(setupHelpView());

        // handle the intent received
        try {
            handleIntent();
        } catch (IOException | XmlPullParserException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        // exit activity on back pressed
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_add_subscription, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // on toolbar back button press
            case android.R.id.home:
                onBackPressed();
                break;
            // on completion press
            case R.id.menu_add_subscription_done:
                // do not call method if already called
                if (!mProgress) {
                    attemptAddSubscription();
                }
                break;
        }

        return true;
    }

    /**
     * Returns the focus change listener that shows/hides
     * the tag hint below the tag field.
     *
     * @return the on focus change listener
     */
    private View.OnFocusChangeListener setupHelpView() {
        return new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                // the focus is on the tag field
                if (hasFocus) {
                    mTagsHelpView.setVisibility(View.VISIBLE);
                } else {
                    mTagsHelpView.setVisibility(View.INVISIBLE);
                }
            }
        };
    }

    /**
     * Checks to see if the intent received is shared intent or a
     * view intent. If it a shared intent, receive the plain text
     * url, else parse the xml from the view intent.
     */
    private void handleIntent() throws IOException, XmlPullParserException {
        // get the calling intent
        Intent intent = getIntent();
        String action = intent.getAction();
        String type = intent.getType();
        boolean isLoggedIn = SharedPreferencesManager.getValue(
                SharedPreferencesManager.KEY_BOOLEAN_LOGGED_IN,
                SharedPreferencesManager.VALUE_BOOLEAN_LOGGED_IN_DEFAULT);

        // make sure user has logged into application
        if (isLoggedIn) {
            if (Intent.ACTION_SEND.equals(action) && type != null && "text/plain".equals(type)) {
                // intent is from receiving a shared text item
                String url = intent.getStringExtra(Intent.EXTRA_TEXT);
                // set the url field if not null
                if (url != null) {
                    mUrlView.setText(url);
                    mUrlView.setSelection(mUrlView.getText().length());
                }
            } else if (Intent.ACTION_VIEW.equals(action)) {
                // intent is from opening a URL
                Uri uri = intent.getData();
                // set the url field if not null
                if (uri != null) {
                    String url = uri.toString();
                    mUrlView.setText(url);
                    mUrlView.setSelection(mUrlView.getText().length());
                }
            }
        } else {
            // start login activity if nog logged in
            Intent i = new Intent(AddSubscriptionActivity.this, LoginActivity.class);
            startActivity(i);
            finish();
        }
    }

    /**
     * Adds a subscription if the correct conditions are achieved.
     */
    private void attemptAddSubscription() {
        // reset error fields
        mUrlView.setError(null);
        mNameView.setError(null);
        mTagsView.setError(null);

        // get the current user-supplied values from the views
        mUrl = mUrlView.getText().toString();
        mName = mNameView.getText().toString();
        mTags = mTagsView.getText().toString();

        // reset focus and cancel to default
        boolean cancel = false;
        View focusView = null;

        // check for the existence of the required URL
        if (TextUtils.isEmpty(mUrl)) {
            mUrlView.setError(getString(R.string.error_field_required));
            focusView = mUrlView;
            cancel = true;
        }

        if (cancel) {
            // there was an error; don't attempt to add the feed
            focusView.requestFocus();
        } else if (!DefaultApplication.isConnected(this)) {
            // display error if not connected
            Snackbar.make(mAddSubscriptionView, R.string.error_no_network,
                    Snackbar.LENGTH_LONG).show();
        } else {
            // attempt to add the feed
            showProgress(true);
            AsyncTask<Void, Void, Boolean> addSubscriptionUrlTask = new AddSubscriptionUrlTask();
            addSubscriptionUrlTask.execute((Void) null);
        }
    }

    /**
     * Shows the progress UI and hides the login form.
     *
     * @param show the boolean value to show/hide progress
     */
    private void showProgress(final boolean show) {
        // on Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations
        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
        mProgress = show;

        // hide main view if true
        mAddSubscriptionView.setVisibility(show ? View.GONE : View.VISIBLE);
        mAddSubscriptionView.animate().setDuration(shortAnimTime).alpha(
                show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mAddSubscriptionView.setVisibility(show ? View.GONE : View.VISIBLE);
            }
        });

        // show progress view if true
        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        mProgressView.animate().setDuration(shortAnimTime).alpha(
                show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            }
        });
    }

    /**
     * AsyncTask that attempts to add a subscription given a URL.
     *
     * @author  Logan Garcia
     * @version %I%, %G%
     */
    @SuppressLint("StaticFieldLeak")
    private class AddSubscriptionUrlTask extends AsyncTask<Void, Void, Boolean> {
        // declare ids and status code
        private String mSubId;
        private String mFeedId;
        private int mStatus;

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                // create subscription
                JSONObject jsonSubscription = new JSONObject();
                jsonSubscription.put(Feedbin.JSON_FEED_URL, mUrl);
                mStatus = Feedbin.createSubscription(jsonSubscription);

                // check for successful creation
                if (mStatus == Feedbin.STATUS_OK) {
                    // get ids from newly created subscription
                    JSONArray jsonSubscriptions = Feedbin.getSubscriptions();
                    for (int i = 0; i < jsonSubscriptions.length(); i++) {
                        jsonSubscription = jsonSubscriptions.getJSONObject(i);
                        if (mUrl.equals(jsonSubscription.getString(Feedbin.JSON_FEED_URL))) {
                            mSubId = jsonSubscription.getString(Feedbin.JSON_ID);
                            mFeedId = jsonSubscription.getString(Feedbin.JSON_FEED_ID);
                        }
                    }

                    // creation of feed was successful
                    return true;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return false;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            if (success) {
                // modify name of subscription if user provided one
                if (!TextUtils.isEmpty(mName)) {
                    AsyncTask<Void, Void, Void> addSubscriptionNameTask =
                            new AddSubscriptionNameTask(mSubId, mFeedId);
                    addSubscriptionNameTask.execute((Void) null);
                } else if (!TextUtils.isEmpty(mTags)) {
                    // categorize subscription if user provided any
                    AsyncTask<Void, Void, Void> addSubscriptionTagsTask =
                            new AddSubscriptionTagsTask(mFeedId);
                    addSubscriptionTagsTask.execute((Void) null);
                } else {
                    // finish activity if no other fields are given
                    setResult(AddSubscriptionActivity.RESULT_OK);
                    finish();
                }
            } else if (mStatus == Feedbin.STATUS_NOT_FOUND) {
                // incorrect URL was given
                showProgress(false);
                Snackbar.make(mAddSubscriptionView, R.string.error_no_subscription,
                        Snackbar.LENGTH_LONG).show();
            } else if (mStatus == Feedbin.STATUS_MULTIPLE_CHOICES) {
                // URL could refer to more than one feed
                showProgress(false);
                Snackbar.make(mAddSubscriptionView, R.string.error_multiple_subscriptions,
                        Snackbar.LENGTH_LONG).show();
            } else {
                // unknown error occurred
                showProgress(false);
                Snackbar.make(mAddSubscriptionView, R.string.error_unknown,
                        Snackbar.LENGTH_LONG).show();
            }
        }

        @Override
        protected void onCancelled() {
            showProgress(false);
        }
    }

    /**
     * AsyncTask that attempts to modify the name of a subscription.
     *
     * @author  Logan Garcia
     * @version %I%, %G%
     */
    @SuppressLint("StaticFieldLeak")
    private class AddSubscriptionNameTask extends AsyncTask<Void, Void, Void> {
        // declare ids related to the subscription
        private String mSubId;
        private String mFeedId;

        /**
         * The default constructor.
         *
         * @param subId  the ID of the subscription to modify
         * @param feedId the ID of the feed to pass to another class
         */
        AddSubscriptionNameTask(String subId, String feedId) {
            mSubId = subId;
            mFeedId = feedId;
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                // modify name of recently added subscription
                JSONObject jsonSubscription = new JSONObject();
                jsonSubscription.put(Feedbin.JSON_TITLE, mName);
                Feedbin.updateSubscription(mSubId, jsonSubscription);
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            if (!TextUtils.isEmpty(mTags)) {
                // add tags if user provided any
                AsyncTask<Void, Void, Void> addSubscriptionTagsTask = new AddSubscriptionTagsTask(mFeedId);
                addSubscriptionTagsTask.execute((Void) null);
            } else {
                // finish the activity if no tags given
                setResult(AddSubscriptionActivity.RESULT_OK);
                finish();
            }
        }
    }

    /**
     * AsyncTask that attempts to tag a subscription.
     *
     * @author  Logan Garcia
     * @version %I%, %G%
     */
    @SuppressLint("StaticFieldLeak")
    private class AddSubscriptionTagsTask extends AsyncTask<Void, Void, Void> {
        // get tag list from user input
        private List<String> mTagList = Arrays.asList(mTags.split(","));
        private String mFeedId;

        /**
         * The default constructor.
         *
         * @param feedId the ID of the feed to create the tag(s).
         */
        AddSubscriptionTagsTask(String feedId) {
            mFeedId = feedId;
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                // loop through and add the given tags
                for (int i = 0; i < mTagList.size(); i++) {
                    String tagName = mTagList.get(i);
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put(Feedbin.JSON_FEED_ID, mFeedId);
                    jsonObject.put(Feedbin.JSON_NAME, tagName);
                    Feedbin.createTag(jsonObject);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // finish the activity
            setResult(AddSubscriptionActivity.RESULT_OK);
            finish();
        }
    }
}
