/* YARR (Yet Another RSS Reader)
 * Copyright (C) 2017  Logan Garcia
 *
 * This file is part of YARR.
 *
 * YARR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * YARR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with YARR.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.cgarcia.yarr.activity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.NotificationCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Menu;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import me.cgarcia.yarr.DefaultApplication;
import me.cgarcia.yarr.EndlessRecyclerViewScrollListener;
import me.cgarcia.yarr.SharedPreferencesManager;
import me.cgarcia.yarr.api.Feedbin;
import me.cgarcia.yarr.decoration.DividerItemDecoration;
import me.cgarcia.yarr.R;
import me.cgarcia.yarr.adapter.EntryAdapter;
import me.cgarcia.yarr.adapter.SubscriptionAdapter;
import me.cgarcia.yarr.adapter.TagAdapter;
import me.cgarcia.yarr.database.FeedDatabase;
import me.cgarcia.yarr.fragment.MainFragment;
import me.cgarcia.yarr.object.Entry;
import me.cgarcia.yarr.object.Subscription;
import me.cgarcia.yarr.object.Tag;

/**
 * Activity which is launched by default if not logged in,
 * otherwise starts the LoginActivity.
 *
 * @author  Logan Garcia
 * @version %I%, %G%
 */
public class MainActivity extends AppCompatActivity implements EntryAdapter.ItemClickCallback,
        SubscriptionAdapter.ItemClickCallback, TagAdapter.TagItemClickCallback,
        TagAdapter.SubscriptionItemClickCallback
{
    // strings related to passing arguments to intents
    private static final String KEY_STRING_QUERY = "restore_query";
    private static final String KEY_STRING_MAIN_FRAGMENT = "main_fragment";
    private static final String KEY_STRING_ARRAY_ENTRY_IDS = "entry_ids";
    private static final String KEY_INTEGER_ENTRY_SIZE = "size";
    private static final String KEY_LONG_SYNC_REMAINING = "sync_remaining";
    private static final String KEY_BOOLEAN_UPDATING = "updating";
    private static final String KEY_BOOLEAN_REFRESHING = "refreshing";

    private static final String VALUE_STRING_QUERY_DEFAULT = "";
    private static final int VALUE_INTEGER_ENTRY_SIZE_DEFAULT = 25;
    private static final boolean VALUE_BOOLEAN_UPDATING_DEFAULT = false;

    // strings related to activity IDs for results
    private static final int ACTIVITY_CONTENT = 1;
    private static final int ACTIVITY_ADD_FEED = 2;
    private static final int ACTIVITY_RENAME_TAG = 3;
    private static final int ACTIVITY_EDIT_SUBSCRIPTION = 4;
    private static final int ACTIVITY_SETTINGS = 5;

    // handler-related declarations
    private static Handler mHandler = new Handler();
    private Runnable mRunnable;

    // lists of tags, subscriptions, and entries to display
    private ArrayList<Entry> mEntries;
    private ArrayList<Subscription> mSubscriptions;
    private ArrayList<Tag> mTags;

    // declare the database and fragment
    private FeedDatabase mFeedDatabase;
    private MainFragment mMainFragment;

    // the primary drawer views
    private View mAllView;
    private View mStarredView;
    private View mUnreadView;

    // query-related declarations
    private SearchView mSearchView;
    private String mSearchQuery;

    // the list size of the entries to display
    private int mCurrentEntrySize;
    // determines whether the activity is updating
    private boolean mIsUpdating;
    // the time that the handler was started
    private long mStartTime;
    // the time remaining for the sync
    private long mSyncRemaining;

    // the recycler views
    private RecyclerView mRecViewEntries;
    private RecyclerView mRecViewTags;
    private RecyclerView mRecViewSubscriptions;

    // layout items needed across the class
    private DrawerLayout mDrawer;
    private Toolbar mToolbar;
    private ActionBarDrawerToggle mDrawerToggle;
    private TagAdapter mTagAdapter;
    private FloatingActionButton mFab;
    private EndlessRecyclerViewScrollListener mScrollListener;

    // the swipe refresh layouts for each view
    private SwipeRefreshLayout mRefreshEntriesLayout;
    private SwipeRefreshLayout mRefreshEmptyLayout;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        boolean isLoggedIn = SharedPreferencesManager.getValue(
                SharedPreferencesManager.KEY_BOOLEAN_LOGGED_IN,
                SharedPreferencesManager.VALUE_BOOLEAN_LOGGED_IN_DEFAULT);

        // close this activity and start the login activity
        // if not logged in
        if (!isLoggedIn) {
            Intent i = new Intent(MainActivity.this, LoginActivity.class);
            startActivity(i);
            finish();
        } else {
            //set the main layout
            setContentView(R.layout.activity_main);

            //set the API and database
            DefaultApplication.setupApi();
            mFeedDatabase = new FeedDatabase(this);

            // find the fragment
            FragmentManager fm = getFragmentManager();
            mMainFragment = (MainFragment) fm.findFragmentByTag(KEY_STRING_MAIN_FRAGMENT);

            // create the fragment and data the first time
            if (mMainFragment == null) {
                // add the fragment
                mMainFragment = new MainFragment();
                fm.beginTransaction().add(mMainFragment, KEY_STRING_MAIN_FRAGMENT).commit();
            }

            // set the tags, subscriptions, and entries array
            mEntries = mMainFragment.getEntries();
            mTags = mMainFragment.getTags();
            mSubscriptions = mMainFragment.getSubscriptions();

            //set the toolbar
            mToolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(mToolbar);

            // set the floating action button
            mFab = (FloatingActionButton) findViewById(R.id.activity_main_fab);
            mFab.setOnClickListener(setupFabListener());

            // set the swipe refresh layouts
            mRefreshEntriesLayout =
                    (SwipeRefreshLayout) findViewById(R.id.activity_main_swipe_refresh_entries);
            mRefreshEntriesLayout.setColorSchemeResources(R.color.colorSecondary);
            mRefreshEntriesLayout.setOnRefreshListener(setupRefreshListener());

            mRefreshEmptyLayout =
                    (SwipeRefreshLayout) findViewById(R.id.activity_main_swipe_refresh_empty);
            mRefreshEmptyLayout.setColorSchemeResources(R.color.colorSecondary);
            mRefreshEmptyLayout.setOnRefreshListener(setupRefreshListener());

            // set the drawer layout
            mDrawer = (DrawerLayout) findViewById(R.id.activity_main_drawer);
            mDrawerToggle = setupDrawerToggle();
            mDrawer.addDrawerListener(mDrawerToggle);

            // set the main drawer views
            mAllView = findViewById(R.id.list_drawer_all);
            mStarredView = findViewById(R.id.list_drawer_starred);
            mUnreadView = findViewById(R.id.list_drawer_unread);

            // set the recycler view for subscriptions
            mRecViewSubscriptions = (RecyclerView) findViewById(R.id.activity_main_list_subscriptions);
            mRecViewSubscriptions.setNestedScrollingEnabled(false);
            mRecViewSubscriptions.setLayoutManager(new LinearLayoutManager(this));
            SubscriptionAdapter subscriptionAdapter = new SubscriptionAdapter(mSubscriptions, this);
            mRecViewSubscriptions.setAdapter(subscriptionAdapter);
            subscriptionAdapter.setItemClickCallback(this);

            // set the recycler view for entries
            mRecViewEntries = (RecyclerView) findViewById(R.id.activity_main_entries);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
            mRecViewEntries.setLayoutManager(linearLayoutManager);
            mScrollListener = setupScrollListener(linearLayoutManager);
            mRecViewEntries.addOnScrollListener(mScrollListener);
            RecyclerView.ItemDecoration dividerItemDecoration =
                    new DividerItemDecoration(ContextCompat.getDrawable(this,
                            R.drawable.item_list_divider));
            mRecViewEntries.addItemDecoration(dividerItemDecoration);
            EntryAdapter entryAdapter = new EntryAdapter(mEntries, this);
            mRecViewEntries.setAdapter(entryAdapter);
            entryAdapter.setItemClickCallback(this);

            // set the recycler view for tags
            mRecViewTags = (RecyclerView) findViewById(R.id.activity_main_list_tags);
            mRecViewTags.setNestedScrollingEnabled(false);
            mRecViewTags.setLayoutManager(new LinearLayoutManager(this));
            mTagAdapter = new TagAdapter(mTags, this);
            mRecViewTags.setAdapter(mTagAdapter);
            mTagAdapter.setTagItemClickCallback(this);
            mTagAdapter.setSubscriptionItemClickCallback(this);

            // don't initialize search on first start of activity
            if (savedInstanceState == null) {
                // reset global variables;
                mSearchQuery = VALUE_STRING_QUERY_DEFAULT;
                mCurrentEntrySize = VALUE_INTEGER_ENTRY_SIZE_DEFAULT;
                mIsUpdating = VALUE_BOOLEAN_UPDATING_DEFAULT;
                updateViews(false);
            } else {
                // restore global variables
                mCurrentEntrySize = savedInstanceState.getInt(KEY_INTEGER_ENTRY_SIZE);
                mSearchQuery = savedInstanceState.getString(KEY_STRING_QUERY);
                mIsUpdating = savedInstanceState.getBoolean(KEY_BOOLEAN_UPDATING);
                setRefreshing(savedInstanceState.getBoolean(KEY_BOOLEAN_REFRESHING));
                updateViews(true);
            }

            // set the runnable
            mRunnable = new Runnable() {
                @Override
                public void run() {
                    updateAll();
                }
            };

            // initiate check for new feeds on network
            // if first start of activity
            if (savedInstanceState == null) {
                updateAll();
            } else {
                // get the saved time
                mSyncRemaining = savedInstanceState.getLong(KEY_LONG_SYNC_REMAINING);

                // start the handler from the saved time
                startHandler();
            }

            // set on click listeners for main drawer views
            mAllView.setOnClickListener(setupActionListener(
                    SharedPreferencesManager.VALUE_STRING_DISPLAY_ALL));

            mStarredView.setOnClickListener(setupActionListener(
                    SharedPreferencesManager.VALUE_STRING_DISPLAY_STARRED));

            mUnreadView.setOnClickListener(setupActionListener(
                    SharedPreferencesManager.VALUE_STRING_DISPLAY_UNREAD));

            // handle any intents
            handleIntent(getIntent());
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // save the current time of the handler
        saveHandler();

        // store the large arrays in the fragment
        mMainFragment.setEntries(mEntries);
        mMainFragment.setSubscriptions(mSubscriptions);
        mMainFragment.setTags(mTags);

        // save smaller variables in the bundle
        outState.putInt(KEY_INTEGER_ENTRY_SIZE, mCurrentEntrySize);
        outState.putLong(KEY_LONG_SYNC_REMAINING, mSyncRemaining);
        outState.putBoolean(KEY_BOOLEAN_UPDATING, mIsUpdating);
        outState.putBoolean(KEY_BOOLEAN_REFRESHING, isRefreshing());

        // save the current user query
        if (mSearchView != null) {
            mSearchQuery = mSearchView.getQuery().toString();
            outState.putString(KEY_STRING_QUERY, mSearchQuery);
        } else {
            outState.putString(KEY_STRING_QUERY, VALUE_STRING_QUERY_DEFAULT);
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        // make sure fab is hidden if drawer is open
        if (mDrawer.isDrawerOpen(GravityCompat.START)) {
            mFab.setVisibility(View.GONE);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        // store the data in the fragment
        mMainFragment.setEntries(mEntries);
        mMainFragment.setSubscriptions(mSubscriptions);
        mMainFragment.setTags(mTags);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch(requestCode) {
            case ACTIVITY_CONTENT:
                if (resultCode == RESULT_OK) {
                    // make sure day/night mode is created
                    recreate();
                    // update all views after viewing entry content
                    updateViews(false);
                }
                break;
            case ACTIVITY_ADD_FEED:
                if (resultCode == RESULT_OK) {
                    // update from network after adding a feed
                    updateAll();
                }
                break;
            case ACTIVITY_RENAME_TAG:
                if (resultCode == RESULT_OK) {
                    // update all views after renaming a tag
                    updateViews(false);
                }
                break;
            case ACTIVITY_EDIT_SUBSCRIPTION:
                if (resultCode == RESULT_OK) {
                    // update all views after editing a subscription
                    updateViews(false);
                }
                break;
            case ACTIVITY_SETTINGS:
                if (resultCode == RESULT_OK) {
                    // recreate the activity after applying
                    // new settings and preferences
                    recreate();
                }
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        // sync the toggle state after onRestoreInstanceState has occurred
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        // pass any configuration change to the drawer toggles
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        handleIntent(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // inflate the menu; this adds items to the action bar if it is present
        getMenuInflater().inflate(R.menu.menu_main, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);

        // set the search view properties
        SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
        mSearchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        if (searchManager != null) {
            mSearchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        }
        mSearchView.setMaxWidth(Integer.MAX_VALUE);

        // set the expand/collapse listener for the search view
        MenuItemCompat.OnActionExpandListener expandListener = setupSearchListener();
        MenuItemCompat.setOnActionExpandListener(searchItem, expandListener);

        if (mSearchQuery != null && !TextUtils.isEmpty(mSearchQuery)) {
            // set the search query if searching
            searchItem.expandActionView();
            mSearchView.setQuery(mSearchQuery, true);
            searchItem.getActionView().clearFocus();
        }

        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // set the menu items that will change depending on the view
        MenuItem editItem = menu.findItem(R.id.action_edit);
        MenuItem renameItem = menu.findItem(R.id.action_rename);
        MenuItem deleteTagItem = menu.findItem(R.id.action_tag_delete);
        MenuItem deleteSubItem = menu.findItem(R.id.action_sub_delete);
        MenuItem markReadItem = menu.findItem(R.id.action_mark_read);
        MenuItem markUnreadItem = menu.findItem(R.id.action_mark_unread);

        // get the current display view
        String display = SharedPreferencesManager.getValue(
                SharedPreferencesManager.KEY_STRING_DISPLAY,
                SharedPreferencesManager.VALUE_STRING_DISPLAY_DEFAULT);
        boolean containsUnread = false;

        switch (display) {
            case SharedPreferencesManager.VALUE_STRING_DISPLAY_ALL:
            case SharedPreferencesManager.VALUE_STRING_DISPLAY_STARRED:
            case SharedPreferencesManager.VALUE_STRING_DISPLAY_UNREAD:
                // common elements whether view is all, starred, or unread
                editItem.setVisible(false);
                renameItem.setVisible(false);
                deleteTagItem.setVisible(false);
                deleteSubItem.setVisible(false);
                break;
            case SharedPreferencesManager.VALUE_STRING_DISPLAY_TAG:
                // set items relevant to a tag view
                editItem.setVisible(false);
                renameItem.setVisible(true);
                deleteTagItem.setVisible(true);
                deleteSubItem.setVisible(false);
                break;
            case SharedPreferencesManager.VALUE_STRING_DISPLAY_SUB:
                // set items relevant to a subscription view
                editItem.setVisible(true);
                renameItem.setVisible(false);
                deleteTagItem.setVisible(false);
                deleteSubItem.setVisible(true);
                break;
        }

        for (int i = 0; i < mEntries.size(); i++) {
            Entry entry = mEntries.get(i);
            if (!entry.isRead()) {
                containsUnread = true;
                break;
            }
        }

        if (mEntries.size() == 0) {
            // no items to mark as read/unread
            markReadItem.setVisible(false);
            markUnreadItem.setVisible(false);
        } else if (containsUnread) {
            // items to mark as read
            markReadItem.setVisible(true);
            markUnreadItem.setVisible(false);
        } else {
            // only items to mark as unread
            markReadItem.setVisible(false);
            markUnreadItem.setVisible(true);
        }

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;

        switch (item.getItemId()) {
            case R.id.action_tag_delete:
                // delete tag if there is a network connection
                if (DefaultApplication.isConnected(this)) {
                    deleteTag();
                } else {
                    Snackbar.make(getView(), R.string.error_no_network,
                            Snackbar.LENGTH_LONG).show();
                }
                break;
            case R.id.action_sub_delete:
                // delete subscription if there is a network connection
                if (DefaultApplication.isConnected(this)) {
                    deleteSubscription();
                } else {
                    Snackbar.make(getView(), R.string.error_no_network,
                            Snackbar.LENGTH_LONG).show();
                }
                break;
            case R.id.action_mark_read:
                // mark as read if there is a network connection
                if (DefaultApplication.isConnected(this)) {
                    markAllAsRead();
                } else {
                    Snackbar.make(getView(), R.string.error_no_network,
                            Snackbar.LENGTH_LONG).show();
                }

                updateViews(false);
                break;
            case R.id.action_mark_unread:
                // mark as unread if there is a network connection
                if (DefaultApplication.isConnected(MainActivity.this)) {
                    markAllAsUnread();
                } else {
                    Snackbar.make(getView(), R.string.error_no_network,
                            Snackbar.LENGTH_LONG).show();
                }

                updateViews(false);
                break;
            case R.id.action_rename:
                // start rename tag intent
                intent = new Intent(MainActivity.this, RenameTagActivity.class);
                startActivityForResult(intent, ACTIVITY_RENAME_TAG);
                break;
            case R.id.action_edit:
                // start edit subscription intent
                intent = new Intent(MainActivity.this, EditSubscriptionActivity.class);
                startActivityForResult(intent, ACTIVITY_EDIT_SUBSCRIPTION);
                break;
            case R.id.action_refresh:
                // update everything if not already updating
                setRefreshing(true);
                updateAll();
                break;
            case R.id.action_settings:
                // start settings intent
                intent = new Intent(MainActivity.this, SettingsActivity.class);
                startActivityForResult(intent, ACTIVITY_SETTINGS);
                break;
        }

        return true;
    }

    @Override
    public void onEntryItemClick(int p) {
        // set the position of the entry item clicked
        SharedPreferencesManager.setValue(SharedPreferencesManager.KEY_INTEGER_POSITION, p);

        // get the entry IDs to pass to the content activity
        String[] entryIds = new String[mEntries.size()];

        for (int i = 0; i < mEntries.size(); i++) {
            entryIds[i] = mEntries.get(i).getEntryId();
        }

        Bundle bundle = new Bundle();
        bundle.putStringArray(KEY_STRING_ARRAY_ENTRY_IDS, entryIds);

        // pass the entry IDs to the new activity
        Intent i = new Intent(MainActivity.this, ContentActivity.class);
        i.putExtras(bundle);
        startActivityForResult(i, ACTIVITY_CONTENT);
    }

    @Override
    public void onUntaggedSubscriptionItemClick(int p) {
        // get the subscription at the position clicked
        Subscription subscription = mSubscriptions.get(p);

        // set the view and current subscription ID to display the subscription
        SharedPreferencesManager.setValue(SharedPreferencesManager.KEY_STRING_SUB_ID,
                subscription.getSubId());
        SharedPreferencesManager.setValue(SharedPreferencesManager.KEY_STRING_DISPLAY,
                SharedPreferencesManager.VALUE_STRING_DISPLAY_SUB);

        // mark item as selected and close the drawer
        onDrawerItemSelected();
    }

    @Override
    public void onTaggedSubscriptionItemClick(int p) {
        // set values to find the subscription
        int calcPosition = 0;
        boolean found = false;

        // find the subscription ID based on its relative position
        for (int i = 0; i < mTags.size() && !found; i++) {
            if (mTagAdapter.isGroupExpanded(mTags.get(i))) {
                List<Subscription> subs = mTags.get(i).getItems();
                for (int v = 0; v < subs.size(); v++) {
                    calcPosition++;
                    Subscription sub = subs.get(v);
                    if (calcPosition == p) {
                        // save the subscription ID and set the view
                        SharedPreferencesManager.setValue(
                                SharedPreferencesManager.KEY_STRING_SUB_ID, sub.getSubId());
                        SharedPreferencesManager.setValue(
                                SharedPreferencesManager.KEY_STRING_DISPLAY,
                                SharedPreferencesManager.VALUE_STRING_DISPLAY_SUB);

                        // exit out of loop if found
                        found = true;
                        break;
                    }
                }
            }

            calcPosition++;
        }

        // select the subscription and close the drawer
        onDrawerItemSelected();
    }

    @Override
    public void onTagItemClick(int p) {
        // set values to find the tag
        int calcPosition = 0;

        for (int i = 0; i < mTags.size(); i++) {
            if (calcPosition == p) {
                // save the subscription ID and set the view
                SharedPreferencesManager.setValue(SharedPreferencesManager.KEY_STRING_TAG_ID,
                        mTags.get(i).getTagId());
                SharedPreferencesManager.setValue(SharedPreferencesManager.KEY_STRING_DISPLAY,
                        SharedPreferencesManager.VALUE_STRING_DISPLAY_TAG);
                break;
            }
            else if (mTagAdapter.isGroupExpanded(mTags.get(i))) {
                calcPosition += mTags.get(i).getItems().size();
            }

            calcPosition++;
        }

        onDrawerItemSelected();
    }

    /**
     * Returns the drawer toggle that indicates whether the drawer is
     * being opened or closed.
     *
     * @return the image at the specified URL
     */
    private ActionBarDrawerToggle setupDrawerToggle() {
        // NOTE: Make sure you pass in a valid mToolbar reference.  ActionBarDrawToggle() does not require it
        // and will not render the hamburger icon without it.
        return new ActionBarDrawerToggle(this, mDrawer, mToolbar, R.string.action_drawer_open,
                R.string.action_drawer_close) {

            @Override
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                // show the fab when the drawer is closed
                mFab.setVisibility(View.VISIBLE);
                displayEntries(false);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                // hide the fab when the drawer is open
                mFab.setVisibility(View.GONE);
            }

            @Override
            public void onDrawerSlide(View drawerView, float offset) {
                super.onDrawerSlide(drawerView, offset);
                // fade in/out the fab while the drawer is sliding
                mFab.setVisibility(View.VISIBLE);
                mFab.setAlpha(1 - offset);
            }

        };
    }

    /**
     * Returns the action expand listener which listens when the search
     * field is expanded or collapsed.
     *
     * @return the menu item action expand listener
     */
    private MenuItemCompat.OnActionExpandListener setupSearchListener() {
        return new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                // disable the refresh, drawer, and fab
                mRefreshEntriesLayout.setEnabled(false);
                mRefreshEmptyLayout.setEnabled(false);
                mDrawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                mFab.setVisibility(View.GONE);
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                // enable the refresh, drawer, and fab
                mRefreshEntriesLayout.setEnabled(true);
                mRefreshEmptyLayout.setEnabled(true);
                mDrawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
                mFab.setVisibility(View.VISIBLE);
                // disable the search
                mSearchQuery = VALUE_STRING_QUERY_DEFAULT;
                invalidateOptionsMenu();
                displayEntries(false);
                return true;
            }
        };
    }

    /**
     * Returns the on click listener used to listen to click events
     * related to the three main drawer buttons (unread, starred, and
     * all).
     *
     * @param display the selected display item
     * @return        the listener for the display item
     */
    private View.OnClickListener setupActionListener(final String display) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // set display and close the drawer
                SharedPreferencesManager.setValue(SharedPreferencesManager.KEY_STRING_DISPLAY,
                        display);
                onDrawerItemSelected();
            }
        };
    }

    /**
     * Returns the on refresh listener used to create the pull-down
     * refresh icon and start related update.
     *
     * @return the on refresh listener
     */
    private SwipeRefreshLayout.OnRefreshListener setupRefreshListener() {
        return new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                updateAll();
            }
        };
    }

    /**
     * Returns the on click listener that listens to click events
     * related to the floating action button (fab).
     *
     * @return the on click listener
     */
    private View.OnClickListener setupFabListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // start the add new subscription intent on click
                Intent i = new Intent(MainActivity.this, AddSubscriptionActivity.class);
                startActivityForResult(i, ACTIVITY_ADD_FEED);
            }
        };
    }

    /**
     * Returns the scroll listener that triggers when a
     * certain visible threshold is met.
     *
     * @return the scroll listener
     */
    private EndlessRecyclerViewScrollListener setupScrollListener(LinearLayoutManager linearLayoutManager) {
        return new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                // triggered only when new data needs to be appended to the list
                displayEntries(true);
            }
        };
    }

    /**
     * Returns the current refresh layout view that is visible on screen.
     *
     * @return the current refresh layout
     */
    private View getView() {
        if (mRefreshEntriesLayout.getVisibility() == View.VISIBLE) {
            return mRefreshEntriesLayout;
        } else {
            return mRefreshEmptyLayout;
        }
    }

    /**
     * Returns the bitmap that is retrieved from the URL if one
     * exists; if not, returns null.
     *
     * @return the bitmap retrieved from the URL
     */
    private Bitmap getBitmapFromUrl(String address) {
        try {
            // get the domain of the url
            URL url = new URL(address);
            String domain = url.getHost();

            // get the image from the url
            address = "https://icons.duckduckgo.com/ip2/" + domain + ".ico";
            InputStream input = new URL(address).openStream();
            return BitmapFactory.decodeStream(input);
        } catch (IOException e) {
            Log.e("IOException", e.getMessage());
        }

        // failed to get the image from url
        return null;
    }

    /**
     * Returns the boolean value of whether the entries layout or the
     * empty layout is currently displaying the refreshing icon.
     *
     * @return the boolean value of a refresh layout
     */
    private boolean isRefreshing() {
        return mRefreshEntriesLayout.isRefreshing() || mRefreshEmptyLayout.isRefreshing();
    }

    /**
     * Sets the refresh layout view to display based on the amount of
     * entries that are visible.
     */
    private void setView() {
        // check if there are entries and if the view is gone
        if (mEntries.size() > 0 && mRefreshEntriesLayout.getVisibility() == View.GONE) {
            mRefreshEntriesLayout.setVisibility(View.VISIBLE);
            mRefreshEmptyLayout.setVisibility(View.GONE);
        } else if (mEntries.size() == 0 && mRefreshEntriesLayout.getVisibility() == View.VISIBLE) {
            mRefreshEntriesLayout.setVisibility(View.GONE);
            mRefreshEmptyLayout.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Activates or deactivates the refresh icon based on which layout
     * is currently visible.
     *
     * @param refreshing show/hide the refresh icon
     */
    private void setRefreshing(boolean refreshing) {
        if (mRefreshEntriesLayout.getVisibility() == View.VISIBLE) {
            // set the refresh entries layout
            mRefreshEntriesLayout.setRefreshing(refreshing);
        } else if (mRefreshEmptyLayout.getVisibility() == View.VISIBLE) {
            // set the refresh empty layout
            mRefreshEmptyLayout.setRefreshing(refreshing);
        }
    }

    /**
     * Processes an external event from an intent.
     *
     * @param intent the intent to process
     */
    private void handleIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            // get the search query and process it
            mSearchQuery = intent.getStringExtra(SearchManager.QUERY);
            mCurrentEntrySize = VALUE_INTEGER_ENTRY_SIZE_DEFAULT;
            displayEntries(false);
        }
    }

    /**
     * Starts the handler from the runnable based on the remaining time
     * since device rotation.
     */
    private void startHandler() {
        // get the current start time
        mStartTime = SystemClock.elapsedRealtime();

        // set the handler with the remaining time
        if (mSyncRemaining != SharedPreferencesManager.VALUE_INTEGER_SYNC_NEVER) {
            mHandler.removeCallbacks(mRunnable);
            mHandler.postDelayed(mRunnable, mSyncRemaining);
        }
    }

    /**
     * Restarts the handler from the runnable based on the user-set
     * sync time.
     */
    private void restartHandler() {
        // set the current start time
        mStartTime = SystemClock.elapsedRealtime();

        // get the user set time
        mSyncRemaining = SharedPreferencesManager.getValue(
                SharedPreferencesManager.KEY_INTEGER_SYNC,
                SharedPreferencesManager.VALUE_INTEGER_SYNC_DEFAULT);

        // remove current runnable and add the new one
        if (mSyncRemaining != SharedPreferencesManager.VALUE_INTEGER_SYNC_NEVER) {
            mHandler.removeCallbacks(mRunnable);
            mHandler.postDelayed(mRunnable, mSyncRemaining);
        }
    }

    /**
     * Saves the handler by getting the remaining time since the start
     * of the handler and saving that value.
     */
    private void saveHandler() {
        // calculate the remaining time
        long elapsedTime = SystemClock.elapsedRealtime() - mStartTime;
        mSyncRemaining = mSyncRemaining - elapsedTime;
    }

    /**
     * Highlights the currently selected drawer item and refreshes
     * the tag and subscriptions list.
     */
    private void onDrawerItemSelected() {
        // highlight the currently selected item
        setSelection();

        // reset the current entry size
        mCurrentEntrySize = VALUE_INTEGER_ENTRY_SIZE_DEFAULT;

        // update the subscription and tag lists
        mRecViewSubscriptions.getAdapter().notifyDataSetChanged();
        saveToggleState();

        // close the drawer
        mDrawer.closeDrawers();
    }

    public void setSelection() {
        // get the current display
        String display = SharedPreferencesManager.getValue(
                SharedPreferencesManager.KEY_STRING_DISPLAY,
                SharedPreferencesManager.VALUE_STRING_DISPLAY_DEFAULT);

        // select display and set others to false
        switch (display) {
            case SharedPreferencesManager.VALUE_STRING_DISPLAY_ALL:
                setTitle(R.string.dialogue_all);

                mAllView.setSelected(true);
                mUnreadView.setSelected(false);
                mStarredView.setSelected(false);

                SharedPreferencesManager.setValue(SharedPreferencesManager.KEY_STRING_TAG_ID, "");
                SharedPreferencesManager.setValue(SharedPreferencesManager.KEY_STRING_SUB_ID, "");
                break;
            case SharedPreferencesManager.VALUE_STRING_DISPLAY_STARRED:
                setTitle(R.string.dialogue_starred);

                mStarredView.setSelected(true);
                mUnreadView.setSelected(false);
                mAllView.setSelected(false);

                SharedPreferencesManager.setValue(SharedPreferencesManager.KEY_STRING_TAG_ID, "");
                SharedPreferencesManager.setValue(SharedPreferencesManager.KEY_STRING_SUB_ID, "");
                break;
            case SharedPreferencesManager.VALUE_STRING_DISPLAY_UNREAD:
                setTitle(R.string.dialogue_unread);

                mUnreadView.setSelected(true);
                mAllView.setSelected(false);
                mStarredView.setSelected(false);

                SharedPreferencesManager.setValue(SharedPreferencesManager.KEY_STRING_TAG_ID, "");
                SharedPreferencesManager.setValue(SharedPreferencesManager.KEY_STRING_SUB_ID, "");
                break;
            case SharedPreferencesManager.VALUE_STRING_DISPLAY_TAG:
                String tagId = SharedPreferencesManager.getValue(
                        SharedPreferencesManager.KEY_STRING_TAG_ID,
                        SharedPreferencesManager.VALUE_STRING_TAG_ID_DEFAULT);
                // the tag still exists
                Tag tag = mFeedDatabase.getTag(tagId);
                setTitle(tag.getTitle());

                mUnreadView.setSelected(false);
                mAllView.setSelected(false);
                mStarredView.setSelected(false);

                SharedPreferencesManager.setValue(SharedPreferencesManager.KEY_STRING_SUB_ID, "");
                break;
            case SharedPreferencesManager.VALUE_STRING_DISPLAY_SUB:
                String subId = SharedPreferencesManager.getValue(
                        SharedPreferencesManager.KEY_STRING_SUB_ID,
                        SharedPreferencesManager.VALUE_STRING_SUB_ID_DEFAULT);
                // the tag still exists
                Subscription subscription = mFeedDatabase.getSubscription(subId);
                setTitle(subscription.getSubName());

                mUnreadView.setSelected(false);
                mAllView.setSelected(false);
                mStarredView.setSelected(false);

                SharedPreferencesManager.setValue(SharedPreferencesManager.KEY_STRING_TAG_ID, "");
                break;
        }
    }

    /**
     * Saves the current expand/collapse state of all tags in the drawer
     * and restores them after recreation.
     */
    private void saveToggleState() {
        // store the toggle groups
        List<Tag> toggledGroups = new ArrayList<>();

        try {
            for (int i = 0; i < mTags.size(); i++) {
                if (mTagAdapter.isGroupExpanded(mTags.get(i))) {
                    toggledGroups.add(mTags.get(i));
                }
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            e.printStackTrace();
        }

        // create new adapter for the toggled groups
        mTagAdapter = new TagAdapter(mTags, this);

        // toggle the stored groups
        for (int i = toggledGroups.size() - 1; i >= 0; i--) {
            if (mTagAdapter.getGroups().contains(toggledGroups.get(i))) {
                mTagAdapter.toggleGroup(toggledGroups.get(i));
            }
        }

        // set the new adapter for the toggled groups
        mRecViewTags.setAdapter(mTagAdapter);
        mTagAdapter.setTagItemClickCallback(this);
        mTagAdapter.setSubscriptionItemClickCallback(this);
    }

    /**
     * Keeps the number of entries per subscription (not counting
     * starred entries) to no more than 100.
     */
    private void trimEntries() {
        List<Subscription> subscriptions = mFeedDatabase.getAllSubscriptions();

        // get all subscriptions
        for (int i = 0; i < subscriptions.size(); i++) {
            Subscription subscription = subscriptions.get(i);
            String subId = subscription.getSubId();
            List<Entry> entries = mFeedDatabase.getEntriesBySub(subId);
            // go through each entry of a subscription
            // that has more than 100 entries
            for (int v = 100; v < entries.size(); v++) {
                Entry entry = entries.get(v);
                if (!entry.isStarred()) {
                    mFeedDatabase.deleteEntry(entry.getEntryId());
                }
            }
        }
    }

    /**
     * Displays a confirmation dialogue window and, on confirmation,
     * deletes the currently displayed tag.
     */
    private void deleteTag() {
        // set the confirmation dialogue box
        new AlertDialog.Builder(this)
                .setTitle(R.string.popup_delete_tag_title)
                .setMessage(R.string.popup_delete_tag_message)
                .setPositiveButton(R.string.popup_delete_positive,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // get the tag ID and name from the stored values
                                String tagId = SharedPreferencesManager.getValue(
                                        SharedPreferencesManager.KEY_STRING_TAG_ID,
                                        SharedPreferencesManager.VALUE_STRING_TAG_ID_DEFAULT);
                                Tag tag = mFeedDatabase.getTag(tagId);
                                String tagName = tag.getTitle();

                                // delete the tag from the network and local database
                                mFeedDatabase.deleteTag(tagId);
                                AsyncTask<Void, Void, Void> deleteTagTask =
                                        new DeleteTagTask(tagName);
                                deleteTagTask.execute((Void) null);

                                // refresh the display
                                SharedPreferencesManager.setValue(
                                        SharedPreferencesManager.KEY_STRING_DISPLAY,
                                        SharedPreferencesManager.VALUE_STRING_DISPLAY_UNREAD);
                                displayEntries(false);
                            }
                        })
                .setNegativeButton(R.string.popup_delete_negative, null)
                .show();
    }

    /**
     * Displays a confirmation dialogue window and, on confirmation,
     * deletes the currently displayed subscription.
     */
    private void deleteSubscription() {
        // set the confirmation dialogue box
        new AlertDialog.Builder(this)
                .setTitle(R.string.popup_delete_subscription_title)
                .setMessage(R.string.popup_delete_subscription_message)
                .setPositiveButton(R.string.popup_delete_positive,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // get the subscription ID from stored value
                                String subId = SharedPreferencesManager.getValue(
                                        SharedPreferencesManager.KEY_STRING_SUB_ID,
                                        SharedPreferencesManager.VALUE_STRING_SUB_ID_DEFAULT);

                                // delete the subscription from the network and local database
                                mFeedDatabase.deleteSubscription(subId);
                                AsyncTask<Void, Void, Void> deleteSubscriptionTask =
                                        new DeleteSubscriptionTask(subId);
                                deleteSubscriptionTask.execute((Void) null);

                                // refresh the display
                                SharedPreferencesManager.setValue(
                                        SharedPreferencesManager.KEY_STRING_DISPLAY,
                                        SharedPreferencesManager.VALUE_STRING_DISPLAY_UNREAD);
                                displayEntries(false);
                            }
                        })
                .setNegativeButton(R.string.popup_delete_negative, null)
                .show();
    }

    /**
     * Marks the currently displayed entries on screen as read and
     * provides an undo option to reverse the action.
     */
    private void markAllAsRead() {
        // store marked entries in a temporary list
        final List<Entry> readEntries = new ArrayList<>();

        // go through the entries and mark unread entries as read
        for (int i = 0; i < mEntries.size(); i++) {
            Entry entry = mEntries.get(i);
            if (!entry.isRead()) {
                readEntries.add(entry);
                entry.setRead(true);
                // update the network and local database
                mFeedDatabase.updateEntry(entry);
                AsyncTask<Void, Void, Void> markAsReadTask =
                        new MarkAsReadTask(entry.getEntryId());
                markAsReadTask.execute();
            }
        }

        Snackbar.make(getView(), R.string.popup_marked_as_read, Snackbar.LENGTH_LONG)
                .setAction(R.string.action_undo, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // mark the previously-read entries as unread from
                        // the temporary list
                        for (int i = 0; i < readEntries.size(); i++) {
                            Entry entry = readEntries.get(i);
                            entry.setRead(false);
                            mFeedDatabase.updateEntry(entry);
                            AsyncTask<Void, Void, Void> markAsUnreadTask =
                                    new MarkAsUnreadTask(entry.getEntryId());
                            markAsUnreadTask.execute();
                        }

                        updateViews(false);
                    }
                })
                .show();
    }

    /**
     * Marks the currently displayed entries on screen as unread
     * and provides an undo option to reverse the action.
     */
    private void markAllAsUnread() {
        // store marked entries in a temporary list
        final List<Entry> unreadEntries = new ArrayList<>();

        // go through the entries and mark read entries as unread
        for (int i = 0; i < mEntries.size(); i++) {
            Entry entry = mEntries.get(i);
            if (entry.isRead()) {
                unreadEntries.add(entry);
                entry.setRead(false);
                // update the network and local database
                mFeedDatabase.updateEntry(entry);
                AsyncTask<Void, Void, Void> markAsUnreadTask =
                        new MarkAsUnreadTask(entry.getEntryId());
                markAsUnreadTask.execute();
            }
        }

        Snackbar.make(getView(), R.string.popup_marked_as_unread, Snackbar.LENGTH_LONG)
                .setAction(R.string.action_undo, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // mark the previously-unread entries as read
                        // from the temporary list
                        for (int i = 0; i < unreadEntries.size(); i++) {
                            Entry entry = unreadEntries.get(i);
                            entry.setRead(true);
                            mFeedDatabase.updateEntry(entry);
                            AsyncTask<Void, Void, Void> markAsReadTask =
                                    new MarkAsReadTask(entry.getEntryId());
                            markAsReadTask.execute();
                        }

                        updateViews(false);
                    }
                })
                .show();
    }

    /**
     * Triggers a network operation to update all of the tags,
     * subscriptions, and entries if it is not currently doing so
     * and if there is a network connection.
     */
    private void updateAll() {
        if (DefaultApplication.isConnected(this) && !mIsUpdating) {
            // restart sync time to default value
            restartHandler();
            mIsUpdating = true;

            boolean firstLogin = SharedPreferencesManager.getValue(
                    SharedPreferencesManager.KEY_BOOLEAN_FIRST_LOGIN,
                    SharedPreferencesManager.VALUE_BOOLEAN_FIRST_LOGIN_DEFAULT);

            if (firstLogin) {
                setRefreshing(true);
            }

            // start network update
            AsyncTask<Void, Void, Void> updateSubscriptionsTask = new UpdateSubscriptionsTask();
            updateSubscriptionsTask.execute((Void) null);
        } else {
            setRefreshing(false);
        }
    }

    /**
     * Refreshes all of the current displays, including the drawer,
     * to display the most recent values.
     */
    private void updateViews(boolean restore) {
        displayUnreadCount();
        displayStarredCount();
        setSelection();

        if (restore) {
            // refresh array lists
            mRecViewEntries.getAdapter().notifyDataSetChanged();
            mRecViewSubscriptions.getAdapter().notifyDataSetChanged();
            saveToggleState();
        } else {
            // restore lists from database
            displayTags();
            displaySubs();
            displayEntries(false);
        }
    }

    /**
     * Updates the current main view to display the most recent
     * entries of the selected category.
     */
    private void displayEntries(boolean loadMore) {
        int limit;
        String date;

        if (loadMore) {
            // increase the scope of entries to load
            limit = VALUE_INTEGER_ENTRY_SIZE_DEFAULT;
            date = mEntries.get(mEntries.size() - 1).getEntryDate();
            mCurrentEntrySize += VALUE_INTEGER_ENTRY_SIZE_DEFAULT;
        } else {
            // completely refresh the list
            limit = mCurrentEntrySize;
            date = null;
            mEntries.clear();
        }

        // get the current display
        String display = SharedPreferencesManager.getValue(
                SharedPreferencesManager.KEY_STRING_DISPLAY,
                SharedPreferencesManager.VALUE_STRING_DISPLAY_DEFAULT);

        // store the entries retrieved from the database
        List<Entry> entries = new ArrayList<>();

        if (mSearchQuery == null || TextUtils.isEmpty(mSearchQuery)) {
            // if not calling searching, falls back to previous display
            // as user would expect
            switch (display) {
                case SharedPreferencesManager.VALUE_STRING_DISPLAY_ALL:
                    entries = mFeedDatabase.getAllEntries(date, limit);
                    break;
                case SharedPreferencesManager.VALUE_STRING_DISPLAY_STARRED:
                    entries = mFeedDatabase.getStarredEntries(date, limit);
                    break;
                case SharedPreferencesManager.VALUE_STRING_DISPLAY_SUB:
                    String subId = SharedPreferencesManager.getValue(
                            SharedPreferencesManager.KEY_STRING_SUB_ID,
                            SharedPreferencesManager.VALUE_STRING_SUB_ID_DEFAULT);
                    if (mFeedDatabase.containsSubscription(subId)) {
                        // subscription still exists
                        entries = mFeedDatabase.getEntriesBySub(subId, date, limit);
                    } else {
                        // subscription does not exist; go to unread
                        // entries and refresh the views
                        SharedPreferencesManager.setValue(SharedPreferencesManager.KEY_STRING_DISPLAY,
                                SharedPreferencesManager.VALUE_STRING_DISPLAY_UNREAD);
                        updateViews(false);
                        return;
                    }
                    break;
                case SharedPreferencesManager.VALUE_STRING_DISPLAY_TAG:
                    String tagId = SharedPreferencesManager.getValue(
                            SharedPreferencesManager.KEY_STRING_TAG_ID,
                            SharedPreferencesManager.VALUE_STRING_TAG_ID_DEFAULT);
                    if (mFeedDatabase.containsTag(tagId)) {
                        // the tag still exists
                        entries = mFeedDatabase.getEntriesByTag(tagId, date, limit);
                    } else {
                        // the tag does not exist; go to unread entries
                        // and refresh the views
                        SharedPreferencesManager.setValue(SharedPreferencesManager.KEY_STRING_DISPLAY,
                                SharedPreferencesManager.VALUE_STRING_DISPLAY_UNREAD);
                        updateViews(false);
                        return;
                    }
                    break;
                case SharedPreferencesManager.VALUE_STRING_DISPLAY_UNREAD:
                    entries = mFeedDatabase.getUnreadEntries(date, limit);
                    break;
            }
        } else {
            // display the results of the query
            entries = mFeedDatabase.getEntriesByQuery(mSearchQuery, date, limit);
        }

        // add the database entries to the main list
        mEntries.addAll(entries);

        // refresh the adapter
        mRecViewEntries.getAdapter().notifyDataSetChanged();

        // don't reset the listener if there are no
        // more entries to load
        if (entries.size() >= mCurrentEntrySize) {
            mScrollListener.resetState();
        }

        // refresh the other views
        setView();
    }

    /**
     * Checks whether the user has notifications enabled and, if so,
     * builds and displays a notifications containing the current
     * unread count and a list of unread entries.
     */
    private void displayNotification() {
        // get current unread count and notification preferences
        boolean notify = SharedPreferencesManager.getValue(
                SharedPreferencesManager.KEY_BOOLEAN_NOTIFY,
                SharedPreferencesManager.VALUE_BOOLEAN_NOTIFY_DEFAULT);

        // only display if user wants notifications and there are new unread notifications
        if (notify) {
            // build the notification
            NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
            builder.setSmallIcon(R.drawable.ic_rss_feed_24dp);
            builder.setLargeIcon(BitmapFactory.decodeResource(this.getResources(),
                    R.mipmap.ic_launcher));
            builder.setAutoCancel(true);
            builder.setOngoing(false);
            builder.setDefaults(Notification.DEFAULT_LIGHTS);

            // get the unread entries list
            List<Entry> entries = mFeedDatabase.getUnreadEntries(null, 5);
            int size = mFeedDatabase.getUnreadCount();
            String contentTitle = String.valueOf(size) + " ";
            StringBuilder contentText = new StringBuilder();

            // display singular or pluralized message depending on
            // the number of unread entries
            if (size == 1) {
                contentTitle += this.getString(R.string.popup_notification_unread_entry);
            } else {
                contentTitle += this.getString(R.string.popup_notification_unread_entries);
            }

            // get expandable inbox style
            builder.setContentTitle(contentTitle);
            NotificationCompat.InboxStyle style = new android.support.v4.app.NotificationCompat.InboxStyle();

            // set a public notification to display non-privacy-sensitive version, so
            // only display the unread count
            NotificationCompat.Builder publicBuilder = new NotificationCompat.Builder(this);
            publicBuilder.setSmallIcon(R.drawable.ic_rss_feed_24dp);
            publicBuilder.setContentTitle(contentTitle);
            builder.setPublicVersion(publicBuilder.build());

            // format the entry names and titles for display
            for (int i = 0; i < entries.size(); i++) {
                Entry entry = entries.get(i);
                String line = entry.getEntryName() + " — "
                        + DefaultApplication.fromHtml(entry.getEntryTitle());
                contentText.append(line);
                contentText.append("\n");
                style.addLine(line);
            }

            // set the inbox style
            builder.setContentText(contentText.toString());
            builder.setStyle(style);

            // determine whether to set a notification sound
            String notificationSound = SharedPreferencesManager.getValue(
                    SharedPreferencesManager.KEY_STRING_NOTIFICATION_URI,
                    SharedPreferencesManager.VALUE_STRING_NOTIFICATION_URI_DEFAULT);
            if (notificationSound != null) {
                builder.setSound(Uri.parse(notificationSound));
            }

            // determine whether to vibrate
            boolean vibrate = SharedPreferencesManager.getValue(
                    SharedPreferencesManager.KEY_BOOLEAN_VIBRATE,
                    SharedPreferencesManager.VALUE_BOOLEAN_VIBRATE_DEFAULT);
            if (vibrate) {
                builder.setVibrate(new long[] {500, 500, 500});
            }

            // set a result intent and add it to the stack
            Intent resultIntent = new Intent(this, MainActivity.class);
            TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
            stackBuilder.addParentStack(MainActivity.class);
            stackBuilder.addNextIntent(resultIntent);

            // set the pending intent
            PendingIntent resultPendingIntent =
                    stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
            builder.setContentIntent(resultPendingIntent);

            // display the notification
            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            if (notificationManager != null) {
                notificationManager.notify(1, builder.build());
            }
        }
    }

    /**
     * Displays the current number of unread entries
     * next to the unread section of the drawer.
     */
    private void displayUnreadCount() {
        // get the unread count
        String count = "";
        int result = mFeedDatabase.getUnreadCount();

        // convert the unread count to a string
        if (result > 0) {
            count = String.valueOf(result);
        }

        // set the unread count in the drawer view
        TextView unreadCount = (TextView)findViewById(R.id.list_drawer_unread_count);
        unreadCount.setText(count);
    }

    /**
     * Displays the current number of starred entries
     * next to the starred section of the drawer.
     */
    private void displayStarredCount() {
        // get the starred count
        String count = "";
        int result = mFeedDatabase.getStarredCount();

        // convert the starre count to a string
        if (result > 0) {
            count = String.valueOf(result);
        }

        // set the starred count in the drawer view
        TextView starredCount = (TextView)findViewById(R.id.list_drawer_starred_count);
        starredCount.setText(count);
    }

    /**
     * Updates the drawer view to display all of the untagged
     * subscriptions.
     */
    private void displaySubs() {
        // clear the current subscriptions
        mSubscriptions.clear();

        // set the new subscriptions
        mSubscriptions.addAll(mFeedDatabase.getUntaggedSubscriptions());

        // refresh the adapter
        mRecViewSubscriptions.getAdapter().notifyDataSetChanged();
    }

    /**
     * Updates the drawer view to display all of the tags and their
     * related subscriptions.
     */
    private void displayTags() {
        // clear current tags
        mTags.clear();

        // set the new tags
        mTags.addAll(mFeedDatabase.getAllTags());

        // destroy and recreate the display of the tags
        saveToggleState();
    }

    /**
     * AsyncTask that deletes a subscription.
     *
     * @author  Logan Garcia
     * @version %I%, %G%
     */
    @SuppressLint("StaticFieldLeak")
    private class DeleteSubscriptionTask extends AsyncTask<Void, Void, Void> {

        String mFeedId;

        /**
         * The default constructor.
         *
         * @param feedId the ID of the subscription to delete
         */
        DeleteSubscriptionTask(String feedId) {
            mFeedId = feedId;
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                JSONArray jsonSubscriptions = Feedbin.getSubscriptions();
                for (int i = 0; i < jsonSubscriptions.length(); i++) {
                    JSONObject jsonSubscription = jsonSubscriptions.getJSONObject(i);
                    if (mFeedId.equals(jsonSubscription.getString(Feedbin.JSON_FEED_ID))) {
                        String subId =
                                jsonSubscription.getString(jsonSubscription.getString(Feedbin.JSON_ID));
                        Feedbin.deleteSubscription(subId);
                        break;
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    /**
     * AsyncTask that deletes a tag.
     *
     * @author  Logan Garcia
     * @version %I%, %G%
     */
    @SuppressLint("StaticFieldLeak")
    private class DeleteTagTask extends AsyncTask<Void, Void, Void> {

        String mTagName;

        /**
         * The default constructor.
         *
         * @param tagName the name o f the tag to delete
         */
        DeleteTagTask(String tagName) {
            mTagName = tagName;
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                JSONArray jsonTags = Feedbin.getTags();
                // delete all tags matching the tag name
                for (int i = 0; i < jsonTags.length(); i++) {
                    JSONObject jsonTag = jsonTags.getJSONObject(i);
                    if (jsonTag.getString(Feedbin.JSON_NAME).equals(mTagName)) {
                        String jsonTagId = jsonTag.getString(Feedbin.JSON_ID);
                        Feedbin.deleteTag(jsonTagId);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }
    }

    /**
     * AsyncTask that marks a feed as unread.
     *
     * @author  Logan Garcia
     * @version %I%, %G%
     */
    @SuppressLint("StaticFieldLeak")
    private class MarkAsUnreadTask extends AsyncTask<Void, Void, Void> {

        private String mEntryId;

        /**
         * The default constructor.
         *
         * @param entryId the ID of the entry to mark as unread
         */
        MarkAsUnreadTask(String entryId) {
            mEntryId = entryId;
        }

        @Override
        protected Void doInBackground(Void... params) {
            // create JSON object of entry to mark as unread
            JSONObject unreadObject = new JSONObject();
            JSONArray unreadArray = new JSONArray();
            unreadArray.put(mEntryId);

            try {
                unreadObject.put(Feedbin.JSON_UNREAD_ENTRIES, unreadArray);
                // add the unread entry
                Feedbin.createUnreadEntry(unreadObject);
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    /**
     * AsyncTask that marks a feed as read.
     *
     * @author  Logan Garcia
     * @version %I%, %G%
     */
    @SuppressLint("StaticFieldLeak")
    private class MarkAsReadTask extends AsyncTask<Void, Void, Void> {

        private String mEntryId;

        /**
         * The default constructor.
         *
         * @param entryId the ID of the entry to mark as read
         */
        MarkAsReadTask(String entryId) {
            mEntryId = entryId;
        }

        @Override
        protected Void doInBackground(Void... params) {
            // crate JSON object of entry to mark as read
            JSONObject readObject = new JSONObject();
            JSONArray readArray = new JSONArray();
            readArray.put(mEntryId);

            try {
                readObject.put(Feedbin.JSON_UNREAD_ENTRIES, readArray);
                // remove the unread entry
                Feedbin.deleteUnreadEntry(readObject);
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    /**
     * AsyncTask that updates all of the unread entries
     * from the server.
     *
     * @author  Logan Garcia
     * @version %I%, %G%
     */
    @SuppressLint("StaticFieldLeak")
    private class UpdateUnreadEntriesTask extends AsyncTask<Void, Void, Void> {
        // set to true if there are new unread entries
        private boolean mNewEntries;

        @Override
        protected Void doInBackground(Void... params) {
            try {
                // set the boolean and get the unread entries
                mNewEntries = false;
                JSONArray jsonUnreadEntries = Feedbin.getUnreadEntries();

                // check if unread entries have been added
                for (int i = 0; i < jsonUnreadEntries.length(); i++) {
                    String entryId = String.valueOf(jsonUnreadEntries.get(i));
                    if (mFeedDatabase.containsEntry(entryId)) {
                        Entry entry = mFeedDatabase.getEntry(entryId);
                        if (entry.isRead()) {
                            // there are new unread entries
                            entry.setRead(false);
                            mFeedDatabase.updateEntry(entry);
                            mNewEntries = true;
                        }
                    }
                }

                // get current unread entries from the local database
                List<Entry> unreadEntries = new ArrayList<>(mFeedDatabase.getUnreadEntries());
                boolean isRead = true;

                // check to see if unread entries were removed
                for (int i = 0; i < unreadEntries.size(); i++) {
                    Entry entry = unreadEntries.get(i);
                    for (int v = 0; v < jsonUnreadEntries.length(); v++) {
                        String entryId = String.valueOf(jsonUnreadEntries.get(v));
                        if (entry.getEntryId().equals(entryId)) {
                            // the unread entry was not removed
                            isRead = false;
                            break;
                        }
                    }

                    if (isRead) {
                        // the unread entry was removed (marked as read)
                        entry.setRead(true);
                        mFeedDatabase.updateEntry(entry);
                    }

                    // reset the boolean value
                    isRead = true;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            if (mNewEntries) {
                displayNotification();
            }

            // update the current views and notifications
            SharedPreferencesManager.setValue(SharedPreferencesManager.KEY_BOOLEAN_FIRST_LOGIN, false);
            setRefreshing(false);
            mIsUpdating = false;
            updateViews(false);
        }
    }

    /**
     * AsyncTask that updates all of the starred entries
     * from the server.
     *
     * @author  Logan Garcia
     * @version %I%, %G%
     */
    @SuppressLint("StaticFieldLeak")
    private class UpdateStarredEntriesTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            try {
                // get the starred entries
                JSONArray jsonStarredEntries = Feedbin.getStarredEntries();

                // check if there were starred entries added
                for (int i = 0; i < jsonStarredEntries.length(); i++) {
                    String entryId = String.valueOf(jsonStarredEntries.get(i));
                    if (mFeedDatabase.containsEntry(entryId)) {
                        // starred entry is already in database
                        Entry entry = mFeedDatabase.getEntry(entryId);
                        if (!entry.isStarred()) {
                            entry.setStarred(true);
                            mFeedDatabase.updateEntry(entry);
                        }
                    } else {
                        // starred entry is not in database; fetch it
                        JSONArray starredArray = Feedbin.getEntry(entryId);
                        JSONObject starredObject = starredArray.getJSONObject(0);

                        String subId = starredObject.getString(Feedbin.JSON_FEED_ID);
                        String entryDate = starredObject.getString(Feedbin.JSON_PUBLISHED);
                        String entryTitle = starredObject.getString(Feedbin.JSON_TITLE);
                        String entryContent = starredObject.getString(Feedbin.JSON_CONTENT);
                        String entryAuthor = starredObject.getString(Feedbin.JSON_AUTHOR);
                        String entryUrl = starredObject.getString(Feedbin.JSON_URL);
                        String entryEnclosure = null;

                        // get the RSS enclosure if there is one
                        if (starredObject.has(Feedbin.JSON_ENCLOSURE)) {
                            JSONObject jsonEnclosure = starredObject.getJSONObject(Feedbin.JSON_ENCLOSURE);
                            entryEnclosure = jsonEnclosure.getString(Feedbin.JSON_ENCLOSURE_URL);
                        }

                        // get the name of the entry if it exists (starred
                        // entries can have no corresponding subscription)
                        if (mFeedDatabase.containsSubscription(subId)) {
                            Subscription subscription = mFeedDatabase.getSubscription(subId);
                            String entryName = subscription.getSubName();

                            // add the new entry to the database if it
                            // has a corresponding subscription
                            Entry entry = new Entry(entryId, subId, entryName, entryDate, entryTitle,
                                    entryContent, entryAuthor, entryUrl, entryEnclosure, true, true);
                            mFeedDatabase.createEntry(entry);
                        }
                    }
                }

                // get current starred entries from the local database
                List<Entry> starredEntries = new ArrayList<>(mFeedDatabase.getStarredEntries());
                boolean isStarred = false;

                // check if there were starred entries removed
                for (int i = 0; i < starredEntries.size(); i++) {
                    Entry entry = starredEntries.get(i);
                    for (int v = 0; v < jsonStarredEntries.length(); v++) {
                        String entryId = String.valueOf(jsonStarredEntries.get(v));
                        if (entry.getEntryId().equals(entryId)) {
                            // the starred entry was not removed
                            isStarred = true;
                            break;
                        }
                    }

                    if (!isStarred) {
                        // the starred entry was removed
                        entry.setStarred(false);
                        mFeedDatabase.updateEntry(entry);
                    }

                    // reset the boolean value
                    isStarred = false;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            AsyncTask<Void, Void, Void> updateUnreadEntriesTask = new UpdateUnreadEntriesTask();
            updateUnreadEntriesTask.execute((Void) null);
        }
    }

    /**
     * AsyncTask that updates all of the entries
     * from the server.
     *
     * @author  Logan Garcia
     * @version %I%, %G%
     */
    @SuppressLint("StaticFieldLeak")
    private class UpdateAllEntriesTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            try {
                // get the since parameter
                String since = SharedPreferencesManager.getValue(
                        SharedPreferencesManager.KEY_STRING_SINCE,
                        SharedPreferencesManager.VALUE_STRING_SINCE_DEFAULT);

                // get all entries since a certain date
                JSONArray jsonAllEntries = Feedbin.getAllEntries(since);

                // update the since parameter
                if (jsonAllEntries.length() > 0) {
                    JSONObject jsonEntry = jsonAllEntries.getJSONObject(0);
                    since = jsonEntry.getString(Feedbin.JSON_CREATED_AT);
                    SharedPreferencesManager.setValue(SharedPreferencesManager.KEY_STRING_SINCE, since);
                }

                // add the new entries if not already added
                for (int i = 0; i < jsonAllEntries.length(); i++) {
                    JSONObject jsonEntry = jsonAllEntries.getJSONObject(i);
                    String entryId = jsonEntry.getString(Feedbin.JSON_ID);
                    if (!mFeedDatabase.containsEntry(entryId)) {
                        String subId = jsonEntry.getString(Feedbin.JSON_FEED_ID);
                        String entryDate = jsonEntry.getString(Feedbin.JSON_PUBLISHED);
                        String entryTitle = jsonEntry.getString(Feedbin.JSON_TITLE);
                        String entryContent = jsonEntry.getString(Feedbin.JSON_CONTENT);
                        String entryAuthor = jsonEntry.getString(Feedbin.JSON_AUTHOR);
                        String entryUrl = jsonEntry.getString(Feedbin.JSON_URL);
                        String entryEnclosure = null;

                        // get the name of the entry
                        Subscription subscription = mFeedDatabase.getSubscription(subId);
                        String entryName = subscription.getSubName();

                        // get the RSS enclosure if there is one
                        if (jsonEntry.has(Feedbin.JSON_ENCLOSURE)) {
                            JSONObject jsonEnclosure = jsonEntry.getJSONObject(Feedbin.JSON_ENCLOSURE);
                            entryEnclosure = jsonEnclosure.getString(Feedbin.JSON_ENCLOSURE_URL);
                        }

                        // add the new entry to the database
                        Entry entry = new Entry(entryId, subId, entryName, entryDate, entryTitle,
                                entryContent, entryAuthor, entryUrl, entryEnclosure, true, false);
                        mFeedDatabase.createEntry(entry);
                    }
                }

                // limit the number of entries per
                // subscription to 100
                trimEntries();
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // update unread entries
            AsyncTask<Void, Void, Void> updateStarredEntriesTask = new UpdateStarredEntriesTask();
            updateStarredEntriesTask.execute((Void) null);
        }
    }

    /**
     * AsyncTask that updates all of the tags from the server.
     *
     * @author  Logan Garcia
     * @version %I%, %G%
     */
    @SuppressLint("StaticFieldLeak")
    private class UpdateTagsTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            try {
                // get the tags
                JSONArray jsonTags = Feedbin.getTags();
                // loop through the tags
                for (int i = 0; i < jsonTags.length(); i++) {
                    JSONObject jsonTag = jsonTags.getJSONObject(i);
                    Tag tag;
                    // determine whether a tag was added
                    if (!mFeedDatabase.containsTagByName(jsonTag.getString(Feedbin.JSON_NAME))) {
                        String tagName = jsonTag.getString(Feedbin.JSON_NAME);
                        List<Subscription> subscriptions = new ArrayList<>();
                        String tagId = String.valueOf(jsonTag.getInt(Feedbin.JSON_ID));
                        int tagCount = 0;
                        tag = new Tag(tagName, subscriptions, tagId, tagCount);
                        mFeedDatabase.createTag(tag);
                    }

                    tag = mFeedDatabase.getTagByName(jsonTag.getString(Feedbin.JSON_NAME));
                    JSONArray jsonSubs = Feedbin.getSubscriptions();
                    // determine whether a new relationship exists between
                    // a subscription and a tag
                    for (int v = 0; v < jsonSubs.length(); v++) {
                        JSONObject jsonSub = jsonSubs.getJSONObject(v);
                        Subscription subscription
                                = mFeedDatabase.getSubscription(jsonSub.getString(Feedbin.JSON_FEED_ID));
                        if (jsonSub.getInt(Feedbin.JSON_FEED_ID) == jsonTag.getInt(Feedbin.JSON_FEED_ID)
                                && !mFeedDatabase.containsCategorizedAs(tag.getTagId(), subscription.getSubId())) {
                            mFeedDatabase.createCategorizedAs(tag.getTagId(), subscription.getSubId());
                            break;
                        }
                    }
                }

                // get current tags from local database
                List<Tag> tags = new ArrayList<>(mFeedDatabase.getAllTags());
                boolean isTagDeleted = true;
                boolean isUnsubbed = true;

                // determine whether a tag was deleted
                for (int i = 0; i < tags.size(); i++) {
                    Tag tag = tags.get(i);
                    for (int v = 0; v < jsonTags.length(); v++) {
                        JSONObject jsonTag = jsonTags.getJSONObject(v);
                        if (tag.getTitle().equals(jsonTag.getString(Feedbin.JSON_NAME))) {
                            // the tag was not deleted
                            isTagDeleted = false;
                        }
                    }

                    if (isTagDeleted) {
                        // the tag was deleted
                        mFeedDatabase.deleteTag(tag.getTagId());
                    } else {
                        // determine whether a relationship between
                        // a tag and a subscription was deleted
                        List<Subscription> subscriptions =
                                new ArrayList<>(mFeedDatabase.getSubscriptionsByTag(tag.getTagId()));
                        for (int v = 0; v < subscriptions.size(); v++) {
                            Subscription subscription = subscriptions.get(v);
                            for (int x = 0; x < jsonTags.length(); x++) {
                                JSONObject jsonTag = jsonTags.getJSONObject(x);
                                if (jsonTag.getString(Feedbin.JSON_FEED_ID).equals(subscription.getSubId())
                                        && tag.getTitle().equals(jsonTag.getString(Feedbin.JSON_NAME))) {
                                    // the relationship was not deleted
                                    isUnsubbed = false;
                                    break;
                                }
                            }

                            if (isUnsubbed) {
                                // the relationship was deleted
                                mFeedDatabase.deleteCategorizedAs(tag.getTagId(), subscription.getSubId());
                            }

                            // reset boolean values
                            isUnsubbed = true;
                        }
                    }

                    // reset boolean value
                    isTagDeleted = true;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            AsyncTask<Void, Void, Void> task = new UpdateAllEntriesTask();
            task.execute((Void) null);
        }
    }

    /**
     * AsyncTask that updates all of the subscriptions
     * from the server.
     *
     * @author  Logan Garcia
     * @version %I%, %G%
     */
    @SuppressLint("StaticFieldLeak")
    private class UpdateSubscriptionsTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            try {
                // get the subscriptions
                JSONArray jsonSubscriptions = Feedbin.getSubscriptions();

                // loop through the subscriptions
                for (int i = 0; i < jsonSubscriptions.length(); i++) {
                    JSONObject jsonSub = jsonSubscriptions.getJSONObject(i);

                    // determine whether a subscription was added
                    if (!mFeedDatabase.containsSubscription(jsonSub.getString(Feedbin.JSON_FEED_ID))) {
                        String subId = String.valueOf(jsonSub.getInt(Feedbin.JSON_FEED_ID));
                        String subName = jsonSub.getString(Feedbin.JSON_TITLE);
                        String subUrl = jsonSub.getString(Feedbin.JSON_FEED_URL);
                        int subCount = 0;
                        Bitmap subImage = getBitmapFromUrl(jsonSub.getString(Feedbin.JSON_SITE_URL));
                        Subscription subscription = new Subscription(subId, subName, subUrl, subCount, subImage);
                        mFeedDatabase.createSub(subscription);
                    } else {
                        // determine whether a subscription was renamed
                        Subscription subscription
                                = mFeedDatabase.getSubscription(String.valueOf(jsonSub.getInt(Feedbin.JSON_FEED_ID)));
                        if (!subscription.getSubName().equals(jsonSub.getString(Feedbin.JSON_TITLE))) {
                            subscription.setSubName(jsonSub.getString(Feedbin.JSON_TITLE));
                            mFeedDatabase.updateSubscription(subscription);
                        }
                    }
                }

                // get current subscriptions from local database
                List<Subscription> subscriptions
                        = new ArrayList<>(mFeedDatabase.getAllSubscriptions());
                boolean isDeleted = true;

                // determine whether a subscription was deleted
                for (int i = 0; i < subscriptions.size(); i++) {
                    Subscription subscription = subscriptions.get(i);
                    for (int v = 0; v < jsonSubscriptions.length(); v++) {
                        JSONObject jsonSub = jsonSubscriptions.getJSONObject(v);
                        if (subscription.getSubId().equals(
                                String.valueOf(jsonSub.getInt(Feedbin.JSON_FEED_ID)))) {
                            // the subscription was not deleted
                            isDeleted = false;
                            break;
                        }
                    }

                    if (isDeleted) {
                        // the subscription was deleted
                        mFeedDatabase.deleteSubscription(subscription.getSubId());
                    }

                    // reset boolean value
                    isDeleted = true;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // update the tags
            AsyncTask<Void, Void, Void> task = new UpdateTagsTask();
            task.execute((Void) null);
        }
    }
}