/* YARR (Yet Another RSS Reader)
 * Copyright (C) 2017  Logan Garcia
 *
 * This file is part of YARR.
 *
 * YARR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * YARR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with YARR.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.cgarcia.yarr.activity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaScannerConnection;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.obsez.android.lib.filechooser.ChooserDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParserException;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import me.cgarcia.yarr.DefaultApplication;
import me.cgarcia.yarr.Opml;
import me.cgarcia.yarr.R;
import me.cgarcia.yarr.SharedPreferencesManager;
import me.cgarcia.yarr.api.Feedbin;
import me.cgarcia.yarr.database.FeedDatabase;
import me.cgarcia.yarr.object.Outline;
import me.cgarcia.yarr.object.Subscription;

/**
 * Activity that enables users to modify preferences.
 *
 * @author  Logan Garcia
 * @version %I%, %G%
 */
public class SettingsActivity extends AppCompatActivity {
    // integers to pass to activities for result
    private static final int ACTIVITY_RINGTONE = 1;

    // declare the database
    private FeedDatabase mFeedDatabase;

    // declare the views in the layout
    private View mSoundView;
    private TextView mSoundNameView;
    private TextView mFrequencyTimeView;
    private AppCompatCheckBox mVibrateView;

    // declare the sync frequency selection
    // and URI of sound
    private int mFrequencySelection;
    private Uri mUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // set the main layout and database
        setContentView(R.layout.activity_settings);
        mFeedDatabase = new FeedDatabase(this);

        //set the toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle(R.string.dialogue_settings);

        // set the toolbar back button
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        // set the email view
        TextView emailView = (TextView)findViewById(R.id.activity_settings_sign_out_subtitle);
        String email = SharedPreferencesManager.getValue(
                SharedPreferencesManager.KEY_STRING_EMAIL,
                SharedPreferencesManager.VALUE_STRING_EMAIL_DEFAULT);
        emailView.setText(email);

        // set the sound view
        mSoundNameView = (TextView) findViewById(R.id.activity_settings_sound_name);
        String ringtone = SharedPreferencesManager.getValue(
                SharedPreferencesManager.KEY_STRING_NOTIFICATION_URI,
                SharedPreferencesManager.VALUE_STRING_NOTIFICATION_URI_DEFAULT);

        // set the name of the current ringtone
        if (ringtone != null) {
            mUri = Uri.parse(ringtone);
            Ringtone soundName = RingtoneManager.getRingtone(this, mUri);
            mSoundNameView.setText(soundName.getTitle(this));
        } else {
            mUri = null;
            mSoundNameView.setText(R.string.dialogue_sound_none);
        }

        // set the frequency
        mFrequencyTimeView = (TextView) findViewById(R.id.activity_settings_sync_frequency_time);
        setFrequency();

        // set the boolean value and listener for night mode
        AppCompatCheckBox nightView = (AppCompatCheckBox) findViewById(R.id.activity_settings_night);
        boolean night = SharedPreferencesManager.getValue(
                SharedPreferencesManager.KEY_BOOLEAN_NIGHT,
                SharedPreferencesManager.VALUE_BOOLEAN_NIGHT_DEFAULT);
        nightView.setChecked(night);
        nightView.setOnClickListener(setupBooleanListener(
                SharedPreferencesManager.KEY_BOOLEAN_NIGHT,
                SharedPreferencesManager.VALUE_BOOLEAN_NIGHT_DEFAULT));

        // set the sign out view and listener
        View signOutView = findViewById(R.id.activity_settings_sign_out);
        signOutView.setOnClickListener(setupLogoutListener());

        // set the sound view and listener
        mSoundView = findViewById(R.id.activity_settings_sound);
        String title = this.getString(R.string.popup_sound);
        mSoundView.setOnClickListener(setupSoundListener(title));

        // set the vibrate view and listener
        mVibrateView = (AppCompatCheckBox)findViewById(R.id.activity_settings_vibrate);
        boolean vibrate = SharedPreferencesManager.getValue(
                SharedPreferencesManager.KEY_BOOLEAN_VIBRATE,
                SharedPreferencesManager.VALUE_BOOLEAN_VIBRATE_DEFAULT);
        mVibrateView.setChecked(vibrate);
        mVibrateView.setOnClickListener(setupBooleanListener(
                SharedPreferencesManager.KEY_BOOLEAN_VIBRATE,
                SharedPreferencesManager.VALUE_BOOLEAN_VIBRATE_DEFAULT));

        // set the notification view and listener
        AppCompatCheckBox notificationView =
                (AppCompatCheckBox) findViewById(R.id.activity_settings_notifications);
        boolean notify = SharedPreferencesManager.getValue(
                SharedPreferencesManager.KEY_BOOLEAN_NOTIFY,
                SharedPreferencesManager.VALUE_BOOLEAN_NOTIFY_DEFAULT);
        notificationView.setChecked(notify);
        setNotifyChildrenState(notify);
        notificationView.setOnClickListener(setupBooleanListener(
                SharedPreferencesManager.KEY_BOOLEAN_NOTIFY,
                SharedPreferencesManager.VALUE_BOOLEAN_NOTIFY_DEFAULT));

        // set the wifi view and listener
        AppCompatCheckBox wifiView =
                (AppCompatCheckBox) findViewById(R.id.activity_settings_sync_wifi);
        boolean wifi = SharedPreferencesManager.getValue(
                SharedPreferencesManager.KEY_BOOLEAN_WIFI,
                SharedPreferencesManager.VALUE_BOOLEAN_WIFI_DEFAULT);
        wifiView.setChecked(wifi);
        wifiView.setOnClickListener(setupBooleanListener(
                SharedPreferencesManager.KEY_BOOLEAN_WIFI,
                SharedPreferencesManager.VALUE_BOOLEAN_WIFI_DEFAULT));

        // set the charging view and listener
        AppCompatCheckBox chargingView =
                (AppCompatCheckBox) findViewById(R.id.activity_settings_sync_charge);
        boolean charging = SharedPreferencesManager.getValue(
                SharedPreferencesManager.KEY_BOOLEAN_CHARGING,
                SharedPreferencesManager.VALUE_BOOLEAN_CHARGING_DEFAULT);
        chargingView.setChecked(charging);
        chargingView.setOnClickListener(setupBooleanListener(
                SharedPreferencesManager.KEY_BOOLEAN_CHARGING,
                SharedPreferencesManager.VALUE_BOOLEAN_CHARGING_DEFAULT));

        // set the frequency view and listener
        View frequencyView = findViewById(R.id.activity_settings_sync_frequency);
        frequencyView.setOnClickListener(setupFrequencyListener());

        // set the import view and listener
        View importView = findViewById(R.id.activity_settings_import);
        importView.setOnClickListener(setupImportListener());

        // set the export view and listener
        View exportView = findViewById(R.id.activity_settings_export);
        exportView.setOnClickListener(setupExportListener());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case ACTIVITY_RINGTONE:
                if (resultCode == RESULT_OK) {
                    // get the new URI
                    mUri = data.getParcelableExtra(RingtoneManager.EXTRA_RINGTONE_PICKED_URI);

                    if (mUri != null) {
                        // URI has a sound
                        SharedPreferencesManager.setValue(
                                SharedPreferencesManager.KEY_STRING_NOTIFICATION_URI,
                                mUri.toString());
                        Ringtone soundName = RingtoneManager.getRingtone(this, mUri);
                        mSoundNameView.setText(soundName.getTitle(this));
                    } else {
                        // URI does not have a sound
                        SharedPreferencesManager.setValue(
                                SharedPreferencesManager.KEY_STRING_NOTIFICATION_URI, null);
                        mSoundNameView.setText(R.string.dialogue_sound_none);
                    }
                }
                break;
        }
    }

    @Override
    public void onBackPressed() {
        // finish the activity
        setResult(SettingsActivity.RESULT_OK);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // on toolbar back button pressed
            case android.R.id.home:
                onBackPressed();
                break;
        }

        return true;
    }

    /**
     * Returns the on click listener used to start the ringtone
     * selection activity and eventually receive the result of
     * the selection.
     *
     * @param title the header of the selection window
     * @return      the listener to start the intent
     */
    private View.OnClickListener setupSoundListener(final String title) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // create the intent
                Intent intent = new Intent(RingtoneManager.ACTION_RINGTONE_PICKER);

                // assign intent values
                intent.putExtra(RingtoneManager.EXTRA_RINGTONE_TYPE,
                        RingtoneManager.TYPE_NOTIFICATION);
                intent.putExtra(RingtoneManager.EXTRA_RINGTONE_TITLE, title);
                intent.putExtra(RingtoneManager.EXTRA_RINGTONE_EXISTING_URI, mUri);
                intent.putExtra(RingtoneManager.EXTRA_RINGTONE_SHOW_SILENT, true);
                intent.putExtra(RingtoneManager.EXTRA_RINGTONE_SHOW_DEFAULT, true);

                // start intent for result
                startActivityForResult(intent, ACTIVITY_RINGTONE);
            }
        };
    }

    /**
     * Returns the on click listener used display a
     * confirmation dialogue window and, upon confirmation,
     * sign out of the application.
     *
     * @return the listener to display the dialogue box
     */
    private View.OnClickListener setupLogoutListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // create the confirmation dialogue box
                new AlertDialog.Builder(SettingsActivity.this)
                        .setTitle(R.string.popup_sign_out_title)
                        .setMessage(R.string.popup_sign_out_message)
                        .setPositiveButton(R.string.popup_sign_out_positive, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // delete the database and reset the preferences
                                mFeedDatabase.deleteDB();
                                SharedPreferencesManager.clear();

                                // start the login activity and close this one
                                Intent intent = new Intent(SettingsActivity.this, LoginActivity.class);
                                startActivity(intent);
                                finish();
                            }

                        })
                        .setNegativeButton(R.string.popup_sign_out_negative, null)
                        .show();
            }
        };
    }

    /**
     * Returns the on click listener used to start the ringtone
     * selection activity and eventually receive the result of
     * the selection.
     *
     * @param key      the key for shared preferences
     * @param defValue the default value for shared preferences
     * @return         the listener to toggle the boolean value
     */
    private View.OnClickListener setupBooleanListener(final String key, final boolean defValue) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // get the negation of the stored value
                boolean newValue = !SharedPreferencesManager.getValue(key, defValue);
                SharedPreferencesManager.setValue(key, newValue);
                ((AppCompatCheckBox) v).toggle();
                ((AppCompatCheckBox) v).setChecked(newValue);

                if (key.equals(SharedPreferencesManager.KEY_BOOLEAN_NOTIFY)) {
                    // enable/disable notification children
                    setNotifyChildrenState(newValue);
                } else if (key.equals(SharedPreferencesManager.KEY_BOOLEAN_NIGHT)) {
                    // set the night mode
                    if (newValue) {
                        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
                    } else {
                        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
                    }

                    // recreate the application on night mode change
                    recreate();
                }
            }
        };
    }

    /**
     * Returns the on click listener used to display a
     * selection to choose the sync frequency of the
     * application.
     *
     * @return the listener to display the selection
     */
    private View.OnClickListener setupFrequencyListener() {
        return new View.OnClickListener() {
            // save the current frequency selection
            int newSelection = mFrequencySelection;
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(SettingsActivity.this)
                        .setTitle(R.string.popup_sync_frequency)
                        .setSingleChoiceItems(R.array.sync, mFrequencySelection, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // set the new frequency selection
                                newSelection = which;
                            }
                        })
                        .setPositiveButton(R.string.popup_sync_positive,
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        // assign new frequency selection to current
                                        // selection on confirmation
                                        mFrequencySelection = newSelection;
                                        int prefsValue = SharedPreferencesManager.VALUE_INTEGER_SYNC_DEFAULT;

                                        // get the new sync frequency value from the new
                                        // position in the array
                                        switch (mFrequencySelection) {
                                            case 0:
                                                prefsValue = SharedPreferencesManager.VALUE_INTEGER_SYNC_NEVER;
                                                break;
                                            case 1:
                                                prefsValue = SharedPreferencesManager.VALUE_INTEGER_SYNC_15_MINUTES;
                                                break;
                                            case 2:
                                                prefsValue = SharedPreferencesManager.VALUE_INTEGER_SYNC_30_MINUTES;
                                                break;
                                            case 3:
                                                prefsValue = SharedPreferencesManager.VALUE_INTEGER_SYNC_1_HOUR;
                                                break;
                                            case 4:
                                                prefsValue = SharedPreferencesManager.VALUE_INTEGER_SYNC_3_HOURS;
                                                break;
                                            case 5:
                                                prefsValue = SharedPreferencesManager.VALUE_INTEGER_SYNC_6_HOURS;
                                                break;
                                            case 6:
                                                prefsValue = SharedPreferencesManager.VALUE_INTEGER_SYNC_12_HOURS;
                                                break;
                                            case 7:
                                                prefsValue = SharedPreferencesManager.VALUE_INTEGER_SYNC_1_DAY;
                                                break;
                                        }

                                        // set the new frequency value
                                        SharedPreferencesManager.setValue(
                                                SharedPreferencesManager.KEY_INTEGER_SYNC, prefsValue);
                                        setFrequency();
                                    }
                                })
                        .setNegativeButton(R.string.popup_sync_negative, null)
                        .show();
            }
        };
    }

    /**
     * Returns the on click listener used to start the
     * file chooser that can then import a valid OPML
     * file.
     *
     * @return the listener to start the file chooser
     */
    private View.OnClickListener setupImportListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (DefaultApplication.isConnected(SettingsActivity.this)) {
                    // open the file chooser and display only opml/xml files
                    new ChooserDialog().with(SettingsActivity.this)
                            .withFilter(false, false, "opml", "xml")
                            .withStartFile(Environment.getExternalStorageDirectory().toString())
                            .withResources(R.string.popup_choose_file_title,
                                    R.string.popup_choose_file_positive,
                                    R.string.popup_choose_file_negative)
                            .withChosenListener(new ChooserDialog.Result() {
                                @Override
                                public void onChoosePath(String path, File pathFile) {
                                    try {
                                        // read the opened file
                                        InputStream inputStream = new FileInputStream(path);
                                        List<Outline> outlines = new ArrayList<>(Opml.read(inputStream));

                                        // loop through the outlines and add them
                                        // as new subscriptions
                                        for (int i = 0; i < outlines.size(); i++) {
                                            Outline outline = outlines.get(i);
                                            String title = outline.getTitle();
                                            String link = outline.getLink();

                                            AsyncTask<Void, Void, Void> addSubscriptionTask =
                                                    new AddSubscriptionTask(link, title);
                                            addSubscriptionTask.execute();
                                        }
                                    } catch (XmlPullParserException | IOException e) {
                                        e.printStackTrace();
                                    }
                                }
                            })
                            .build()
                            .show();
                } else {
                    // display network connection error
                    Snackbar.make(findViewById(R.id.activity_settings), R.string.error_no_network,
                            Snackbar.LENGTH_LONG).show();
                }
            }
        };
    }

    /**
     * Returns the on click listener used open a dialogue
     * box and, upon confirmation, export the user's list
     * of subscriptions to an opml file.
     *
     * @return the listener to export the file
     */
    private View.OnClickListener setupExportListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (DefaultApplication.isStoragePermissionGranted(SettingsActivity.this)) {
                    // display confirmation dialogue box
                    new AlertDialog.Builder(SettingsActivity.this)
                            .setMessage(R.string.popup_export_message)
                            .setPositiveButton(R.string.popup_export_positive,
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            // set the file name
                                            String name = getResources().getString(
                                                    R.string.dialogue_subscriptions);
                                            String fileName = name.toLowerCase() + ".opml";

                                            // get all of the subscriptions
                                            List<Subscription> subscriptions =
                                                    mFeedDatabase.getAllSubscriptions();

                                            try {
                                                // create a new file
                                                File opmlFile = new File(
                                                        Environment.getExternalStoragePublicDirectory(
                                                        Environment.DIRECTORY_DOWNLOADS), fileName);

                                                // create the OPML file
                                                BufferedWriter writer = new BufferedWriter(
                                                        new FileWriter(opmlFile, true));
                                                Opml.write(name, subscriptions, writer);
                                                writer.close();

                                                // confirm that the file was created
                                                MediaScannerConnection.scanFile(SettingsActivity.this,
                                                        new String[]{opmlFile.toString()}, null, null);

                                                // display where the file was exported to
                                                String exportedTo = getResources().getString(
                                                        R.string.popup_export_location) + " " + opmlFile;
                                                Snackbar.make(findViewById(R.id.activity_settings), exportedTo,
                                                        Snackbar.LENGTH_LONG).show();
                                            } catch (IOException e) {
                                                e.printStackTrace();
                                            }
                                        }

                                    })
                            .setNegativeButton(R.string.popup_export_negative, null)
                            .show();
                }
            }
        };
    }

    /**
     * Sets the active/inactive state of notification's child
     * elements depending upon whether notifications are
     * enabled/disabled.
     *
     * @param isEnabled the state of notifications
     */
    private void setNotifyChildrenState(boolean isEnabled) {
        if (isEnabled) {
            // set to completely visible
            mVibrateView.setAlpha(1f);
            mSoundView.setAlpha(1f);
        } else {
            // set to partially visible
            mVibrateView.setAlpha(0.5f);
            mSoundView.setAlpha(0.5f);
        }

        // set clickable based on enabled state
        mVibrateView.setClickable(isEnabled);
        mSoundView.setClickable(isEnabled);
    }

    /**
     * Sets the subtitle text and selection placement within
     * the array of the currently selected sync frequency.
     */
    private void setFrequency() {
        // get the current sync frequency
        int time = SharedPreferencesManager.getValue(SharedPreferencesManager.KEY_INTEGER_SYNC,
                SharedPreferencesManager.VALUE_INTEGER_SYNC_DEFAULT);

        // set text and selection of frequency
        // based on current value
        switch (time) {
            case SharedPreferencesManager.VALUE_INTEGER_SYNC_NEVER:
                mFrequencyTimeView.setText(R.string.popup_sync_never);
                mFrequencySelection = 0;
                break;
            case SharedPreferencesManager.VALUE_INTEGER_SYNC_15_MINUTES:
                mFrequencyTimeView.setText(R.string.popup_sync_15_minutes);
                mFrequencySelection = 1;
                break;
            case SharedPreferencesManager.VALUE_INTEGER_SYNC_30_MINUTES:
                mFrequencyTimeView.setText(R.string.popup_sync_30_minutes);
                mFrequencySelection = 2;
                break;
            case SharedPreferencesManager.VALUE_INTEGER_SYNC_1_HOUR:
                mFrequencyTimeView.setText(R.string.popup_sync_1_hour);
                mFrequencySelection = 3;
                break;
            case SharedPreferencesManager.VALUE_INTEGER_SYNC_3_HOURS:
                mFrequencyTimeView.setText(R.string.popup_sync_3_hours);
                mFrequencySelection = 4;
                break;
            case SharedPreferencesManager.VALUE_INTEGER_SYNC_6_HOURS:
                mFrequencyTimeView.setText(R.string.popup_sync_6_hours);
                mFrequencySelection = 5;
                break;
            case SharedPreferencesManager.VALUE_INTEGER_SYNC_12_HOURS:
                mFrequencyTimeView.setText(R.string.popup_sync_12_hours);
                mFrequencySelection = 6;
                break;
            case SharedPreferencesManager.VALUE_INTEGER_SYNC_1_DAY:
                mFrequencyTimeView.setText(R.string.popup_sync_1_day);
                mFrequencySelection = 7;
                break;
        }
    }

    /**
     * AsyncTask that adds a new subscription.
     *
     * @author  Logan Garcia
     * @version %I%, %G%
     */
    @SuppressLint("StaticFieldLeak")
    private class AddSubscriptionTask extends AsyncTask<Void, Void, Void> {
        // declare the subscription variables
        private String mUrl;
        private String mName;

        /**
         * The default constructor.
         *
         * @param url  the URL of the subscription
         * @param name the name of the subscription
         */
        AddSubscriptionTask(String url, String name) {
            mUrl = url;
            mName = name;
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                // create a new subscription using a JSON object
                JSONObject jsonSubscription = new JSONObject();
                jsonSubscription.put(Feedbin.JSON_FEED_URL, mUrl);
                int status = Feedbin.createSubscription(jsonSubscription);

                if (status == Feedbin.STATUS_OK) {
                    // find the newly created subscription
                    JSONArray jsonSubscriptions = Feedbin.getSubscriptions();
                    for (int i = 0; i < jsonSubscriptions.length(); i++) {
                        jsonSubscription = jsonSubscriptions.getJSONObject(i);
                        if (mUrl.equals(jsonSubscription.getString(Feedbin.JSON_FEED_URL))) {
                            if (!mName.equals(jsonSubscription.getString(Feedbin.JSON_TITLE))) {
                                // update the newly created subscription with
                                // the new name
                                String subId = jsonSubscription.getString(Feedbin.JSON_ID);
                                jsonSubscription = new JSONObject();
                                jsonSubscription.put(Feedbin.JSON_TITLE, mName);
                                Feedbin.updateSubscription(subId, jsonSubscription);
                            }

                            break;
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }
    }
}
