/* YARR (Yet Another RSS Reader)
 * Copyright (C) 2017  Logan Garcia
 *
 * This file is part of YARR.
 *
 * YARR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * YARR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with YARR.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.cgarcia.yarr.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;

import android.os.AsyncTask;

import android.os.Bundle;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

import me.cgarcia.yarr.R;
import me.cgarcia.yarr.SharedPreferencesManager;
import me.cgarcia.yarr.api.Feedbin;

/**
 * Activity that offers login via email/password.
 *
 * @author  Logan Garcia
 * @version %I%, %G%
 */
public class LoginActivity extends AppCompatActivity {

     // keep track of the login task to ensure we can cancel it if requested.
    private UserLoginTask mAuthTask = null;

    // declare view fields
    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView;
    private View mProgressView;
    private View mLoginFormView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle(R.string.dialogue_sign_in);

        SharedPreferencesManager.setValue(SharedPreferencesManager.KEY_STRING_DISPLAY,
                SharedPreferencesManager.VALUE_STRING_DISPLAY_UNREAD);
        SharedPreferencesManager.setValue(SharedPreferencesManager.KEY_BOOLEAN_LOGGED_IN, false);

        // Set up the login form.
        mEmailView = (AutoCompleteTextView) findViewById(R.id.activity_login_email);

        mPasswordView = (EditText) findViewById(R.id.activity_login_password);
        mPasswordView.setOnEditorActionListener(setupEditorListener());

        Button mEmailSignInButton = (Button) findViewById(R.id.activity_login_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        mLoginFormView = findViewById(R.id.activity_login_form);
        mProgressView = findViewById(R.id.activity_login_progress);
    }

    private TextView.OnEditorActionListener setupEditorListener() {
        return new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        };
    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {
        if (mAuthTask != null) {
            return;
        }

        // reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);

        // store values at the time of the login attempt.
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // check that the password is not empty
        if (TextUtils.isEmpty(password)) {
            mPasswordView.setError(getString(R.string.error_field_required));
            focusView = mPasswordView;
            cancel = true;
        }

        // check that the email is not empty
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        }

        if (cancel) {
            // there was an error; don't attempt login and focus the first
            // form field with an error
            focusView.requestFocus();
        } else {
            // show a progress spinner, and kick off a background task to
            // perform the user login attempt
            showProgress(true);
            mAuthTask = new UserLoginTask(email, password);
            mAuthTask.execute((Void) null);
            boolean result = false; //set login unsuccessful flag

            try {
                result = mAuthTask.get(); //get flag from user login task
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }

            //execute code block if result is true
            if (result) {
                //store credentials in preferences
                SharedPreferencesManager.setValue(SharedPreferencesManager.KEY_STRING_EMAIL,
                        email);
                SharedPreferencesManager.setValue(SharedPreferencesManager.KEY_STRING_PASSWORD,
                        password);
                SharedPreferencesManager.setValue(SharedPreferencesManager.KEY_BOOLEAN_LOGGED_IN,
                        true);
                //start new Intent after storing credentials
                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(intent);
            }
        }
    }

    /**
     * Shows the progress UI and hides the login form.
     *
     * @param show the boolean value to show/hide progress
     */
    private void showProgress(final boolean show) {
        // on Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations
        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

        // hide the main view if true
        mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            }
        });

        // show the progress view if true
        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        mProgressView.animate().setDuration(shortAnimTime).alpha(
                show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            }
        });
    }

    /**
     * AsyncTask that offers login via email/password.
     *
     * @author  Logan Garcia
     * @version %I%, %G%
     */
    @SuppressLint("StaticFieldLeak")
    private class UserLoginTask extends AsyncTask<Void, Void, Boolean> {

        private String mEmail;
        private String mPassword;

        /**
         * Thd default constructor.
         *
         * @param email    the user-provided email
         * @param password the user-provided password
         */
        UserLoginTask(String email, String password) {
            mEmail = email;
            mPassword = password;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                int status = Feedbin.authenticate(mEmail, mPassword);
                if (status == Feedbin.STATUS_OK) {
                    return true;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            //default to returning false if status is not OK
            return false;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthTask = null;
            showProgress(false);

            if (success) {
                finish();
            } else {
                // failed to authenticate the credentials
                Snackbar.make(mLoginFormView, R.string.error_incorrect_email_password,
                        Snackbar.LENGTH_LONG).show();
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showProgress(false);
        }
    }
}