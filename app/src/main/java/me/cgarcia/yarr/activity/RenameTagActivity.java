/* YARR (Yet Another RSS Reader)
 * Copyright (C) 2017  Logan Garcia
 *
 * This file is part of YARR.
 *
 * YARR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * YARR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with YARR.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.cgarcia.yarr.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import me.cgarcia.yarr.DefaultApplication;
import me.cgarcia.yarr.R;
import me.cgarcia.yarr.SharedPreferencesManager;
import me.cgarcia.yarr.api.Feedbin;
import me.cgarcia.yarr.database.FeedDatabase;
import me.cgarcia.yarr.object.Subscription;
import me.cgarcia.yarr.object.Tag;

/**
 * Activity which manages the renaming of a tag.
 *
 * @author  Logan Garcia
 * @version %I%, %G%
 */
public class RenameTagActivity extends AppCompatActivity {
    // declare the database and progress
    private FeedDatabase mFeedDatabase;
    private boolean mProgress;

    // store values from user in variables
    private String mOldName;
    private String mNewName;
    private String mTagId;

    // declare layout fields
    private EditText mNameView;
    private View mProgressView;
    private View mRenameTagView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // set the main layout and API
        setContentView(R.layout.activity_rename_tag);
        DefaultApplication.setupApi();

        //set the toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle(R.string.dialogue_rename_tag);

        // set the toolbar back button
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        // set the database and initial progress value
        mFeedDatabase = new FeedDatabase(this);
        mProgress = false;

        // set the layout fields
        mNameView = (EditText) findViewById(R.id.activity_rename_tag_field);
        mProgressView = findViewById(R.id.activity_rename_tag_progress);
        mRenameTagView = findViewById(R.id.activity_rename_tag_form);

        // get the current tag name
        mTagId = SharedPreferencesManager.getValue(SharedPreferencesManager.KEY_STRING_TAG_ID,
                SharedPreferencesManager.VALUE_STRING_TAG_ID_DEFAULT);
        Tag tag = mFeedDatabase.getTag(mTagId);
        mOldName = tag.getTitle();

        // set the current tag name in the text field on startup
        if (savedInstanceState == null) {
            mNameView.setText(mOldName);
            mNameView.setSelection(mNameView.getText().length());
        }
    }

    @Override
    public void onBackPressed() {
        // close activity on back pressed
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_rename_tag, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // on toolbar back button pressed
            case android.R.id.home:
                onBackPressed();
                break;
            // rename tag if not currently renaming
            case R.id.menu_rename_tag_done:
                if (!mProgress) {
                    attemptRenameTag();
                }
                break;
        }
        return true;
    }

    /**
     * Renames a tag if the correct conditions are achieved.
     */
    private void attemptRenameTag() {
        // reset errors and get user input
        mNameView.setError(null);
        mNewName = mNameView.getText().toString();

        // reset focus and cancel values
        boolean cancel = false;
        View focusView = null;

        // make sure required name is not empty
        if (TextUtils.isEmpty(mNewName)) {
            mNameView.setError(getString(R.string.error_field_required));
            focusView = mNameView;
            cancel = true;
        }

        if (cancel) {
            // there was an error; don't attempt login and focus the first
            // form field with an error
            focusView.requestFocus();
        } else if (mNewName.equals(mOldName)) {
            // nothing was modified
            finish();
        } else if (!DefaultApplication.isConnected(this)) {
            // display error if no connection
            Snackbar.make(mRenameTagView, R.string.error_no_network,
                    Snackbar.LENGTH_LONG).show();
        } else {
            // show progress spinner
            showProgress(true);

            // delete old tag and create new tag with same subscriptions
            Tag oldTag = mFeedDatabase.getTag(mTagId);
            List<Subscription> subs = mFeedDatabase.getSubscriptionsByTag(mTagId);
            Tag newTag = new Tag(mNewName, oldTag.getItems(), mTagId, oldTag.getTagCount());
            mFeedDatabase.deleteTag(mTagId);
            mFeedDatabase.createTag(newTag);

            // add previous subscriptions to new tag
            for (int i = 0; i < subs.size(); i++) {
                mFeedDatabase.createCategorizedAs(mTagId, subs.get(i).getSubId());
            }

            // change the tag name on the network
            AsyncTask<Void, Void, Void> renameTagTask = new RenameTagTask();
            renameTagTask.execute((Void) null);
        }
    }

    /**
     * Shows the progress UI and hides the login form.
     *
     * @param show the boolean value to show/hide progress
     */
    private void showProgress(final boolean show) {
        // on Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations
        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
        mProgress = show;

        // hide main view layout if true
        mRenameTagView.setVisibility(show ? View.GONE : View.VISIBLE);
        mRenameTagView.animate().setDuration(shortAnimTime).alpha(
                show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mRenameTagView.setVisibility(show ? View.GONE : View.VISIBLE);
            }
        });

        // show progress view if true
        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        mProgressView.animate().setDuration(shortAnimTime).alpha(
                show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            }
        });
    }

    /**
     * AsyncTask that attempts to modify the name of a tag.
     *
     * @author  Logan Garcia
     * @version %I%, %G%
     */
    @SuppressLint("StaticFieldLeak")
    private class RenameTagTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            try {
                // get the tags from the network
                JSONArray jsonTags = Feedbin.getTags();
                List<String> feedIds = new ArrayList<>();

                // delete the old tags and save their IDs
                for (int i = 0; i < jsonTags.length(); i++) {
                    if (jsonTags.getJSONObject(i).getString(Feedbin.JSON_NAME).equals(mOldName)) {
                        feedIds.add(jsonTags.getJSONObject(i).getString(Feedbin.JSON_FEED_ID));
                        String tagId = jsonTags.getJSONObject(i).getString(Feedbin.JSON_ID);
                        Feedbin.deleteTag(tagId);
                    }
                }

                // create new tags with the same feed ID in order
                // to connect them to the subscriptions
                for (int i = 0; i < feedIds.size(); i++) {
                    String feedId = feedIds.get(i);
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put(Feedbin.JSON_FEED_ID, feedId);
                    jsonObject.put(Feedbin.JSON_NAME, mNewName);
                    Feedbin.createTag(jsonObject);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // finish the activity
            setResult(AddSubscriptionActivity.RESULT_OK);
            finish();
        }
    }
}
