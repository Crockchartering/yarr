/* YARR (Yet Another RSS Reader)
 * Copyright (C) 2017  Logan Garcia
 *
 * This file is part of YARR.
 *
 * YARR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * YARR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with YARR.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.cgarcia.yarr.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.MultiAutoCompleteTextView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import me.cgarcia.yarr.DefaultApplication;
import me.cgarcia.yarr.R;
import me.cgarcia.yarr.SharedPreferencesManager;
import me.cgarcia.yarr.api.Feedbin;
import me.cgarcia.yarr.database.FeedDatabase;
import me.cgarcia.yarr.object.Subscription;
import me.cgarcia.yarr.object.Tag;

/**
 * Activity that manages the editing of current subscriptions.
 *
 * @author  Logan Garcia
 * @version %I%, %G%
 */
public class EditSubscriptionActivity extends AppCompatActivity {
    // declare the database
    private FeedDatabase mFeedDatabase;

    // declare subscription and tags
    private Subscription mSubscription;
    private List<Tag> mSubTags;

    // hold the user input from the fields
    private String mNewName;
    private String mNewTags;

    // determine whether there is loading
    private boolean mProgress;

    // the view fields of the layout
    private EditText mNameView;
    private MultiAutoCompleteTextView mTagsView;
    private TextView mTagsHelpView;
    private View mProgressView;
    private View mEditSubscriptionView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // set the main layout
        setContentView(R.layout.activity_edit_subscription);
        DefaultApplication.setupApi();

        //set the toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle(R.string.dialogue_edit_subscription);

        // set the toolbar back button
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        // set the database
        mFeedDatabase = new FeedDatabase(this);
        mProgress = false;

        // set the view fields
        mNameView = (EditText) findViewById(R.id.activity_edit_subscription_name);
        mTagsView = (MultiAutoCompleteTextView) findViewById(R.id.activity_edit_subscription_tags);
        mProgressView = findViewById(R.id.activity_edit_subscription_progress);
        mEditSubscriptionView = findViewById(R.id.activity_edit_subscription);
        mTagsHelpView = (TextView) findViewById(R.id.activity_edit_subscription_help);

        // get the subscriptions and tags from the subscription ID
        String subId = SharedPreferencesManager.getValue(
                SharedPreferencesManager.KEY_STRING_SUB_ID,
                SharedPreferencesManager.VALUE_STRING_SUB_ID_DEFAULT);
        mSubscription = mFeedDatabase.getSubscription(subId);
        mSubTags = mFeedDatabase.getTagsBySubscription(subId);

        // set the tag names for auto-completion
        List<Tag> tags = mFeedDatabase.getAllTags();
        List<String> tagNames = new ArrayList<>();

        for (int i = 0; i < tags.size(); i++) {
            tagNames.add(i, tags.get(i).getTitle());
        }

        // set the tag field as comma-separated list
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_dropdown_item_1line, tagNames);
        mTagsView = (MultiAutoCompleteTextView) findViewById(R.id.activity_edit_subscription_tags);
        mTagsView.setAdapter(adapter);
        mTagsView.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
        mTagsView.setOnFocusChangeListener(setupHelpView());

        // set the field values for the first time
        if (savedInstanceState == null) {
            mNameView.setText(mSubscription.getSubName());
            mNameView.setSelection(mNameView.getText().length());
            StringBuilder subTagNames = new StringBuilder();

            // loop through the tag names and set them in the field
            for (int i = 0; i < mSubTags.size() - 1; i++) {
                subTagNames.append(mSubTags.get(i).getTitle()).append(", ");
            }

            // last tag name does not have a comma at the end
            if (mSubTags.size() > 0) {
                subTagNames.append(mSubTags.get(mSubTags.size() - 1).getTitle());
            }

            mTagsView.setText(subTagNames.toString());
            mTagsView.setSelection(mTagsView.getText().length());
        }
    }

    @Override
    public void onBackPressed() {
        // finish the activity on back pressed
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_edit_subscription, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // on toolbar back button pressed
            case android.R.id.home:
                onBackPressed();
                return true;
            // on completion button press
            case R.id.menu_edit_subscription_done:
                // do not call method if already called
                if (!mProgress) {
                    attemptEditSubscription();
                }
                break;
        }

        return true;
    }

    /**
     * Returns the focus change listener that shows/hides
     * the tag hint below the tag field.
     *
     * @return the on focus change listener
     */
    private View.OnFocusChangeListener setupHelpView() {
        return new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                // the tag view has focus
                if (hasFocus) {
                    mTagsHelpView.setVisibility(View.VISIBLE);
                } else {
                    mTagsHelpView.setVisibility(View.INVISIBLE);
                }
            }
        };
    }

    /**
     * Edits a subscription if the correct conditions are achieved.
     */
    private void attemptEditSubscription() {
        // reset the error fields
        mNameView.setError(null);
        mTagsView.setError(null);

        // get the user-provided values from the text fields
        mNewName = mNameView.getText().toString();
        mNewTags = mTagsView.getText().toString();

        // reset cancel and focus
        boolean cancel = false;
        View focusView = null;

        // check if required name exists
        if (TextUtils.isEmpty(mNewName)) {
            mNameView.setError(getString(R.string.error_field_required));
            focusView = mNameView;
            cancel = true;
        }

        if (cancel) {
            // there was an error; don't attempt login and focus the first
            // form field with an error
            focusView.requestFocus();
        } else if (!DefaultApplication.isConnected(this)) {
            // there is no network connection
            Snackbar.make(mEditSubscriptionView, R.string.error_no_network,
                    Snackbar.LENGTH_LONG).show();
        } else if (!mSubscription.getSubName().equals(mNewName)) {
            // the new subscription name does not equal the old one, so
            // update the name
            showProgress(true);
            mSubscription.setSubName(mNewName);
            mFeedDatabase.updateSubscription(mSubscription);
            String subId = mSubscription.getSubId();
            AsyncTask<Void, Void, Void> editSubscriptionNameTask = new EditSubscriptionNameTask(subId);
            editSubscriptionNameTask.execute((Void) null);
        } else {
            // update the tags only
            showProgress(true);
            AsyncTask<Void, Void, Void> editSubscriptionTagsTask = new EditSubscriptionTagsTask();
            editSubscriptionTagsTask.execute((Void) null);
        }
    }

    /**
     * Shows the progress UI and hides the login form.
     *
     * @param show the boolean value to show/hide progress
     */
    private void showProgress(final boolean show) {
        // on Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations
        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
        mProgress = show;

        // hide main view if true
        mEditSubscriptionView.setVisibility(show ? View.GONE : View.VISIBLE);
        mEditSubscriptionView.animate().setDuration(shortAnimTime).alpha(
                show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mEditSubscriptionView.setVisibility(show ? View.GONE : View.VISIBLE);
            }
        });

        // show progress view if true
        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        mProgressView.animate().setDuration(shortAnimTime).alpha(
                show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            }
        });
    }

    /**
     * AsyncTask that attempts to modify the name of a subscription.
     *
     * @author  Logan Garcia
     * @version %I%, %G%
     */
    @SuppressLint("StaticFieldLeak")
    private class EditSubscriptionNameTask extends AsyncTask<Void, Void, Void> {
        // the ID of the subscription to rename
        private String mFeedId;

        /**
         * The default constructor.
         *
         * @param feedId the ID of the subscription to modify
         */
        EditSubscriptionNameTask(String feedId) {
            mFeedId = feedId;
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                // update subscription using a JSON object
                JSONArray jsonSubscriptions = Feedbin.getSubscriptions();
                for (int i = 0; i < jsonSubscriptions.length(); i++) {
                    JSONObject jsonSubscription = jsonSubscriptions.getJSONObject(i);
                    if (mFeedId.equals(jsonSubscription.getString(Feedbin.JSON_FEED_ID))) {
                        String subId = jsonSubscription.getString(Feedbin.JSON_ID);
                        JSONObject jsonSubName = new JSONObject();
                        jsonSubName.put(Feedbin.JSON_TITLE, mNewName);
                        Feedbin.updateSubscription(subId, jsonSubName);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // edit the tags
            AsyncTask<Void, Void, Void> editSubscriptionTagsTask = new EditSubscriptionTagsTask();
            editSubscriptionTagsTask.execute((Void) null);
        }
    }

    /**
     * AsyncTask that attempts to tag a subscription.
     *
     * @author  Logan Garcia
     * @version %I%, %G%
     */
    @SuppressLint("StaticFieldLeak")
    private class EditSubscriptionTagsTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            try {
                // get the list of tags
                JSONArray jsonTags = Feedbin.getTags();
                String feedId = mSubscription.getSubId();
                List<String> tagList = Arrays.asList(mNewTags.split(","));
                boolean isAdded = true;

                // determine whether a tag has been added
                for (int i = 0; i < tagList.size(); i++) {
                    String tagName = tagList.get(i);
                    for (int v = 0; v < mSubTags.size(); v++) {
                        Tag tag = mSubTags.get(v);
                        // tag exists; do not add it
                        if (tagName.equals(tag.getTitle())) {
                            isAdded = false;
                        }
                    }

                    // a new tag was added
                    if (isAdded) {
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put(Feedbin.JSON_FEED_ID, feedId);
                        jsonObject.put(Feedbin.JSON_NAME, tagName);
                        Feedbin.createTag(jsonObject);
                    }

                    // reset boolean value
                    isAdded = true;
                }

                boolean isRemoved = true;

                // determine whether a tag was removed
                for (int i = 0; i < mSubTags.size(); i++) {
                    Tag tag = mSubTags.get(i);
                    for (int v = 0; v < tagList.size(); v++) {
                        String tagName = tagList.get(v);
                        // the tag was not removed
                        if (tag.getTitle().equals(tagName)) {
                            isRemoved = false;
                            break;
                        }
                    }

                    // the tag was removed
                    if (isRemoved) {
                        // find the tag and remove it
                        for (int v = 0; v < jsonTags.length(); v++) {
                            JSONObject jsonTag = jsonTags.getJSONObject(v);
                            if (jsonTag.getString(Feedbin.JSON_FEED_ID).equals(feedId)
                                    && jsonTag.getString(Feedbin.JSON_NAME).equals(tag.getTitle())) {
                                String tagId = jsonTag.getString(Feedbin.JSON_ID);
                                Feedbin.deleteTag(tagId);
                                break;
                            }
                        }
                    }

                    // reset the boolean value
                    isRemoved = true;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // finish the activity
            setResult(AddSubscriptionActivity.RESULT_OK);
            finish();
        }
    }
}
