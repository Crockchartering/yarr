/* YARR (Yet Another RSS Reader)
 * Copyright (C) 2017  Logan Garcia
 *
 * This file is part of YARR.
 *
 * YARR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * YARR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with YARR.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.cgarcia.yarr.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.webkit.WebView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import me.cgarcia.yarr.DefaultApplication;
import me.cgarcia.yarr.SharedPreferencesManager;
import me.cgarcia.yarr.api.Feedbin;
import me.cgarcia.yarr.extension.ExtendedViewPager;
import me.cgarcia.yarr.R;
import me.cgarcia.yarr.database.FeedDatabase;
import me.cgarcia.yarr.fragment.ContentFragment;
import me.cgarcia.yarr.object.Entry;

/**
 * Activity that displays the content of entries.
 *
 * @author  Logan Garcia
 * @version %I%, %G%
 */
public class ContentActivity extends AppCompatActivity {
    // passed arguments from an intent
    private static final String KEY_STRING_ARRAY_ENTRY_IDS = "entry_ids";
    private static final String KEY_BOOLEAN_FULLSCREEN = "fullscreen";

    // declare the entries and database
    private List<Entry> mEntries = new ArrayList<>();
    private FeedDatabase mFeedDatabase;

    // declare the view pager
    private ExtendedViewPager mViewPager;

    // true if fullscreen is enabled
    private boolean mIsFullscreen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // load dummy webview once to avoid day/night bug
        if (Build.VERSION.SDK_INT >= 24) {
            new WebView(this);
        }

        super.onCreate(savedInstanceState);

        // set the main layout
        setContentView(R.layout.activity_content);
        DefaultApplication.setupApi();

        // set the database and retrieve the entries
        mFeedDatabase = new FeedDatabase(this);
        getEntries();

        // get the current position of the page
        int position = SharedPreferencesManager.getValue(
                SharedPreferencesManager.KEY_INTEGER_POSITION,
                SharedPreferencesManager.VALUE_INTEGER_POSITION_DEFAULT);

        // set the view pager with the current position
        mViewPager = (ExtendedViewPager) findViewById(R.id.activity_content_viewpager);
        FragmentStatePagerAdapter adapterViewPager = new ContentAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(adapterViewPager);
        mViewPager.setCurrentItem(position);
        mViewPager.addOnPageChangeListener(setupOnPageListener());
        mViewPager.setOffscreenPageLimit(1);

        // set the toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("");

        // set the toolbar back button
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        // get boolean fullscreen
        mIsFullscreen = savedInstanceState != null && savedInstanceState.getBoolean(KEY_BOOLEAN_FULLSCREEN);
        mViewPager.setPagingEnabled(!mIsFullscreen);

        // hide toolbar if fullscreen
        if (mIsFullscreen) {
            onShowCustomView();
        }

        // get entry at current position
        Entry entry = mEntries.get(position);

        // set the first opened entry to read if unread
        if (!entry.isRead() && savedInstanceState == null && DefaultApplication.isConnected(this)) {
            entry.setRead(true);
            mFeedDatabase.updateEntry(entry);

            AsyncTask<Void, Void, Void> markAsReadTask = new MarkAsReadTask(entry.getEntryId());
            markAsReadTask.execute((Void) null);
        }
    }

    @Override
    public void onBackPressed() {
        // finish the activity on back pressed if
        // not in full screen mode
        if (!mIsFullscreen) {
            setResult(ContentActivity.RESULT_OK);
            finish();
        }

        // still in full screen mode
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_content, menu);
        // get entry from current position
        int position = mViewPager.getCurrentItem();
        Entry entry = mEntries.get(position);

        if (menu != null) {
            // set menu items
            MenuItem itemRead = menu.findItem(R.id.menu_content_read);
            MenuItem itemStar = menu.findItem(R.id.menu_content_star);

            if (entry.isRead()) {
                // set proper read icon and title
                itemRead.setIcon(R.drawable.ic_read_24dp);
                itemRead.setTitle(R.string.action_mark_unread);
            } else {
                // set proper unread icon and title
                itemRead.setIcon(R.drawable.ic_unread_24dp);
                itemRead.setTitle(R.string.action_mark_read);
            }

            if (entry.isStarred()) {
                // set proper star icon and title
                itemStar.setIcon(R.drawable.ic_starred_24dp);
                itemStar.setTitle(R.string.action_remove_star);
            } else {
                // set proper unstar icon and title
                itemStar.setIcon(R.drawable.ic_unstarred_24dp);
                itemStar.setTitle(R.string.action_add_star);
            }
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // get entry from current position
        int position = mViewPager.getCurrentItem();
        Entry entry = mEntries.get(position);

        switch (item.getItemId()) {
            // on toolbar back button pressed
            case android.R.id.home:
                onBackPressed();
                break;
            // on read/unread button pressed
            case R.id.menu_content_read:
                if (DefaultApplication.isConnected(this)) {
                    if (entry.isRead()) {
                        // mark as unread if read and change icon/title
                        AsyncTask<Void, Void, Void> markAsUnreadTask = new MarkAsUnreadTask(entry.getEntryId());
                        markAsUnreadTask.execute((Void) null);

                        item.setIcon(R.drawable.ic_unread_24dp);
                        item.setTitle(R.string.action_mark_read);
                    } else {
                        // mark as read if unread and change icon/title
                        AsyncTask<Void, Void, Void> markAsReadTask = new MarkAsReadTask(entry.getEntryId());
                        markAsReadTask.execute((Void) null);

                        item.setIcon(R.drawable.ic_read_24dp);
                        item.setTitle(R.string.action_mark_unread);
                    }

                    // update database with new read value
                    entry.setRead(!entry.isRead());
                    mFeedDatabase.updateEntry(entry);
                } else {
                    Snackbar.make(findViewById(R.id.activity_content), R.string.error_no_network,
                            Snackbar.LENGTH_LONG).show();
                }
                break;
            // on star/unstar button pressed
            case R.id.menu_content_star:
                if (DefaultApplication.isConnected(this)) {
                    if (entry.isStarred()) {
                        // remove star if starred and change icon/title
                        AsyncTask<Void, Void, Void> removeStarTask = new RemoveStarTask(entry.getEntryId());
                        removeStarTask.execute((Void) null);

                        item.setIcon(R.drawable.ic_unstarred_24dp);
                        item.setTitle(R.string.action_add_star);
                    } else {
                        // add star if unstarred and change icon/title
                        AsyncTask<Void, Void, Void> addStarTask = new AddStarTask(entry.getEntryId());
                        addStarTask.execute((Void) null);

                        item.setIcon(R.drawable.ic_starred_24dp);
                        item.setTitle(R.string.action_remove_star);
                    }

                    // update database with new star value
                    entry.setStarred(!entry.isStarred());
                    mFeedDatabase.updateEntry(entry);
                } else {
                    Snackbar.make(findViewById(R.id.activity_content), R.string.error_no_network,
                            Snackbar.LENGTH_LONG).show();
                }
                break;
            case R.id.menu_content_share:
                // open the share menu
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(Intent.EXTRA_SUBJECT, entry.getEntryTitle());
                sharingIntent.putExtra(Intent.EXTRA_TEXT, entry.getEntryUrl());
                startActivity(Intent.createChooser(sharingIntent, this.getString(R.string.dialogue_share)));
                break;
            case R.id.menu_content_open:
                // open the web browser
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(entry.getEntryUrl()));
                startActivity(browserIntent);
                break;
        }

        return true;
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        // save current position of page in shared preferences
        SharedPreferencesManager.setValue(SharedPreferencesManager.KEY_INTEGER_POSITION,
                mViewPager.getCurrentItem());
        savedInstanceState.putBoolean(KEY_BOOLEAN_FULLSCREEN, mIsFullscreen);

        super.onSaveInstanceState(savedInstanceState);
    }

    /**
     * Returns the page change listener that marks an entry
     * as read if it is unread.
     *
     * @return the page change listener
     */
    private ViewPager.SimpleOnPageChangeListener setupOnPageListener() {
        return new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                // get entry at page position
                Entry entry = mEntries.get(position);

                // mark entry as read if unread and if there is a network connection
                if (!entry.isRead() && DefaultApplication.isConnected(ContentActivity.this)) {
                    entry.setRead(true);
                    mFeedDatabase.updateEntry(entry);

                    AsyncTask<Void, Void, Void> markAsReadTask = new MarkAsReadTask(entry.getEntryId());
                    markAsReadTask.execute((Void) null);
                }

                // reload the toolbar menu to reflect values of current entry
                invalidateOptionsMenu();
            }
        };
    }

    /**
     * Instantiates the entries that are passed to it by an intent.
     */
    private void getEntries() {
        // get the entry ids from the arguments
        Bundle bundle = this.getIntent().getExtras();
        String[] entryIds = new String[0];
        if (bundle != null) {
            entryIds = bundle.getStringArray(KEY_STRING_ARRAY_ENTRY_IDS);
        }

        // add entries from the database
        if (entryIds != null) {
            for (String entryId : entryIds) {
                Entry entry = mFeedDatabase.getEntry(entryId);
                mEntries.add(entry);
            }
        }
    }

    /**
     * Expands a view to fullscreen and called from
     * within the fragment that contains the view.
     */
    public void onShowCustomView() {
        // hide the toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }

        // keep screen on
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        // set fullscreen to true
        mIsFullscreen = true;
        mViewPager.setPagingEnabled(false);
    }

    /**
     * Collapses a view from fullscreen and called from
     * within the fragment that contains the view.
     */
    public void onHideCustomView() {
        // display the toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().show();
        }

        // do not try and keep the screen on
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        // set fullscreen to false
        mIsFullscreen = false;
        mViewPager.setPagingEnabled(true);
    }

    /**
     * FragmentStatePageAdapter that manages the fragments
     * belonging to the activity.
     *
     * @author  Logan Garcia
     * @version %I%, %G%
     */
    private class ContentAdapter extends FragmentStatePagerAdapter {

        /**
         * The default constructor.
         *
         * @param fragmentManager the transaction for modifying fragments
         */
        private ContentAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public int getCount() {
            return mEntries.size();
        }

        @Override
        public Fragment getItem(int position) {
            // pass entry data to new fragment
            Entry entry = mEntries.get(position);
            String title = entry.getEntryTitle();
            String author = entry.getEntryAuthor();
            String date = entry.getEntryDate();
            String name = entry.getEntryName();
            String enclosure = entry.getEntryEnclosure();
            String body = entry.getEntryContent();
            return ContentFragment.newInstance(title, author, date, name, enclosure, body);
        }
    }

    /**
     * AsyncTask that marks an entry as read.
     *
     * @author  Logan Garcia
     * @version %I%, %G%
     */
    @SuppressLint("StaticFieldLeak")
    private class MarkAsReadTask extends AsyncTask<Void, Void, Void> {
        // the ID of the entry mark as read
        private String mEntryId;

        /**
         * The default constructor.
         *
         * @param entryId the ID of the entry to mark as read
         */
        MarkAsReadTask(String entryId) {
            mEntryId = entryId;
        }

        @Override
        protected Void doInBackground(Void... params) {
            // create the JSON object from the entry data
            JSONArray jsonArray = new JSONArray();
            JSONObject readObject = new JSONObject();
            jsonArray.put(mEntryId);

            try {
                readObject.put(Feedbin.JSON_UNREAD_ENTRIES, jsonArray);
                // remove the unread entry
                Feedbin.deleteUnreadEntry(readObject);
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }
    }

    /**
     * AsyncTask that marks an entry as unread.
     *
     * @author  Logan Garcia
     * @version %I%, %G%
     */
    @SuppressLint("StaticFieldLeak")
    private class MarkAsUnreadTask extends AsyncTask<Void, Void, Void> {
        // the ID of the entry to mark as unread
        private String mEntryId;

        /**
         * The default constructor.
         *
         * @param entryId the ID of the entry to mark as unread
         */
        MarkAsUnreadTask(String entryId) {
            mEntryId = entryId;
        }

        @Override
        protected Void doInBackground(Void... params) {
            // create the JSON object from the entry data
            JSONArray unreadArray = new JSONArray();
            JSONObject unreadObject = new JSONObject();
            unreadArray.put(mEntryId);

            try {
                unreadObject.put(Feedbin.JSON_UNREAD_ENTRIES, unreadArray);
                // add the unread entry
                Feedbin.createUnreadEntry(unreadObject);
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }
    }

    /**
     * AsyncTask that adds a star to an entry.
     *
     * @author  Logan Garcia
     * @version %I%, %G%
     */
    @SuppressLint("StaticFieldLeak")
    private class AddStarTask extends AsyncTask<Void, Void, Void> {
        // the ID of the entry to star
        private String mEntryId;

        /**
         * The default constructor.
         *
         * @param entryId the ID of the entry to star
         */
        AddStarTask(String entryId) {
            mEntryId = entryId;
        }

        @Override
        protected Void doInBackground(Void... params) {
            // create the JSON object from the entry data
            JSONArray starredArray = new JSONArray();
            JSONObject starredObject = new JSONObject();
            starredArray.put(mEntryId);

            try {
                starredObject.put(Feedbin.JSON_STARRED_ENTRIES, starredArray);
                // add the starred entry
                Feedbin.createStarredEntry(starredObject);
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }
    }

    /**
     * AsyncTask that removes a star from an entry.
     *
     * @author  Logan Garcia
     * @version %I%, %G%
     */
    @SuppressLint("StaticFieldLeak")
    private class RemoveStarTask extends AsyncTask<Void, Void, Void> {
        // the ID of the entry to unstar
        private String mEntryId;

        /**
         * The default constructor.
         *
         * @param entryId the ID of the entry to unstar
         */
        RemoveStarTask(String entryId) {
            mEntryId = entryId;
        }

        @Override
        protected Void doInBackground(Void... params) {
            // create the JSON object from the entry data
            JSONArray unstarredArray = new JSONArray();
            JSONObject unstarredObject = new JSONObject();
            unstarredArray.put(mEntryId);

            try {
                unstarredObject.put(Feedbin.JSON_STARRED_ENTRIES, unstarredArray);
                // remove the starred entry
                Feedbin.deleteStarredEntry(unstarredObject);
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }
    }
}
