/* YARR (Yet Another RSS Reader)
 * Copyright (C) 2017  Logan Garcia
 *
 * This file is part of YARR.
 *
 * YARR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * YARR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with YARR.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.cgarcia.yarr.extension;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

/**
 * Overrides the ViewPager to prevent the WebView from
 * scrolling in certain situations.
 *
 * @author  Logan Garcia
 * @version %I%, %G%
 */
public class ExtendedViewPager extends ViewPager {
    private boolean isPagingEnabled;

    public ExtendedViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean canScroll(View v, boolean checkV, int dx, int x, int y) {
        // the inner views of the view pager cannot scroll
        return false;
    }

    @Override
    @SuppressLint("ClickableViewAccessibility")
    public boolean onTouchEvent(MotionEvent event) {
        return this.isPagingEnabled && super.onTouchEvent(event);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        return this.isPagingEnabled && super.onInterceptTouchEvent(event);
    }

    /**
     * Sets the boolean parameter that determines
     * whether the viewpager can scroll.
     *
     * @param isPagingEnabled true if viewpager can scroll
     */
    public void setPagingEnabled(boolean isPagingEnabled) {
        this.isPagingEnabled = isPagingEnabled;
    }
}
