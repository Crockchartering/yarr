/* YARR (Yet Another RSS Reader)
 * Copyright (C) 2017  Logan Garcia
 *
 * This file is part of YARR.
 *
 * YARR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * YARR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with YARR.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.cgarcia.yarr.api;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Authenticator;
import java.net.HttpURLConnection;
import java.net.PasswordAuthentication;
import java.net.URL;

/**
 * Manages API-specific functionality of Feedbin.
 *
 * @author  Logan Garcia
 * @version %I%, %G%
 */
public class Feedbin {
    // declare instance of the class
    private static Feedbin mInstance;

    // the date format
    public static final String DATE_FORMAT = "yyyy-MM-dd'T'kk:mm:ss.ssssss'Z'";

    // strings related to JSON parameters
    public static final String JSON_ID = "id";
    public static final String JSON_TITLE = "title";
    public static final String JSON_FEED_URL = "feed_url";
    public static final String JSON_NAME = "name";
    public static final String JSON_FEED_ID = "feed_id";
    public static final String JSON_PUBLISHED = "published";
    public static final String JSON_CONTENT = "content";
    public static final String JSON_AUTHOR = "author";
    public static final String JSON_URL = "url";
    public static final String JSON_ENCLOSURE = "enclosure";
    public static final String JSON_ENCLOSURE_URL = "enclosure_url";
    public static final String JSON_UNREAD_ENTRIES = "unread_entries";
    public static final String JSON_STARRED_ENTRIES = "starred_entries";
    public static final String JSON_CREATED_AT = "created_at";
    public static final String JSON_SITE_URL = "site_url";

    // strings related to network status results
    public static final int STATUS_OK = 200;
    public static final int STATUS_MULTIPLE_CHOICES = 300;
    public static final int STATUS_NOT_FOUND = 404;

    // strings related to URLs
    private static final String URL_BASE_START = "https://api.feedbin.com/v2/";
    private static final String URL_BASE_END = ".json";
    private static final String URL_BASE_AUTHENTICATION = "authentication";
    private static final String URL_BASE_TAGS = "taggings";
    private static final String URL_BASE_SUBS = "subscriptions";
    private static final String URL_BASE_ENTRIES = "entries";
    private static final String URL_BASE_ENTRIES_UNREAD = "unread_entries";
    private static final String URL_BASE_ENTRIES_STARRED = "starred_entries";
    private static final String URL_BASE_UPDATE = "update";

    // strings related to URL parameters
    private static final String URL_PARAMETER_IDS = "ids=";
    private static final String URL_PARAMETER_SINCE = "since=";
    private static final String URL_PARAMETER_PAGE = "page=";
    private static final String URL_PARAMETER_ENCLOSURE = "include_enclosure=true";

    // strings related to the content type header
    private static final String CONTENT_TYPE_KEY = "Content-Type";
    private static final String CONTENT_TYPE_VALUE = "application/json; charset=utf-8";

    /**
     * The default constructor.
     */
    private Feedbin() {}

    /**
     * Manages static initialization of the class.
     *
     * @param email    the user-provided email to authenticate
     * @param password the user-provided password to authenticate
     */
    public static void init(final String email, final String password) {
        if (mInstance == null) {
            // only allow one instance of this class
            mInstance = new Feedbin();
            Authenticator.setDefault(new Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(email, password.toCharArray());
                }
            });
        }
    }

    /**
     * Returns all entries.
     *
     * @return       the list of entries as a JSON array
     */
    public static JSONArray getAllEntries(String since) throws JSONException, IOException {
        int pageNum = 1;

        String address = URL_BASE_START + URL_BASE_ENTRIES + URL_BASE_END
                + "?" + URL_PARAMETER_ENCLOSURE
                + "&" + URL_PARAMETER_PAGE + pageNum;

        if (since != null && !since.isEmpty()) {
            address += "&" + URL_PARAMETER_SINCE + since;
        }

        JSONArray jsonEntries = new JSONArray();
        JSONArray retrievedEntries = get(address);

        while (retrievedEntries != null) {
            for (int i = 0; i < retrievedEntries.length(); i++) {
                JSONObject retrievedEntry = retrievedEntries.getJSONObject(i);
                jsonEntries.put(retrievedEntry);
            }

            pageNum++;
            address = URL_BASE_START + URL_BASE_ENTRIES + URL_BASE_END
                    + "?" + URL_PARAMETER_ENCLOSURE
                    + "&" + URL_PARAMETER_PAGE + pageNum;

            if (since != null && !since.isEmpty()) {
                address += "&" + URL_PARAMETER_SINCE + since;
            }

            retrievedEntries = get(address);
        }

        return jsonEntries;
    }

    /**
     * Returns all unread entries.
     *
     * @return the list of entries as a JSON array
     */
    public static JSONArray getUnreadEntries() throws IOException, JSONException {
        String address = URL_BASE_START + URL_BASE_ENTRIES_UNREAD
                + URL_BASE_END;
        return get(address);
    }

    /**
     * Returns all starred entries.
     *
     * @return the list of entries as a JSON array
     */
    public static JSONArray getStarredEntries() throws IOException, JSONException {
        String address = URL_BASE_START + URL_BASE_ENTRIES_STARRED
                + URL_BASE_END;
        return get(address);
    }

    /**
     * Returns all starred entries.
     *
     * @param entryId the ID of the entry to retrieve
     * @return        the entry as a JSON array
     */
    public static JSONArray getEntry(String entryId) throws IOException, JSONException {
        String address = URL_BASE_START + URL_BASE_ENTRIES + URL_BASE_END
                + "?" + URL_PARAMETER_ENCLOSURE
                + "&" + URL_PARAMETER_IDS + entryId;
        return get(address);
    }

    /**
     * Returns all subscriptions
     *
     * @return the list of subscriptions as a JSON array
     */
    public static JSONArray getSubscriptions() throws IOException, JSONException {
        String address = URL_BASE_START + URL_BASE_SUBS + URL_BASE_END;
        return get(address);
    }

    /**
     * Returns all tags.
     *
     * @return the list of tags as a JSON array
     */
    public static JSONArray getTags() throws IOException, JSONException {
        String address = URL_BASE_START + URL_BASE_TAGS + URL_BASE_END;
        return get(address);
    }

    /**
     * Creates an unread entry.
     *
     * @param unreadEntry the JSON object containing the entry
     * @return            the status code from the resulting operation
     */
    public static int createUnreadEntry(JSONObject unreadEntry) throws IOException {
        String address = URL_BASE_START + URL_BASE_ENTRIES_UNREAD
                + URL_BASE_END;
        return create(address, unreadEntry);
    }

    /**
     * Creates a starred entry.
     *
     * @param starredEntry the JSON object containing the entry
     * @return             the status code from the resulting operation
     */
    public static int createStarredEntry(JSONObject starredEntry) throws IOException {
        String address = URL_BASE_START + URL_BASE_ENTRIES_STARRED
                + URL_BASE_END;
        return create(address, starredEntry);
    }

    /**
     * Creates a subscription.
     *
     * @param subscription the JSON object containing the subscription
     * @return             the status code from the resulting operation
     */
    public static int createSubscription(JSONObject subscription) throws IOException {
        String address = URL_BASE_START + URL_BASE_SUBS + URL_BASE_END;
        return create(address, subscription);
    }

    /**
     * Creates a tag.
     *
     * @param tag the JSON object containing the tag
     * @return    the status code from the resulting operation
     */
    public static int createTag(JSONObject tag) throws IOException {
        String address = URL_BASE_START + URL_BASE_TAGS + URL_BASE_END;
        return create(address, tag);
    }

    /**
     * Updates a subscription with the given information.
     *
     * @param subId        the ID of the subscription
     * @param subscription the JSON object containing the subscription
     * @return             the status code from the resulting operation
     */
    public static int updateSubscription(String subId, JSONObject subscription) throws IOException {
        String address = URL_BASE_START + URL_BASE_SUBS
                + "/" + subId + "/" + URL_BASE_UPDATE + URL_BASE_END;
        return create(address, subscription);
    }

    /**
     * Deletes an unread entry.
     *
     * @param unreadEntry the JSON object containing the entry
     * @return            the status code from the resulting operation
     */
    public static int deleteUnreadEntry(JSONObject unreadEntry) throws IOException {
        String address = URL_BASE_START + URL_BASE_ENTRIES_UNREAD
                + URL_BASE_END;
        return delete(address, unreadEntry);
    }

    /**
     * Deletes a starred entry.
     *
     * @param starredEntry the JSON object containing the entry
     * @return             the status code from the resulting operation
     */
    public static int deleteStarredEntry(JSONObject starredEntry) throws IOException {
        String address = URL_BASE_START + URL_BASE_ENTRIES_STARRED
                + URL_BASE_END;
        return delete(address, starredEntry);
    }

    /**
     * Deletes a subscription.
     *
     * @param subId the ID of the subscription
     * @return      the status code from the resulting operation
     */
    public static int deleteSubscription(String subId) throws IOException {
        String address = URL_BASE_START + URL_BASE_SUBS
                + "/" + subId + URL_BASE_END;
        return delete(address);
    }

    /**
     * Deletes a tag.
     *
     * @param tagId the ID of the tag
     * @return      the status code from the resulting operation
     */
    public static int deleteTag(String tagId) throws IOException {
        String address = URL_BASE_START + URL_BASE_TAGS
                + "/" + tagId + URL_BASE_END;
        return delete(address);
    }

    /**
     * Authenticates an email and password.
     *
     * @param email    the user-provided email to authenticate
     * @param password the user-provided password to authenticate
     * @return         the status code from the resulting operation
     */
    public static int authenticate(final String email, final String password) throws IOException {
        // reset the instance of this class if
        // attempting to log in for the first time
        mInstance = null;

        Authenticator.setDefault(new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(email, password.toCharArray());
            }
        });

        // attempt to authenticate the credentials
        String address = URL_BASE_START + URL_BASE_AUTHENTICATION
                + URL_BASE_END;
        URL url = new URL(address);
        HttpURLConnection endpoint = (HttpURLConnection)url.openConnection();
        endpoint.connect();
        int status = endpoint.getResponseCode(); //get status code from endpoint
        endpoint.disconnect();

        return status;
    }

    /**
     * Returns a JSON array.
     *
     * @param address the URL endpoint to retrieve the array
     * @return        the status code from the resulting operation
     */
    private static JSONArray get(String address) throws IOException, JSONException {
        // create a string builder to store input stream
        StringBuilder sb = new StringBuilder();
        URL url = new URL(address);
        HttpURLConnection endpoint = (HttpURLConnection) url.openConnection();
        endpoint.connect();

        if (endpoint.getResponseCode() == STATUS_OK) {
            BufferedReader br = new BufferedReader(new InputStreamReader(endpoint.getInputStream()));
            String line;

            // store the input stream
            while ((line = br.readLine()) != null) {
                sb.append(line).append("\n");
            }

            br.close();
        }

        // convert the input stream to JSON array
        endpoint.disconnect();

        String s = sb.toString();
        if (s.length() > 0) {
            return new JSONArray(s);
        }

        return null;
    }

    /**
     * Creates a JSON object.
     *
     * @param address    the URL endpoint to create the object
     * @param jsonObject the object to be created
     * @return           the status code from the resulting operation
     */
    private static int create(String address, JSONObject jsonObject) throws IOException {
        // set the network protocol as a POST
        // request using the given address
        URL url = new URL(address);
        HttpURLConnection endpoint = (HttpURLConnection) url.openConnection();
        endpoint.setDoInput(true);
        endpoint.setDoOutput(true);
        endpoint.setRequestProperty(CONTENT_TYPE_KEY, CONTENT_TYPE_VALUE);
        OutputStreamWriter osw = new OutputStreamWriter(endpoint.getOutputStream());
        osw.write(jsonObject.toString());
        osw.flush();
        osw.close();
        int status = endpoint.getResponseCode();
        endpoint.disconnect();

        return status;
    }

    /**
     * Deletes a JSON object.
     *
     * @param address    the URL endpoint to delete the object
     * @param jsonObject the object to be deleted
     * @return           the status code from the resulting operation
     */
    private static int delete(String address, JSONObject jsonObject) throws IOException {
        // set the network protocol as a DELETE
        // request using the given address
        URL url = new URL(address);
        HttpURLConnection endpoint = (HttpURLConnection) url.openConnection();
        endpoint.setDoOutput(true);
        endpoint.setRequestMethod("DELETE");
        endpoint.setRequestProperty(CONTENT_TYPE_KEY, CONTENT_TYPE_VALUE);
        OutputStreamWriter osw = new OutputStreamWriter(endpoint.getOutputStream());
        osw.write(jsonObject.toString());
        osw.flush();
        osw.close();
        int status = endpoint.getResponseCode();
        endpoint.disconnect();

        return status;
    }

    /**
     * Deletes a JSON object.
     *
     * @param address the URL endpoint to delete the object
     * @return        the status code from the resulting operation
     */
    private static int delete(String address) throws IOException {
        // set the network protocol as a DELETE
        // request using the given address
        URL url = new URL(address);
        HttpURLConnection endpoint = (HttpURLConnection) url.openConnection();
        endpoint.setDoOutput(true);
        endpoint.setRequestMethod("DELETE");
        endpoint.setRequestProperty(CONTENT_TYPE_KEY, CONTENT_TYPE_VALUE);
        int status = endpoint.getResponseCode();
        endpoint.disconnect();

        return status;
    }
}
