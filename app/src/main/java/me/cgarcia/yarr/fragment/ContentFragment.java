/* YARR (Yet Another RSS Reader)
 * Copyright (C) 2017  Logan Garcia
 *
 * This file is part of YARR.
 *
 * YARR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * YARR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with YARR.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.cgarcia.yarr.fragment;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DownloadManager;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.DownloadListener;
import android.webkit.URLUtil;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import me.cgarcia.yarr.DefaultApplication;
import me.cgarcia.yarr.R;
import me.cgarcia.yarr.activity.ContentActivity;

/**
 * Displays the layout for a single entry.
 *
 * @author  Logan Garcia
 * @version %I%, %G%
 */
public class ContentFragment extends Fragment {
    // strings related to passing arguments
    private static final String KEY_STRING_TITLE = "title";
    private static final String KEY_STRING_AUTHOR = "author";
    private static final String KEY_STRING_DATE = "date";
    private static final String KEY_STRING_NAME = "name";
    private static final String KEY_STRING_ENCLOSURE = "enclosure";
    private static final String KEY_STRING_BODY = "body";

    // declare the elements of the content
    private String mTitle;
    private String mAuthor;
    private String mDate;
    private String mName;
    private String mBody;
    private String mEnclosure;

    // declare the views
    private View mFragmentView;
    private ScrollView mScrollView;
    private FrameLayout mFullscreenViewContainer;
    private View mFullscreenView;
    private WebChromeClient.CustomViewCallback mFullscreenViewCallback;

    /**
     * Constructor for creating fragment with arguments.
     *
     * @param title     the title of the entry
     * @param author    the author of the entry
     * @param date      the date of the entry
     * @param name      the name of the entry
     * @param enclosure the enclosed data of the entry
     * @param body      the html body of the entry
     * @return          the newly created content fragment
     */
    public static ContentFragment newInstance(String title, String author, String date, String name,
                                              String enclosure, String body) {
        // pass the view elements as arguments
        ContentFragment contentFragment = new ContentFragment();
        Bundle args = new Bundle();
        args.putString(KEY_STRING_TITLE, title);
        args.putString(KEY_STRING_AUTHOR, author);
        args.putString(KEY_STRING_DATE, date);
        args.putString(KEY_STRING_NAME, name);
        args.putString(KEY_STRING_ENCLOSURE, enclosure);
        args.putString(KEY_STRING_BODY, body);
        contentFragment.setArguments(args);
        return contentFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // do not reload fragment on rotation
        setRetainInstance(true);

        // get the view elements
        mTitle = getArguments().getString(KEY_STRING_TITLE);
        mAuthor = getArguments().getString(KEY_STRING_AUTHOR);
        mDate = getArguments().getString(KEY_STRING_DATE);
        mName = getArguments().getString(KEY_STRING_NAME);
        mBody = getArguments().getString(KEY_STRING_BODY);
        mEnclosure = getArguments().getString(KEY_STRING_ENCLOSURE);
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // set the main layout
        if (mFragmentView == null) {
            mFragmentView = inflater.inflate(R.layout.fragment_content, container, false);

            // set the main view elements
            //mRootView = (LinearLayout) mFragmentView.findViewById(R.id.fragment_content_root);
            mScrollView = (ScrollView) mFragmentView.findViewById(R.id.fragment_content_scroll);
            TextView titleView = (TextView) mFragmentView.findViewById(R.id.fragment_content_title);
            TextView subtitleView = (TextView) mFragmentView.findViewById(R.id.fragment_content_subtitle);
            WebView bodyView = (WebView) mFragmentView.findViewById(R.id.fragment_content_body);
            mFullscreenViewContainer = (FrameLayout) mFragmentView.findViewById(R.id.fragment_content_fullscreen);

            bodyView.setOnTouchListener(setupTouchListener());
            bodyView.setDownloadListener(setupDownloadListener());
            bodyView.setWebChromeClient(new CustomChromeClient());
            bodyView.getSettings().setJavaScriptEnabled(true);
            bodyView.getSettings().setDefaultFontSize(14);
            bodyView.setBackgroundColor(ContextCompat.getColor(getActivity(),
                    R.color.backgroundPrimary));

            // set the title
            if (mTitle != null && !mTitle.equals("null")) {
                titleView.setText(DefaultApplication.fromHtml(mTitle));
            }

            // get the relative date
            mDate = DefaultApplication.getRelativeTimeSpanString(mDate);
            String subtitleText;

            // set the subtitle text
            if (mAuthor.equals("null") || mAuthor == null || mAuthor.equals(mName)) {
                // do not include redundant author field
                subtitleText = mDate + "\n" + mName;
            } else {
                // author exists and is unique
                subtitleText = mAuthor + " \u2022 " + mDate + "\n" + mName;
            }

            subtitleView.setText(subtitleText);

            // load the web view with custom CSS
            mBody = "<html><head>" + getStyle() + "</head><body>" + getEnclosure()
                    + mBody + "</body></html>";
            bodyView.loadData(DefaultApplication.getEncodedUrl(mBody), "text/html", "utf-8");
        }

        return mFragmentView;
    }

    /**
     * Creates a style element that properly sets the color,
     * padding, and view for the web layout.
     *
     * @return the CSS style element
     */
    private String getStyle() {
        return "<style>" +
                "body{color: rgba(" +
                getRgbaFromHex(R.color.textPrimary) +
                ");}" +
                "a, a:visited{color: rgba(" +
                getRgbaFromHex(R.color.colorSecondary) +
                ");word-wrap: break-word;}" +
                "blockquote{border-left: 2px solid rgba(" +
                getRgbaFromHex(R.color.colorPrimary) +
                ");padding-left: 12px;margin: 0;" +
                "color: rgba(" +
                getRgbaFromHex(R.color.textSecondary) +
                ");}" +
                "::selection{color: rgba(" +
                getRgbaFromHex(R.color.textPrimary) +
                ");background: rgba(" +
                getRgbaFromHex(R.color.colorSecondaryLight) +
                ");}" +
                "img, .wp-caption{max-width: 100% !important;height: auto !important;}" +
                "audio{width: 100% !important;}" +
                "video, .wp-video{width: 100% !important; height: auto !important;}" +
                "div[style]{background-color: rgba(" +
                getRgbaFromHex(R.color.backgroundPrimary) +
                ") !important;}" +
                ".tilt-card--has-image{display: none;}" +
                "pre{white-space: pre-wrap;}" +
                "td{display: inline;}" +
                "td+td:before{content: \"| \";}" +
                "iframe{width: 100% !important;height: 56.25vw;}" +
                "</style>";
    }

    /**
     * Converts an 8-digit ARGB hex color value to its
     * equivalent RGBA value to be used in a CSS element.
     *
     * @return the CSS-compliant RGBA value
     */
    private String getRgbaFromHex(int color) {
        // get hex color from reference
        color = ContextCompat.getColor(getActivity(), color);

        // calculate individual values from offset
        int red = Math.round((color >> 16) & 0xFF);
        int green = Math.round((color >> 8) & 0xFF);
        int blue = Math.round((color) & 0xFF);

        // offset alpha range from 0.0 to 1.0
        float alpha = (float)((color >> 24) & 0xFF) / 256;

        // format the return value for CSS-compliance
        return String.valueOf(red) + ", " + String.valueOf(green) + ", " + String.valueOf(blue)
                + ", " + String.valueOf(alpha);
    }

    /**
     * Creates an audio, video, or img HTML element based
     * on the type of enclosure provided.
     *
     * @return the HTML tag element
     */
    private String getEnclosure() {
        StringBuilder enclosure = new StringBuilder();

        // make sure there is something to parse from the enclosure
        if (mEnclosure != null && !mEnclosure.isEmpty()) {
            if (mEnclosure.contains(".aac")
                    || mEnclosure.contains(".flac")
                    || mEnclosure.contains(".mp3")
                    || mEnclosure.contains(".ogg")
                    || mEnclosure.contains(".wav")
                    || mEnclosure.contains(".m4a")) {
                // build the audio element
                enclosure.append("<p><audio controls>");
                enclosure.append("<source src=\"");
                enclosure.append(mEnclosure);
                enclosure.append("\">");
                enclosure.append("</audio></p>");
            } else if (mEnclosure.contains(".3gp")
                    || mEnclosure.contains(".mkv")
                    || mEnclosure.contains(".mp4")
                    || mEnclosure.contains(".webm")) {
                // build the video element
                enclosure.append("<p><video controls>");
                enclosure.append("<source src=\"");
                enclosure.append(mEnclosure);
                enclosure.append("\">");
                enclosure.append("</video></p>");
            } else if (mEnclosure.contains(".bmp")
                    || mEnclosure.contains(".gif")
                    || mEnclosure.contains(".jpg")
                    || mEnclosure.contains(".png")
                    || mEnclosure.contains(".webp")) {
                // build the img element
                enclosure.append("<p><img src=\"");
                enclosure.append(mEnclosure);
                enclosure.append("\"></p>");
            }
        }

        return enclosure.toString();
    }

    /**
     * Returns the listener that prevents the WebView from
     * scrolling vertically independent of the rest of the
     * layout.
     *
     * @return the on touch listener
     */
    private View.OnTouchListener setupTouchListener() {
        return new View.OnTouchListener() {
            @SuppressLint("ClickableViewAccessibility")
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return (event.getAction() == MotionEvent.ACTION_MOVE);
            }
        };
    }

    /**
     * Returns the listener that opens a dialogue box and, upon
     * confirmation, downloads the resulting file.
     *
     * @return the on touch listener
     */
    private DownloadListener setupDownloadListener() {
        return new DownloadListener() {
            @Override
            public void onDownloadStart(String url, String userAgent,
                                        String contentDisposition, String mimetype,
                                        long contentLength) {
                // set the download manager from the URI
                final DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
                request.allowScanningByMediaScanner();
                final String filename = URLUtil.guessFileName(url, contentDisposition, mimetype);

                // notify client once download is completed
                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);

                // make sure storage permission has been granted
                if (DefaultApplication.isStoragePermissionGranted(getActivity())) {
                    new AlertDialog.Builder(getActivity())
                            .setMessage(R.string.popup_download_message)
                            .setPositiveButton(R.string.popup_download_positive, new DialogInterface.OnClickListener()
                            {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    // download to default downloads directory
                                    request.setDestinationInExternalPublicDir(
                                            Environment.DIRECTORY_DOWNLOADS, filename);
                                    DownloadManager dm = (DownloadManager)
                                            getActivity().getSystemService(Context.DOWNLOAD_SERVICE);
                                    if (dm != null) {
                                        dm.enqueue(request);
                                    }
                                }

                            })
                            .setNegativeButton(R.string.popup_download_negative, null)
                            .show();
                }

            }
        };
    }

    /**
     * WebChromeClient that implements fullscreen support
     * for video elements within an HTML document.
     *
     * @author  Logan Garcia
     * @version %I%, %G%
     */
    private class CustomChromeClient extends WebChromeClient {
        @Override
        public void onShowCustomView(View view, CustomViewCallback callback) {
            if (isAdded() && getActivity() != null) {
                ContentActivity contentActivity = (ContentActivity) getActivity();
                contentActivity.onShowCustomView();

                // if a view already exists then immediately terminate the new one
                if (mFullscreenView != null) {
                    callback.onCustomViewHidden();
                    return;
                }

                // hide UI when in fullscreen
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    // enable immersive mode if API is high enough
                    view.setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
                } else {
                    // do not enable immersive mode
                    view.setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN);
                }

                // hide the content and show fullscreen
                mScrollView.setVisibility(View.GONE);
                mFullscreenView = view;

                // show the custom view
                mFullscreenViewContainer.setBackgroundResource(android.R.color.black);
                mFullscreenViewContainer.setVisibility(View.VISIBLE);

                // add the custom view from its container
                mFullscreenViewContainer.addView(view);
                mFullscreenViewCallback = callback;
            }
        }

        @Override
        public void onHideCustomView() {
            if (isAdded() && getActivity() != null) {
                ContentActivity contentActivity = (ContentActivity) getActivity();
                contentActivity.onHideCustomView();

                // check to see if view is already hidden
                if (mFullscreenView == null) {
                    return;
                }

                // show the content and hide fullscreen
                mScrollView.setVisibility(View.VISIBLE);
                mFullscreenViewContainer.setVisibility(View.GONE);

                // hide the custom view
                mFullscreenView.setVisibility(View.GONE);

                // remove the custom view from its container
                mFullscreenViewContainer.removeView(mFullscreenView);
                mFullscreenViewCallback.onCustomViewHidden();
                mFullscreenView = null;
            }
        }
    }
}
