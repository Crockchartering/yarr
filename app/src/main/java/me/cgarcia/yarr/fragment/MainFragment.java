/* YARR (Yet Another RSS Reader)
 * Copyright (C) 2017  Logan Garcia
 *
 * This file is part of YARR.
 *
 * YARR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * YARR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with YARR.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.cgarcia.yarr.fragment;

import android.app.Fragment;
import android.os.Bundle;

import java.util.ArrayList;

import me.cgarcia.yarr.object.Entry;
import me.cgarcia.yarr.object.Subscription;
import me.cgarcia.yarr.object.Tag;

/**
 * Fragment that holds the array lists for entries,
 * subscriptions, and tags between UI changes.
 *
 * @author  Logan Garcia
 * @version %I%, %G%
 */
public class MainFragment extends Fragment {
    // array lists
    private ArrayList<Entry> entries;
    private ArrayList<Subscription> subscriptions;
    private ArrayList<Tag> tags;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // retain this fragment
        setRetainInstance(true);
    }

    /**
     * Returns the entry array list.
     *
     * @return the entry array list
     */
    public ArrayList<Entry> getEntries() {
        if (entries == null) {
            return new ArrayList<>();
        }

        return entries;
    }

    /**
     * Returns the subscription array list.
     *
     * @return the subscription array list
     */
    public ArrayList<Subscription> getSubscriptions() {
        if (subscriptions == null) {
            return new ArrayList<>();
        }

        return subscriptions;
    }

    /**
     * Returns the tag array list.
     *
     * @return the tag array list
     */
    public ArrayList<Tag> getTags() {
        if (tags == null) {
            return new ArrayList<>();
        }

        return tags;
    }

    /**
     * Sets the entry array list.
     *
     * @param entries the entry array list
     */
    public void setEntries(ArrayList<Entry> entries) {
        this.entries = entries;
    }

    /**
     * Sets the subscription array list.
     *
     * @param subscriptions the subscription array list
     */
    public void setSubscriptions(ArrayList<Subscription> subscriptions) {
        this.subscriptions = subscriptions;
    }

    /**
     * Sets the tag array list.
     *
     * @param tags the tag array list
     */
    public void setTags(ArrayList<Tag> tags) {
        this.tags = tags;
    }
}
