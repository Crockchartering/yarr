<?xml version="1.0" encoding="utf-8"?>

<!--YARR (Yet Another RSS Reader)
    Copyright (C) 2017  Logan Garcia

    This file is part of YARR.

    YARR is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    YARR is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with YARR.  If not, see <http://www.gnu.org/licenses/>.-->

<android.support.constraint.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context="me.cgarcia.yarr.activity.SettingsActivity">

    <LinearLayout
        android:id="@+id/activity_settings"
        android:layout_width="0dp"
        android:layout_height="0dp"
        android:layout_marginBottom="0dp"
        android:layout_marginEnd="0dp"
        android:layout_marginStart="0dp"
        android:layout_marginTop="0dp"
        android:orientation="vertical"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintHorizontal_bias="1.0"
        app:layout_constraintLeft_toLeftOf="parent"
        app:layout_constraintRight_toRightOf="parent"
        app:layout_constraintTop_toTopOf="parent"
        app:layout_constraintVertical_bias="0.0"
        tools:layout_constraintBottom_creator="1"
        tools:layout_constraintLeft_creator="1"
        tools:layout_constraintRight_creator="1"
        tools:layout_constraintTop_creator="1">

        <!-- the ActionBar displayed at the top -->
        <include
            layout="@layout/toolbar"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            app:layout_behavior="@string/appbar_scrolling_view_behavior" />

        <ScrollView
            android:layout_width="match_parent"
            android:layout_height="wrap_content">

            <LinearLayout
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:orientation="vertical">

                <!-- the general label category -->
                <TextView
                    android:text="@string/dialogue_general"
                    android:textColor="@color/colorSecondary"
                    style="@style/Subheader"/>

                <!-- night mode check box -->
                <android.support.v7.widget.AppCompatCheckBox
                    android:id="@+id/activity_settings_night"
                    android:text="@string/action_dark_theme"
                    style="@style/CheckBox"/>

                <View style="@style/LineDivider" />

                <!-- sign out clickable layout -->
                <LinearLayout
                    android:id="@+id/activity_settings_sign_out"
                    style="@style/SettingsItem"
                    android:layout_width="match_parent"
                    android:layout_height="@dimen/height_entry_item"
                    android:clickable="true"
                    android:background="@drawable/background_state_drawable">

                    <!-- sign out name -->
                    <TextView
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:ellipsize="end"
                        android:lines="1"
                        android:maxLines="1"
                        android:text="@string/action_sign_out"
                        android:textColor="@color/textPrimary"
                        android:textSize="@dimen/text_size_regular" />

                    <!-- user email -->
                    <TextView
                        android:id="@+id/activity_settings_sign_out_subtitle"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:ellipsize="end"
                        android:lines="1"
                        android:maxLines="1"
                        android:text="Email"
                        android:textColor="@color/textSecondary"
                        android:textSize="@dimen/text_size_small" />

                </LinearLayout>

                <!-- data usage section -->
                <TextView
                    android:text="@string/dialogue_sync"
                    android:textColor="@color/colorSecondary"
                    style="@style/Subheader"/>


                <!-- sync frequency clickable layout -->
                <LinearLayout
                    android:id="@+id/activity_settings_sync_frequency"
                    style="@style/SettingsItem"
                    android:clickable="true"
                    android:background="@drawable/background_state_drawable"
                    android:layout_width="match_parent"
                    android:layout_height="@dimen/height_entry_item">

                    <!-- sync frequency heading -->
                    <TextView
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:ellipsize="end"
                        android:lines="1"
                        android:maxLines="1"
                        android:text="@string/popup_sync_frequency"
                        android:textColor="@color/textPrimary"
                        android:textSize="@dimen/text_size_regular" />

                    <!-- the sync frequency interval -->
                    <TextView
                        android:id="@+id/activity_settings_sync_frequency_time"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:ellipsize="end"
                        android:lines="1"
                        android:maxLines="1"
                        android:text="5 minutes"
                        android:textColor="@color/textSecondary"
                        android:textSize="@dimen/text_size_small" />

                </LinearLayout>

                <View style="@style/LineDivider" />

                <!-- wifi check box -->
                <android.support.v7.widget.AppCompatCheckBox
                    android:id="@+id/activity_settings_sync_wifi"
                    android:text="@string/action_sync_wifi"
                    style="@style/CheckBox" />

                <View style="@style/LineDivider" />

                <!-- charging check box -->
                <android.support.v7.widget.AppCompatCheckBox
                    android:id="@+id/activity_settings_sync_charge"
                    android:text="@string/action_sync_charge"
                    style="@style/CheckBox" />

                <TextView
                    android:text="@string/dialogue_notification"
                    android:textColor="@color/colorSecondary"
                    style="@style/Subheader"/>

                <android.support.v7.widget.AppCompatCheckBox
                    android:id="@+id/activity_settings_notifications"
                    android:text="@string/action_notifications"
                    style="@style/CheckBox" />

                <View style="@style/LineDivider" />

                <!-- sound selection layout -->
                <LinearLayout
                    android:id="@+id/activity_settings_sound"
                    style="@style/SettingsItem"
                    android:layout_width="match_parent"
                    android:layout_height="@dimen/height_entry_item"
                    android:clickable="true"
                    android:background="@drawable/background_state_drawable">

                    <!-- sound heading -->
                    <TextView
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:ellipsize="end"
                        android:lines="1"
                        android:maxLines="1"
                        android:text="@string/popup_sound"
                        android:textColor="@color/textPrimary"
                        android:textSize="@dimen/text_size_regular" />

                    <!-- the current sound selected -->
                    <TextView
                        android:id="@+id/activity_settings_sound_name"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:ellipsize="end"
                        android:lines="1"
                        android:maxLines="1"
                        android:text="None"
                        android:textColor="@color/textSecondary"
                        android:textSize="@dimen/text_size_small" />
                </LinearLayout>

                <View style="@style/LineDivider" />

                <!-- vibrate check box -->
                <android.support.v7.widget.AppCompatCheckBox
                    android:id="@+id/activity_settings_vibrate"
                    android:text="@string/action_vibrate"
                    style="@style/CheckBox" />

                <!-- import and export section -->
                <TextView
                    android:text="@string/dialogue_import_export"
                    android:textColor="@color/colorSecondary"
                    style="@style/Subheader"/>

                <!-- import clickable layout -->
                <LinearLayout
                    android:id="@+id/activity_settings_import"
                    style="@style/SettingsItem"
                    android:layout_width="match_parent"
                    android:layout_height="@dimen/height_settings_item"
                    android:clickable="true"
                    android:background="@drawable/background_state_drawable">

                    <TextView
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:ellipsize="end"
                        android:lines="1"
                        android:maxLines="1"
                        android:text="@string/action_import_opml"
                        android:textColor="@color/textPrimary"
                        android:textSize="@dimen/text_size_regular" />
                </LinearLayout>

                <View style="@style/LineDivider" />

                <!-- export clickable layout -->
                <LinearLayout
                    android:id="@+id/activity_settings_export"
                    style="@style/SettingsItem"
                    android:layout_width="match_parent"
                    android:layout_height="@dimen/height_settings_item"
                    android:clickable="true"
                    android:background="@drawable/background_state_drawable">

                    <TextView
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:ellipsize="end"
                        android:lines="1"
                        android:maxLines="1"
                        android:text="@string/action_export_opml"
                        android:textColor="@color/textPrimary"
                        android:textSize="@dimen/text_size_regular" />
                </LinearLayout>

            </LinearLayout>
        </ScrollView>

    </LinearLayout>
</android.support.constraint.ConstraintLayout>
